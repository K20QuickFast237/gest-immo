<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Contact_model extends CI_Model{
		
	private $id;
	private $id_user;
	private $tel1;
	private $tel2;
	private $email;
	private $whatsapp;
	private $facebook;
	private $twitter;
	private $instagram;
	private $linkedin;
	private $telegram;

	protected $table= 'contact';


	function __construct()
		{
			
		}
		
	// Hydrater un reservation

		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value){
				$method = 'set'.ucfirst($key);
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}

	// inserer une localisation

		public function inserer(array $data){

			foreach ($data as $key=>$value){
				$this->db->set($key, $this->$key);

 			}
 			
			$this->db->insert($this->table);			
		}

	// update un contact

		public function actualise(array $data,$id){

			foreach ($data as $key=>$value){
				$this->db->set($key, $this->$key)
						 ->where('id', $id);

 			}

			$this->db->update($this->table);			
		
		}

	// recuperer un contact 

		public function recuperer(array $tab){
			$key = key($tab);

			$data = $this->db->select('*')
							 ->from($this->table)
							 ->where($key, $tab[$key])
							 ->get()
							 ->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_user']=$row->id_user;
			       	$donnees[$i]['tel1']=$row->tel1;
			       	$donnees[$i]['tel2']=$row->tel2;
			       	$donnees[$i]['email']=$row->email;
			       	$donnees[$i]['whatsapp']=$row->whatsapp;
			       	$donnees[$i]['facebook']=$row->facebook;
			       	$donnees[$i]['twitter']=$row->twitter;
			       	$donnees[$i]['instagram']=$row->instagram;
			       	$donnees[$i]['linkedin']=$row->linkedin;
			       	$donnees[$i]['telegram']=$row->telegram;

			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;			
		
		}	
	// setteurs

		public function setId($id){
			$this->id=$id;
		}
		
		public function setId_user($id_user){
			$this->id_user=$id_user;
		}
		
		public function setTel1($tel1){
			$this->tel1=$tel1;
		}
		
		public function setTel2($tel2){
			$this->tel2=$tel2;
		}
		
		public function setEmail($email){
			$this->email=$email;
		}

		public function setWhatsapp($whatsapp){
			$this->whatsapp=$whatsapp;
		}

		public function setFacebook($facebook){
			$this->facebook=$facebook;
		}

		public function setTwitter($twitter){
			$this->twitter=$twitter;
		}

		public function setInstagram($instagram){
			$this->instagram=$instagram;
		}
	
		public function setLinkedin($linkedin){
			$this->linkedin=$linkedin;
		}
	
		public function setTelegram($telegram){
			$this->telegram=$telegram;
		}


	// getteurs

		public function getId(){
			return $this->id;
		}

		public function getId_user(){
			return $this->id_user;
		}

		public function getTel1(){
			return $this->tel1;
		}

		public function getTel2(){
			return $this->tel2;
		}

		public function getEmail(){
			return $this->email;
		}
		
		public function getWhatsapp(){
			return $this->whatsapp;
		}

		public function getFacebook(){
			return $this->facebook;
		}

		public function getTwitter(){
			return $this->twitter;
		}

		public function getInstagram(){
			return $this->instagram;
		}
	
		public function getLinkedin(){
			return $this->linkedin;
		}

		public function getTelegram(){
			return $this->telegram;
		}
}


?>

