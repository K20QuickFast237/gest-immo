<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Connexion_model extends CI_Model{
		
	private $nom;
	private $email;
	private $password;
	private $random;

	protected $table= 'connexion';


	function __construct()
		{
			
		}
		
	// Hydrater un reservation

		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value){
				$method = 'set'.ucfirst($key);
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}

	// inserer des valeurs
		public function savedata(array $data){

			foreach ($data as $key=>$value){

				if (isset($this->$key)) {
					$this->db->set($key, $this->$key);
				}else{
					$this->db->set($key, $value);
				}
				
			}

			$this->db->insert($this->table);
		}

	// Supprime une connexion	
		public function delete(array $data){ 
			$key = key($data);

			if (isset($this->$key)) {
				$this->db->where($key, $this->$key)
					 ->delete($this->table);
			}else{
				$this->db->where($key, $data[$key])
					 ->delete($this->table);
			}
						
		
		}

	// recuperer recois un tableau representant un champ de la table dont la valeur sert de critere de selection et renvoie la connexion correspondante

		public function recuperer(array $data){
			$key =  key($data);

			if (isset($this->$key)) {
				$data = $this->db->select('*')
								->from($this->table)
								->where($key, $this->$key)
								->get()
								->result();
			}else{
				$data = $this->db->select('*')
								->from($this->table)
								->where($key, $data[$key])
								->get()
								->result();
			}
			

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){

			       	foreach($row as $attrit=>$val){
						$donnees[$i][$attrit]=$val;
					}

			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;			
		
		}

	// setteurs


		public function setNom($val){
			$this->nom=$val;
		}

		public function setEmail($val){
			$this->email=$val;
		}
		
		public function setPassword($val){
			$this->password=$val;
		}

		public function setRandom($val){
			$this->random=$val;
		}


	// getteurs

		public function getNom(){
			return $this->nom;
		}
		
		public function getEmail(){
			return $this->email;
		}

		public function getPassword(){
			return $this->password;
		}

		public function getRandom(){
			return $this->random;
		}

}


?>

