<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Notification_model extends CI_Model{
		
	private $id;
	private $id_user;
	private $id_message;
	private $contenu;

	protected $table= 'notification';


	function __construct()
		{
			
		}
		
	// Hydrater un reservation

		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value){
				$method = 'set'.ucfirst($key);
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}


	// setteurs


		public function setId($id){
			$this->id=$id;
		}

		public function setId_user($id_user){
			$this->id_user=$id_user;
		}
		
		public function setid_Message($id_message){
			$this->id_message=$id_message;
		}

		public function setContenu($contenu){
			$this->contenu=$contenu;
		}



	// getteurs

		public function getId(){
			return $this->id;
		}
		
		public function getId_user(){
			return $this->id_user;
		}

		public function getId_message(){
			return $this->id_message;
		}

		public function getContenu(){
			return $this->contenu;
		}


}


?>
