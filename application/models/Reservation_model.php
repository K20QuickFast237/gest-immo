<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Reservation_model extends CI_Model{
		
	private $id;
	private $id_user;
	private $id_bien;
	private $duree;
	private $prix;
	private $date_creation;
	private $date_fin;

	protected $table= 'reservation';


	function __construct()
		{
			
		}
		
	// Hydrater un reservation

		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value){
				$method = 'set'.ucfirst($key);
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}


	// setteurs


		public function setId($id){
			$this->id=$id;
		}


		public function setId_user($id_user){
			$this->id_user=$id_user;
		}
		
		public function setId_bien($id_bien){
			$this->id_bien=$id_bien;
		}

		public function setDuree($duree){
			$this->duree=$duree;
		}
		public function setPrix($prix){
			$this->prix=$prix;
		}
		public function setDate_creation($date_creation){
			$this->date_creation=$date_creation;
		}
		
		public function setDate_fin($date_fin){
			$this->date_fin=$date_fin;
		}


	// getteurs

		public function getId(){
			return $this->id;
		}
		
		public function getId_user(){
			return $this->id_user;
		}

		public function getId_bien(){
			return $this->id_bien;
		}

		public function detDuree(){
			return $this->duree;
		}

		public function getPrix(){
			return $this->prix;
		}
		public function getDate_creation(){
			return $this->date_creation;
		}

		public function getDate_fin(){
			return $this->date_fin;
		}				
	
}


?>
