<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Abonnement_model extends CI_Model{
		
	private $id;
	private $id_user;
	private $type;
	private $prix;
	private $etat;
	private $date_debut;
	private $date_fin;

	protected $table= 'abonnement';


	function __construct()
		{
			
		}

		
	// Hydrater un reservation

		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value){
				$method = 'set'.ucfirst($key);
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}

	// inserer un Abonnement

		public function inserer(array $data){

			foreach ($data as $key=>$value){
				$this->db->set($key, $this->$key);
			}

			$this->db->insert($this->table);			
		
		}

	// recuperer recois un string representant un champ de la table et renvoie l'abonnement correspondante

		public function recuperer($key){
			// $key = key($tab);

			$data = $this->db->select('*')
								->from($this->table)
								// ->where($key, $tab[$key])
								->where($key, $this->$key)
								->limit(1)
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){

			       	foreach($row as $attrit=>$val){
						$donnees[$i][$attrit]=$val;
					}

			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;			
		
		}

	// upgrade permet de mettre a jour l'etat d'abonnement d'un utilisateur

		public function upgrade(string $key, int $nbr = 0){

			$data = $this->db->select('*')
							 ->from($this->table)
							 ->where($key, $this->$key)
							 // ->where($key, $this->$key)
							 ->limit(1)
							 ->get()
							 ->result();

				$i=0;
			// echo"data dans Abonnement_model <pre>"; print_r($data); echo "</pre> fin data";
			if (isset($data[0])) {
				$data = $data[0];
				// $abonne = [];
				// echo "<pre>"; print_r($data); echo "</pre>";
				if ($nbr!=0) {
					// $jour = new DateTime($data->date_fin);
					// $jour->add(new DateInterval('P'.$nbr.'D'));
					// $abonne['date_fin'] = $jour->format('Y-m-d');
					if (strtotime($data->date_fin) == 0) {
						$data->date_fin = date('Y-m-d');
					}
					$abonne['date_fin'] = date('Y-m-d', strtotime($data->date_fin) + ($nbr*24*3600)); // On ajoute n jours
					$abonne['etat'] = 'valide';
					$abonne['type'] = $nbr.'&nbsp; jours';
				}else{
					switch ($data->type) {
						case 'free': //une personne qui n'a pas d'abonnement peut deposer 2 annonces au plus
							if ($_SESSION['user']['identity']['nombre_annonce'] < 2) {
								$abonne['date_fin'] = 'null';
								$abonne['etat'] = 'valide';
							}else{
								$abonne['date_fin'] = 'null';
								$abonne['etat'] = 'invalide';
							}
							
							break;
						
						default:
							if (strtotime($data->date_fin) < strtotime(date('Y-m-d'))) {
								$abonne['date_fin'] = 'null';
								$abonne['etat'] = 'invalide';
							}else{
								$abonne['date_fin'] = $data->date_fin;
								$abonne['etat'] = $data->etat;
							}
							break;
					}
				}
				// mise a jour des valeurs dans la table
				$this->db->set('etat',$abonne['etat'])
						 ->set('date_fin',$abonne['date_fin'])
						 ->set('type',$abonne['type'])
						 ->where('id_user',$this->$key)
						 ->update($this->table);

				//actualisation de la table user
				$this->db->set('etat_abonnement',$abonne['etat'])
						 ->where('id',$this->$key)
						 ->update('user');

				// retournons le nouvel etat
				return $abonne['etat'];
			}else{ //on insere le nouvel inscrit
				$new['id_user'] = $_SESSION['user']['identity']['id'];
				$new['type'] = 'free';
				$new['etat'] = 'valide';
				$this->hydrate($new);
				$this->inserer($new);

				return 'valide';
			}
						
		
		}

	// setteurs
	

		public function setId($id){
			$this->id=$id;
		}
		
		public function setId_user($id_user){
			$this->id_user=$id_user;
		}
		
		public function setType($type){
			$this->type=$type;
		}
		
		public function setPrix($prix){
			$this->prix=$prix;
		}
		
		public function setEtat($etat){
			$this->etat=$etat;
		}

		public function setDate_debut($date_debut){
			$this->date_debut=$date_debut;
		}
	
		public function setDate_fin($date_fin){
			$this->date_fin=$date_fin;
		}



	// getteurs

		
		public function getId(){
			return $this->id;
		}

		public function getId_user(){
			return $this->id_user;
		}

		public function getType(){
			return $this->type;
		}

		public function getPrix(){
			return $this->prix;
		}

		public function getEtat(){
			return $this->etat;
		}
	
		public function getDate_debut(){
			return $this->date_debut;
		}

		public function getDate_fin(){
			return $this->date_fin;
		}

}


?>

