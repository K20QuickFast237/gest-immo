<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Bien_model extends CI_Model{
		
	private $id;
	private $id_user_proprio;
	private $id_user_utilisateur;
	private $id_localisation;
	private $id_description;
	private $id_reservation;
	private $id_equipement;
	private $prix;
	private $nombre_avis;
	private $type;
	private $document;
	private $etat;

	protected $table= 'bien';


	function __construct()
		{
			
		}
		
	// Hydrater un reservation

		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value){
				$method = 'set'.ucfirst($key);
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}

	// inserer un bien

		public function inserer(array $data){

			foreach ($data as $key=>$value){
				$this->db->set($key, $this->$key);
			}

			$this->db->insert($this->table);			
		
		}

	// mettre a jour un bien
/*
		public function update(array $data){
			$this->db->update($this->table)
				foreach ($data as $key=>$value){
					if ($key !== 'id') {
					 ->set($key, $this->$key);
					}
				}	 ->where('id',$this->id);

		}	
*/
	// mettre a jour un bien

		public function update(array $data){

			foreach ($data as $key=>$value){
				if ($key !== 'id') {
					$this->db->set($key, $this->$key)
							 ->where('id', $this->id)
							 ->update($this->table);
				}
				
			}

					
		
		}

	// recuperer un bien recois un array de noms de champs à recuperer dans la base

	/*	public function recuperer2(array $tab){
			// $key = key($tab);
			echo "tab: ";print_r($tab);

			$data = $this->db->select('*')
								->from($this->table)
							(for($i=0; $i<count($tab); $i++){
								echo '->where('.$tab[$i].','. $this->$tab[$i]);
							})();
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){

					foreach($row as $attrit=>$val){
						$donnees[$i][$attrit]=$val;
					}
			       
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				print_r($donnees);
				return $donnees;			
		
		}   */

	// recuperer un bien recois un string de nom de champs à recuperer dans la base

		public function recuperer($key){
			// $key = key($tab);

			$data = $this->db->select('*')
								->from($this->table)
								->where($key, $this->$key)
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){

			       	foreach($row as $attrit=>$val){
						$donnees[$i][$attrit]=$val;
					}
					$donnees[$i]['date'] = $this->Publication->recupDateByIdBien($donnees[$i]['id']);

			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;			
		
		}

		public function recupererPresentation(){
			$data = $this->db->select('*')
								->from($this->table)
								->order_by('id','DESC')
								// ->limit(6)
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
					$etat = $this->Publication->recupEtatByIdBien($row->id);
					if ( $etat != 'Traitement en cours') {
			       		continue;
			       	}
			       	foreach($row as $attrit=>$val){
			       		
						$donnees[$i][$attrit]=$val;
					}
					if($i == 6){
						break;
					}

			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
			

			if ($donnees['total'] != 0) {
			for ($i=0; $i < $donnees['total']; $i++) { 
				//on recupere l'image le nbre de peces et la superficie dans la table descripotion correspondante au bien
				// echo "data depart: <pre>"; print_r($donnees); echo "</pre>";

				$this->Description->hydrate(array('id'=>$donnees[$i]['id_description']));
				$details = $this->Description->recuperer('id');
				$details = $details[0];
				$image = json_decode($details['image']);
				$donnees[$i]['image'] = $image->{1}; //la premiere image uploadee est prise comme image principale
				// echo "data milieu: <pre>"; print_r($donnees); echo "</pre>";

				$donnees[$i]['nbrepiece'] = $details['nombre_piece'];
				$donnees[$i]['superficie'] = $details['superficie'];
				// $date = $this->Publication->recupDateByIdBien($donnees[$i]['id']);
				$donnees[$i]['date'] = $this->Publication->recupDateByIdBien($donnees[$i]['id']);
				$donnees[$i]['ville'] = $this->Localisation->VillebyId($donnees[$i]['id_localisation']);
				
			}

			return $donnees;
			}			
		}

	// recupIdByEtat recois un string l'etat du bien et retourne l'id du bien concerne

		public function recupIdByEtat(){
			// $key = key($tab);

			$data = $this->db->select('id')
								->from($this->table)
								->where('etat', $this->etat)
								->limit(1)
								->get()
								->result();	
				
				foreach ($data as $row){

			       	$donnees=$row->id;
				}
				
				return $donnees;			
		
		}

	// setteurs

		public function setId($id){
			$this->id=$id;
		}
		
		public function setId_user_proprio($id_user_proprio){
			$this->id_user_proprio=$id_user_proprio;
		}
		
		public function setId_user_utilisateur($id_user_utilisateur){
			$this->id_user_utilisateur=$id_user_utilisateur;
		}
		
		public function setId_localisation($id_localisation){
			$this->id_localisation=$id_localisation;
		}
		
		public function setId_description($id_description){
			$this->id_description=$id_description;
		}
		
		public function setId_reservation($id_reservation){
			$this->id_reservation=$id_reservation;
		}

		public function setId_equipement($id_equipement){
			$this->id_equipement=$id_equipement;
		}

		public function setPrix($prix){
			$this->prix=$prix;
		}

		public function setNombre_avis($nombre_avis){
			$this->nombre_avis=$nombre_avis;
		}

		public function setType($type){
			$this->type=$type;
		}
	
		public function setDocument($document){
			$this->document=$document;
		}
	
		public function setEtat($etat){
			$this->etat=$etat;
		}


	// getteurs

		public function getId(){
			return $this->id;
		}

		public function getId_user_proprio(){
			return $this->id_user_proprio;
		}

		public function getId_user_utilisateur(){
			return $this->id_user_utilisateur;
		}

		public function getId_localisation(){
			return $this->id_localisation;
		}

		public function getId_description(){
			return $this->id_description;
		}
		
		public function getId_reservation(){
			return $this->id_reservation;
		}

		public function getId_equipement(){
			return $this->id_equipement;
		}

		public function getPrix(){
			return $this->prix;
		}

		public function getNombre_avis(){
			return $this->nombre_avis;
		}
	
		public function getType(){
			return $this->type;
		}

		public function getDocument(){
			return $this->document;
		}

		public function getEtat(){
			return $this->etat;
		}
}


?>

