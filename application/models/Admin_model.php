<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Admin_model extends CI_Model{
		
		function __construct()
			{
			
			}
		
			// gerer un admin

			private $id;
			private $nom;
			private $email;
			private $password;
			private $telephone;
			private $profil;

			protected $table= 'admin';


			public function hydrate(array $donnees){
				foreach ($donnees as $key => $value){
					$method = 'set'.ucfirst($key);
					if (method_exists($this, $method)){
						$this->$method($value);
					}
				}
			}

			public function compte($where = array()){
				return (int) $this->db->where($where)->count_all_results($this->table);
			}



			public function addAdmin(){

			    $this->db->set('id', $this->id)
			    	->set('nom', $this->nom)
			    	->set('email', $this->email)
			    	->set('password', $this->password)
			    	->set('telephone', $this->telephone)
			    	->set('profil', $this->profil)
					->insert($this->table);
		
			}


			// fonction qui charge tous les Admins pour faire le filtrage de donnee
			
			public function findAllAdminBd(){
				$data = $this->db->select('id,nom,email,password,telephone,profil')
								->from($this->table)
								->order_by('id','desc')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['nom']=$row->nom;
			       	$donnees[$i]['email']=$row->email;
			       	$donnees[$i]['password']=$row->password;
			       	$donnees[$i]['telephone']=$row->telephone;
			       	$donnees[$i]['profil']=$row->profil;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;	
			}

			// fonction qui reccupère juste l'email d'un admin

			public function findAdminemail($id){
				$data =$this->db->select('email')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

								
				foreach ($data as $row){
			       	$donnees['email']=$row->email;
				}

				return $donnees['email'];
			}

			// fonction qui reccupère juste le password d'un admin

			public function findAdminpassword($id){
				$data =$this->db->select('password')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

					$donnees['password']='non';				
				foreach ($data as $row){
			       	$donnees['password']=$row->password;
				}

				return $donnees['password'];
			}
			

		    // fonction qui reccupère juste le nom d'un admin
			
			public function findAdminName($id){
				$data =$this->db->select('nom')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();

				foreach ($data as $row){
			       	$donnees['nom']=$row->nom;
				}

				return $donnees['nom'];
			}

			// fonction qui reccupère juste le profil d'un admin
			
			public function findProfil($id){
				$data =$this->db->select('profil')
								->from($this->table)
								->where('id', $id)
								->limit(1)
								->get()
								->result();
						
				foreach ($data as $row){
			       	$donnees['profil']=$row->profil;
				}

				return $donnees['profil'];
			}


			// fonction qui reccupère juste le password et email d'un admin

			public function findAdminInfos($email,$password){
				$data =$this->db->select('password,email')
						->from($this->table)
						->where(array('password'=>$password,'email'=>$email))
						->limit(1)
						->get()
						->result();

				$donnees['data']='non';			
				foreach ($data as $row){
			       	$donnees['email']=$row->email;
			       	$donnees['password']=$row->password;
			       	$donnees['data']='ok';
				}

				return $donnees;
			}



			// setteurs


			public function setId($id){
				$this->id=$id;
			}


			public function setNom($nom){
				$this->nom=$nom;
			}
			
			public function setEmail($email){
				$this->email=$email;
			}

			public function setPassword($password){
				$this->password=$password;
			}
			public function setTelephone($telephone){
				$this->telephone=$telephone;
			}
			public function setProfil($profil){
				$this->profil=$profil;
			}
			

			// getteurs

			public function getId(){
				return $this->id;
			
			}

			
			public function getNom(){
				return $this->nom;
			
			}

			public function getEmail(){
				return $this->email;
			
			}

			public function getPassword(){
				return $this->password;
			
			}
			public function getTelephone(){
				return $this->telephone;
			
			}
				public function getProfil(){
				return $this->profil;
			
			}
						
	
}


?>
