<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Services_model extends CI_Model{
		
	private $id;
	private $id_user;
	private $id_profil;
	private $maintenance_de_la_plateforme;
	private $demenagement;
	private $peinture;
	private $plomberie;
	private $maintenance;
	private $elaboration_de_contrats;
	private $assistance_juridique;
	private $contrat_de_bail ;
	private $gestion_location;
	private $assurance_locative;
	private $prospection;
	private $construction;

	protected $table= 'services';


	function __construct()
		{
			
		}
		
	// Hydrater un user

		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value){
				$method = 'set'.ucfirst($key);
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}


	// setteurs


		function __set($attribut,$valeur){    // setter universel

			$this->$attribut = $valeur;
			echo $this->$attribut;
		}

		public function setId($id){
			$this->id=$id;
		}

		public function setId_user($id_user){
			$this->id_user=$id_user;
		}

		public function setId_profil($id_profil){
			$this->id_profil=$id_profil;
		}
		
		public function setMaintenance_de_la_plateforme($maintenance_de_la_plateforme){
			$this->maintenance_de_la_plateforme=$maintenance_de_la_plateforme;
		}

		public function setDemenagement($demenagement){
			$this->demenagement=$demenagement;
		}
		
		public function setPeinture($peinture){
			$this->peinture=$peinture;
		}

		public function setPlomberie($plomberie){
			$this->plomberie=$plomberie;
		}
		
		public function setMaintenance($maintenance){
			$this->maintenance=$maintenance;
		}
		
		public function setElaboration_de_contrats($elaboration_de_contrats){
			$this->elaboration_de_contrats=$elaboration_de_contrats;
		}
		
		public function setAssistance_juridique($assistance_juridique){
			$this->assistance_juridique=$assistance_juridique;
		}
	
		public function setContrat_de_bail($contrat_de_bail){
			$this->contrat_de_bail=$contrat_de_bail;
		}

		public function setGestion_location($gestion_location){
			$this->gestion_location=$gestion_location;
		}

		public function setAssurance_locative($assurance_locative){
			$this->assurance_locative=$assurance_locative;
		}

		public function setProspection($prospection){
			$this->prospection=$prospection;
		}

		public function setConstruction($construction){
			$this->construction=$construction;
		}
		

	// getteurs


		public function get($nom){   // getter universel

			if (isset($this->$nom)) {
				return $this->$nom;
			}	
			else{
				return "Attribut << ".$nom." >> is undefined!";
			}
		}

		public function getId(){
			return $this->id;
		}
		
		public function getId_user(){
			return $this->id_user;
		}
		
		public function getId_profil(){
			return $this->id_profil;
		}

		public function getMaintenance_de_la_plateforme(){
			return $this->maintenance_de_la_plateforme;
		}
		
		public function getDemenagement(){
			return $this->demenagement;
		}

		public function getPeinture(){
			return $this->peinture;
		}
		public function getPlomberie(){
			return $this->plomberie;
		}

		public function getMaintenance(){
			return $this->maintenance;
		}
		
		public function getElaboration_de_contrats(){
			return $this->elaboration_de_contrats;
		}

		public function getAssistance_juridique(){
			return $this->assistance_juridique;
		}

		public function getContrat_de_bail(){
			return $this->contrat_de_bail;
		}

		public function getGestion_location(){
			return $this->gestion_location;
		}
		
		public function getAssurance_locative(){
			return $this->assurance_locative;
		}
			
		public function getProspection(){
			return $this->prospection;
		}
			
		public function getConstruction(){
			return $this->construction;
		}
	

	// liste des services rendus

		/* Retourne (dans une table) la liste des services rendus par l'utilisateur dont on a precise l'id
			structure de la reponse: [
										"data"=>"ok" (ou non si aucun service)
										"total"=>"un entier" (le nombre de services rendus)
										"services"=>['service1', 'service2'] (libele des services rendus)
									  ]

		*/
		public function liste_des_services_rendus(int $id):array {
			if (isset($id)) {

				$data = $this->db->select('*')
								->from($this->table)
								->where('id_user', $id)
								->limit(1)
								->get()
								->result();
				
				$i=0;
				$donnees['data'] = 'non';	
				foreach ($data[0] as $key=>$value){
					if($value == "ok"){
						$donnees['services'][]= str_replace("_", " ", ucfirst($key));
					
			       		$i++;
			       		$donnees['data']='ok';

			       	}
				}
				
				$donnees['total']=$i;  

				return $donnees;
				
			}
			else{
				return "ERROR: Unspecified id_user.";
			}
		}
}


?>
