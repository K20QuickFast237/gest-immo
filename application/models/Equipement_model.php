<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Equipement_model extends CI_Model{
		
	private $id;
	private $id_bien;
	private $eau_chaude;
	private $chauffage;
	private $climatisation;
	private $espace_evenement;
	private $classeur;
	private $autre;

	protected $table= 'equipement';


	function __construct()
		{
			
		}
		
	// Hydrater un equipement

		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value){
				$method = 'set'.ucfirst($key);
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}	

	// inserer un equipement

		public function inserer(array $data){

			foreach ($data as $key=>$value){
				$this->db->set($key, $this->$key);
			}

			$this->db->insert($this->table);

		
		}

	// recuperer recois un string representant un champ de la table et renvoie l'equipement' correspondant

		public function recuperer($key){
			// $key = key($tab);

			$data = $this->db->select('*')
								->from($this->table)
								// ->where($key, $tab[$key])
								->where($key, $this->$key)
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){

			       	foreach($row as $attrit=>$val){
						$donnees[$i][$attrit]=$val;
					}

			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;			
		
		}

	// recupIdByIdbien retourne l'id_bien de l'equipement concerne

		public function recupIdByIdbien(){

			$data = $this->db->select('id')
								->from($this->table)
								->where('id_bien', $this->id_bien)
								->limit(1)
								->get()
								->result();	
				
				foreach ($data as $row){

			       	$donnees=$row->id;
				}
				
				return $donnees;			
		
		}

	// setteurs
		

		public function __set($attribut,$valeur){
			
			if (!isset($this->$attribut)) {
				
				$this->$attribut = $valeur;
			}

		}


	//
		public function setId($id){
			$this->id=$id;
		}
		
		public function setId_bien($id_bien){
			$this->id_bien=$id_bien;
		}
		
		public function setEau_chaude($eau_chaude){
			$this->eau_chaude=$eau_chaude;
		}
		
		public function setChauffage($chauffage){
			$this->chauffage=$chauffage;
		}
		
		public function setClimatisation($climatisation){
			$this->climatisation=$climatisation;
		}

		public function setEspace_evenement($espace_evenement){
			$this->espace_evenement=$espace_evenement;
		}
	
		public function setClasseur($classeur){
			$this->classeur=$classeur;
		}
	
		public function setAutre($autre){
			$this->autre=$autre;
		}




	// getteurs

		public function get($nom){   // getter universel

			if (isset($this->$nom)) {
				return $this->$nom;
			}	
			else{
				return "Attribut << ".$nom." >> is undefined!";
			}
		}
	
	//

		public function getId(){
			return $this->id;
		}

		public function getId_bien(){
			return $this->id_bien;
		}

		public function getEau_chaude(){
			return $this->eau_chaude;
		}

		public function getChauffage(){
			return $this->chauffage;
		}

		public function getClimatisation(){
			return $this->climatisation;
		}
	
		public function getEspace_evenement(){
			return $this->espace_evenement;
		}

		public function getclasseur(){
			return $this->classeur;
		}

		public function getAutre(){
			return $this->autre;
		}


}


?>

