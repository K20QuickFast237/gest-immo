<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Publication_model extends CI_Model{
		
	private $id;
	private $id_user;
	private $id_bien;
	private $id_profil;
	private $type;
	private $prix;
	private $etat;
	private $date_creation;
	private $date_disponibilite;
	private $date_fin;

	private $duree;

	protected $table= 'publication';


	function __construct()
	{
		
	}
		
	// Hydrater un reservation

		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value){
				$method = 'set'.ucfirst($key);
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}

	// recuperer les publications non validées
		public function findAddedpublication(){
			// $key = key($tab);

			$data = $this->db->select('*')
								->from($this->table)
								->where('etat', 'Traitement en cours')
								->order_by('date_creation', 'DESC')
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){

			       	foreach($row as $attrit=>$val){
						$donnees[$i][$attrit]=$val;
					}

			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;			
		
		}

	//recuperer l'etat en fonction de l'id_bien
		public function recupEtatByIdBien($id_bien){
			if ($id_bien == 1) {
				return 'Traitement en cours';
			}

			$data = $this->db->select('etat')
							 ->from($this->table)
							 ->where('id_bien', $id_bien)
							 ->limit(1)
							 ->get()
							 ->result();	
				
				foreach ($data as $row){

			       	$donnees=$row->etat;
				}
				
				return $donnees;			
		
		}

	//recuperer date en fonction de l'id_bien
		public function recupDateByIdBien($id_bien){
			if ($id_bien == 1) {
				return '2021-11-18';
			}

			$data = $this->db->select('date_creation')
							 ->from($this->table)
							 ->where('id_bien', $id_bien)
							 ->limit(1)
							 ->get()
							 ->result();	
				
				foreach ($data as $row){

			       	$donnees=$row->date_creation;
				}
				
				return $donnees;			
		
		}

	// inserer une publication

		public function inserer(array $data){

			foreach ($data as $key=>$value){
				$this->db->set($key, $this->$key);
			}

			$this->db->insert($this->table);	
		}

	// update une publication par indiq

		public function update(array $data, array $indiq){
			$cle = key($indiq);

			foreach ($data as $key=>$value){
				if (isset($this->$key)) {
					$this->db->set($key, $this->$key)
						 ->where($cle, $indiq[$cle]);
				}else{
					$this->db->set($key, $data[$key])
						 ->where($cle, $indiq[$cle]);
				}
				
			}

			$this->db->update($this->table);			
		
		}

	// setteurs


		public function setId($id){
			$this->id=$id;
		}

		public function setId_user($id_user){
			$this->id_user=$id_user;
		}
		
		public function setId_bien($id_bien){
			$this->id_bien=$id_bien;
		}
		
		public function setId_profil($id_profil){
			$this->id_profil=$id_profil;
		}

		public function setType($type){
			$this->type=$type;
		}

		public function setPrix($prix){
			$this->prix=$prix;
		}

		public function setDate_creation($date_creation){
			$this->date_creation=$date_creation;
		}
		
		public function setDate_disponibilite($date_disponibilite){
			$this->date_disponibilite=$date_disponibilite;
		}

		public function setDate_fin($date_fin){
			$this->date_fin=$date_fin;
		}	

		public function setEtat($etat){
			$this->etat=$etat;
		}	

		public function setDuree($duree){
			$this->duree=$duree;
		}



	// getteurs

		public function getId(){
			return $this->id;
		}
		
		public function getId_user(){
			return $this->id_user;
		}

		public function getId_bien(){
			return $this->id_bien;
		}

		public function getId_profil(){
			return $this->id_profil;
		}

		public function getType(){
			return $this->type;
		}

		public function getPrix(){
			return $this->prix;
		}

		public function getDate_creation(){
			return $this->date_creation;
		}

		public function getdate_disponibilite(){
			return $this->date_disponibilite;
		}

		public function getDate_fin(){
			return $this->date_fin;
		}	

		public function getEtat(){
			return $this->etat;
		}
		
		public function getDuree(){
			return $this->duree;
		}	



		public function findlastpublication(){
			$data = $this->db->select('*')
							 ->from($this->table)
							 ->limit(1)
							 ->get()
							 ->result();

			$donnees['data'] = 'non';
			$i=0;
			foreach ($data as $row){
				$donnees[$i] = $row;
	       		$donnees['data']='ok';
				$i++;
			}
			$donnees['total'] = $i;

			return $donnees;
		}		



		public function findAllpublication($target){
			$data = $this->db->select('*')
							 ->from($this->table)
							 ->where('id_user', $target)
							 ->get()
							 ->result();

			$donnees['data'] = 'non';
			$i=0;
			foreach ($data as $row){
				$donnees[$i] = $row;
	       		$donnees['data']='ok';
	       		$i++;
			}
			$donnees['total'] = $i;

			return $donnees;
		}		

}


?>
