<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Localisation_model extends CI_Model{
		
	private $id;
	private $id_user;
	private $id_bien;
	private $pays;
	private $ville;
	private $adresse;
	private $longitude;
	private $latitude;

	protected $table= 'localisation';


	function __construct()
		{
			
		}
		
	// Hydrater un reservation

		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value){
				$method = 'set'.ucfirst($key);
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}

	// inserer une localisation

		public function inserer(array $data){

			foreach ($data as $key=>$value){
				$this->db->set($key, $this->$key);

 			}
			$this->db->insert($this->table);			
		
		}

	// update une localisation

		public function actualise(array $data,$id){

			foreach ($data as $key=>$value){
				$this->db->set($key, $this->$key)
						 ->where('id', $id);

 			}

			$this->db->update($this->table);			
		
		}

	// recuperer une localisation

		public function recuperer(array $tab){
			$key = key($tab);

			$data = $this->db->select('*')
						 ->from($this->table)
						 ->where($key, $tab[$key])
						 ->get()
						 ->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['id_user']=$row->id_user;
			       	$donnees[$i]['id_bien']=$row->id_bien;
			       	$donnees[$i]['pays']=$row->pays;
			       	$donnees[$i]['ville']=$row->ville;
			       	$donnees[$i]['adresse']=$row->adresse;
			       	$donnees[$i]['longitude']=$row->longitude;
			       	$donnees[$i]['latitude']=$row->latitude;
			       	
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;			
		
		}

	// recupIdByIdbien retourne l'id_bien de la localisation concerne

		public function recupIdByIdbien(){

			$data = $this->db->select('id')
								->from($this->table)
								->where('id_bien', $this->id_bien)
								->limit(1)
								->get()
								->result();	
				
				foreach ($data as $row){

			       	$donnees=$row->id;
				}
				
				return $donnees;			
		
		}

	// recuperer un nom de ville par son id

		public function VillebyId($id){

			$data = $this->db->select('ville')
						 ->from($this->table)
						 ->where('id', $id)
						 ->limit(1)
						 ->get()
						 ->result();

				
				foreach ($data as $row){
			       	$donnees['ville']=$row->ville;
			       	
				}
				
				return $donnees['ville'];			
		
		}

	// recuperer un nom de localite par son id

		public function AdressebyId($id){

			$data = $this->db->select('adresse')
						 ->from($this->table)
						 ->where('id', $id)
						 ->limit(1)
						 ->get()
						 ->result();

				
				foreach ($data as $row){
			       	$donnees['adresse']=$row->adresse;
			       	
				}
				
				return $donnees['adresse'];			
		
		}

	// setteurs

		public function setId($id){
			$this->id=$id;
		}

		public function setId_user($id_user){
			$this->id_user=$id_user;
		}
		
		public function setId_bien($id_bien){
			$this->id_bien=$id_bien;
		}

		public function setPays($pays){
			$this->pays=$pays;
		}
	
		public function setVille($ville){
			$this->ville=$ville;
		}

		public function setAdresse($adresse){
			$this->adresse=$adresse;
		}

		public function setLongitude($longitude){
			$this->longitude=$longitude;
		}

		public function setLatitude($latitude){
			$this->latitude=$latitude;
		}



	// getteurs

		public function getId(){
			return $this->id;
		}
		
		public function getId_user(){
			return $this->id_user;
		}

		public function getId_bien(){
			return $this->id_bien;
		}

		public function getPays(){
			return $this->pays;
		}
	
		public function getVille(){
			return $this->ville;
		}

		public function getAdresse(){
			return $this->adresse;
		}

		public function getLongitude(){
			return $this->longitude;
		}

		public function getLatitude(){
			return $this->latitude;
		}

}


?>

