<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Annonces extends CI_Controller {


		public function index()
		{	
			$this->Equipement->hydrate(array('id'=>1));
			$equip = $this->Equipement->recuperer('id');
			$equip = $equip[0];
			unset($equip['id'],$equip['id_bien'],$equip['autre']);
			$data['listeEquipement']= array_keys($equip);

			$this->load->view('Annonce/addAnnonce',$data);

		}

		public function saveAnnonce(){
			if (isset($_SESSION['typeAnnonce']) && ($_SESSION['typeAnnonce'] === 'Offre de Services')) {
				// code...
			}else{
				$this->saveAnnonceClassique();
			}
		}

		public function saveAnnonceClassique(){

			if (isset($_POST['confirmation']) && ($_POST['confirmation']==='ok')) {
				unset($_POST['confirmation']);
			
				$infoProfil;
				$infoDescription;
				$infoLocalisation;
				$infoEquipement = [];
				echo "<pre>"; print_r($_SESSION); echo "</pre>";
				echo "<pre>"; print_r($_POST); echo "</pre>";
				echo "<pre>"; print_r($_FILES); echo "</pre>";
				//traitements pour le profil
					if (isset($_FILES['imageProfil']) AND $_FILES['imageProfil']['error'] == 0 ){
		    			// Testons si le fichier n'est pas trop gros
		                if ($_FILES['imageProfil']['size'] <= 100000000){
		                    // Testons si l'extension est autorisée
		                    $infosfichier =pathinfo($_FILES['imageProfil']['name']);

							if ( preg_match("/jpeg/i", $infosfichier['extension']) + (preg_match("/jpg/i", $infosfichier['extension']) + preg_match("/png/i", $infosfichier['extension']) + preg_match("/gif/i", $infosfichier['extension']) ) !=0 ) {
			                    $config =$infosfichier['filename'].'-'.date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i').$_SESSION['user']['identity']['nom'].'-p';
								$ma_variable = str_replace('.', '_', $config);
								$ma_variable = str_replace(' ', '_', $config);
								$config = $ma_variable.'.'.$infosfichier['extension'];
								move_uploaded_file($_FILES['imageProfil']['tmp_name'],'assets/images/user_profil/'.$config);
								
								//infos a stocker en bd
								$infoProfil['image'] = $config;
							}else{
								// echo "erreur 3";
								$this->session->set_flashdata('errorProfil', 'Type de fichier non Autorisée');
                				// $this->load->view('Annonce/addAnnonce#profil');
		                		// redirect(site_url(array('Annonces','addAnnonce#profil')));
		                		$this->index();
							}
							
		                }else{
		                	// echo "erreur 4";
		                	$this->session->set_flashdata('errorProfil', 'Fichier trop Volumineux');
                			// $this->load->view('Annonce/addAnnonce#profil');
		                	// redirect(site_url(array('Annonces','addAnnonce#profil')));
		                	$this->index();
		                }
					}

					if (isset($_POST['description'])) {

						$infoProfil['description'] = htmlspecialchars(strip_tags(trim($_POST['description'])));
						$infoProfil['id_user'] = $_SESSION['user']['identity']['id'];
					}else{
						$this->index();
					}
				// echo "profil<pre>"; print_r($infoProfil); echo "</pre>fin profil";
				//traitements pour la description
					// traitement de toutes les images
					for ($i=1; $i <count($_FILES) ; $i++) { 

						//traitement de chaque image
						$name = 'imgBien'.$i;
						if (isset($_FILES[$name]) && $_FILES[$name]['error'] == 0) {
							if ($_FILES[$name]['size'] <= 100000000){
		                		// if ($_FILES['image']['size'] <= 10){
			                    // Testons si l'extension est autorisée
			                    $infosfichier =pathinfo($_FILES[$name]['name']);

								if ( preg_match("/jpeg/i", $infosfichier['extension']) + (preg_match("/jpg/i", $infosfichier['extension']) + preg_match("/png/i", $infosfichier['extension']) + preg_match("/gif/i", $infosfichier['extension']) ) !=0 ) {
				                    $config =$infosfichier['filename'].'-'.date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i').'-'.$_SESSION['user']['identity']['nom'].'-B';
									$ma_variable = str_replace('.', '_', $config);
									$ma_variable = str_replace(' ', '_', $config);
									$config = $ma_variable.'.'.$infosfichier['extension'];
									move_uploaded_file($_FILES[$name]['tmp_name'],'assets/images/user_bien/'.$config);
									
									//infos a stocker en bd
									$infoDescription['image'][$i] = $config;
								}else{
									// echo "erreur 5";
									$this->session->set_flashdata('errorDescription', 'Oups! Type de fichier non Autorisée');
                					// $this->index();
                					// $this->load->view('Annonce/addAnnonce#description');
			                		// redirect(site_url(array('Annonces','addAnnonce#description')));
			                		$this->index();
								}
							
			                }else{
			                	// echo "erreur 6";
			                	$this->session->set_flashdata('errorDescription', 'Oups! Fichier trop Volumineux');
                				// $this->index();
                				// $this->load->view('Annonce/addAnnonce#description');
			                	// redirect(site_url(array('Annonces','addAnnonce#description')));
			                	$this->index();
			                }
						
						}else{
						$this->index();
						}
					}
					$infoDescription['image'] = json_encode($infoDescription['image'], JSON_UNESCAPED_UNICODE);

					// traitement des descriptions autres
					$i = 1;
					$libele = 'autrelibele1';
					$valeur = 'autrevaleur1';
					$autre;
					$autre2;
					if (isset($_POST['autrelibele1']) && isset($_POST['autrevaleur1'])) {
						while(array_key_exists($libele, $_POST) && array_key_exists($valeur, $_POST)) {
							// on cree le tableau des autres descriptions
							$attri = htmlspecialchars(strip_tags(trim($_POST[$libele])));
							$vall = htmlspecialchars(strip_tags(trim($_POST[$valeur])));

							$autre[$attri] = $vall;

							$i++;
							$libele = 'autrelibele'.$i;
							$valeur = 'autrevaleur'.$i;
						}
						$infoDescription['autre'] = json_encode($autre, JSON_UNESCAPED_UNICODE);
					}else{
						$infoDescription['autre'] = json_encode(array(), JSON_UNESCAPED_UNICODE);
					}
					// traitement des champs de la table description
					if (!isset($_FILES['imgBien1'])) {
						$this->session->set_flashdata('errorDescription', 'Choisir au moins une image de Description');
                		// redirect(site_url(array('Annonces','index#description')));
                		$this->index();
                		// $this->load->view('Annonce/addAnnonce#description',$data);
					}
					$this->Description->hydrate(array('id'=>1));
					$champs = $this->Description->recuperer('id');
					$champs = $champs[0];
					unset($champs['id'],$champs['id_bien'],$champs['autre']);
					
					foreach($champs as $key=>$value){
						echo "$key";
						if (isset($_POST[$key])) {
							echo "$_POST[$key]";
							$infoDescription = array_merge($infoDescription, array($key=>htmlspecialchars(strip_tags($_POST[$key]))));
						}		
					}
				// echo "description<pre>"; print_r($infoDescription); echo "</pre>fin description";
				//traitements pour la localisation
					if (isset($_POST['pays']) && isset($_POST['ville']) && isset($_POST['adresse'])) {
						$infoLocalisation['pays'] = htmlspecialchars(strip_tags(trim($_POST['pays'])));
						$infoLocalisation['ville'] = htmlspecialchars(strip_tags(trim($_POST['ville'])));
						$infoLocalisation['adresse'] = htmlspecialchars(strip_tags(trim($_POST['adresse'])));

						if (isset($_POST['longitude'])) {
							$infoLocalisation['longitude'] = htmlspecialchars(strip_tags(trim($_POST['longitude'])));
						}
						if (isset($_POST['latitude'])) {
							$infoLocalisation['latitude'] = htmlspecialchars(strip_tags(trim($_POST['latitude'])));
						}
						
					}else{
						// redirection vers le formulaire d'inscription avec une session flash
						$this->session->set_flashdata("errorLocalisation","Oups! Des donnees manquent.");
                		// $this->load->view('Annonce/addAnnonce#localisation');
						redirect(site_url(array('Annonces','addAnnonce#localisation')));

					} 
					// echo "localisation<pre>"; print_r($infoLocalisation); echo "</pre>fin localisation";
				//traitements pour les equipements

					//traitement des champs de la table equipement
					$this->Equipement->hydrate(array('id'=>1));
					$champs = $this->Equipement->recuperer('id');
					$champs = $champs[0];
					unset($champs['id'],$champs['id_bien'],$champs['autre']);
					
					foreach($champs as $key=>$value){
						if (isset($_POST[$key])) {
							$infoEquipement = array_merge($infoEquipement, array($key=>htmlspecialchars(strip_tags($_POST[$key]))));
							unset($_POST[$key]);
						}		
					}
					//equipements autres
					$equip = array_keys($_POST, 'ok');
			 		foreach($equip as $row){
			 			$autre2[$row] = 'ok';
			 		}
			 		if (!empty($equip)) {
			 			$infoEquipement['autre'] = json_encode($autre2, JSON_UNESCAPED_UNICODE);
			 		}else{
			 			$infoEquipement['autre'] = json_encode(array(), JSON_UNESCAPED_UNICODE);
			 		} 
					// echo "equipement<pre>"; print_r($infoEquipement); echo "</pre>fin equipement";
			 		
			 	//insertion des donnes
					// echo "<br>infoProfil: <pre>"; print_r($infoProfil); echo "<pre> fin profil";
					// echo "<br>infoDescription: <pre>"; print_r($infoDescription); echo "<pre> fin description";
					// echo "<br>infoLocalisation: <pre>"; print_r($infoLocalisation); echo "<pre> fin localisation";
					// echo "<br>infoEquipement: <pre>"; print_r($infoEquipement); echo "<pre> fin Equipement";
	
					$date = date("Y-m-d H:i:s");
					// $_SESSION['date'] = $date;
					$bien['prix'] = $infoDescription['prix'];
					$bien['type'] = htmlspecialchars($_POST['type']);
					$bien['etat'] = $date;
					// $bien['etat'] = $_SESSION['date'];
					$this->Bien->hydrate($bien);
					$this->Bien->inserer($bien);
					$bien['id'] = $this->Bien->recupIdByEtat();

					switch ($_SESSION['typeAnnonce']) {
						case 'Echange':
							$bien['etat'] = 'A Echanger';
							$bien['id_user_proprio'] = $_SESSION['user']['identity']['id'];
							$bien['id_user_utilisateur'] = $_SESSION['user']['identity']['id'];
							unset($_SESSION['typeAnnonce']);
							$this->User->hydrate(array('id'=>$_SESSION['user']['identity']['id']));
							$this->User->update(array('nombre_annonce'=>$_SESSION['user']['identity']['nombre_annonce']+1),array('id'=>$_SESSION['user']['identity']['id']));
							//update de la validite abonnement
							$this->Abonnement->hydrate(array('id_user'=>$_SESSION['user']['identity']['id']));
							$_SESSION['user']['identity']['etat_abonnement'] = $this->Abonnement->upgrade('id_user');
							break;

						case 'Location':
							$bien['etat'] = 'A Louer';
							$bien['id_user_proprio'] = $_SESSION['user']['identity']['id'];
							unset($_SESSION['typeAnnonce']);
							//update du niveau
							if ($_SESSION['user']['identity']['niveau'] <= 2) {
								$_SESSION['user']['identity']['niveau'] = 2;
								$this->User->hydrate(array('id'=>$_SESSION['user']['identity']['id']));
								$this->User->update(array('niveau'=>2),array('id'=>$_SESSION['user']['identity']['id']));
							}
							//update du nombre d'annonce
							$this->User->update(array('nombre_annonce'=>$_SESSION['user']['identity']['nombre_annonce']+1),array('id'=>$_SESSION['user']['identity']['id']));
							//update de la validite abonnement
							$this->Abonnement->hydrate(array('id_user'=>$_SESSION['user']['identity']['id']));
							$_SESSION['user']['identity']['etat_abonnement'] = $this->Abonnement->upgrade('id_user');
							break;

						case 'Vente':
							$bien['etat'] = 'A Vendre';
							$bien['id_user_proprio'] = $_SESSION['user']['identity']['id'];
							unset($_SESSION['typeAnnonce']);
							//update du niveau
							if ($_SESSION['user']['identity']['niveau'] <= 2) {
								$_SESSION['user']['identity']['niveau'] = 2;
								$this->User->hydrate(array('id'=>$_SESSION['user']['identity']['id']));
								$this->User->update(array('niveau'=>2),array('id'=>$_SESSION['user']['identity']['id']));
							}
							//update du nombre d'annonce
							$this->User->hydrate(array('id'=>$_SESSION['user']['identity']['id']));
							$this->User->update(array('nombre_annonce'=>$_SESSION['user']['identity']['nombre_annonce']+1),array('id'=>$_SESSION['user']['identity']['id']));
							//update de la validite abonnement
							$this->Abonnement->hydrate(array('id_user'=>$_SESSION['user']['identity']['id']));
							$_SESSION['user']['identity']['etat_abonnement'] = $this->Abonnement->upgrade('id_user');
							break;
						
						default:
							break;
					}
					/*echo "<br>infoProfil: <pre>"; print_r($infoProfil); echo "<pre> fin profil";
					echo "<br>infoDescription: <pre>"; print_r($infoDescription); echo "<pre> fin description";
					echo "<br>infoLocalisation: <pre>"; print_r($infoLocalisation); echo "<pre> fin localisation";
					echo "<br>infoEquipement: <pre>"; print_r($infoEquipement); echo "<pre> fin Equipement";
					echo "<br>bien :<pre>"; print_r($bien ); echo "<pre> fin bien ";//*/
	
					//remplissage de la table profil
					$infoProfil['id_bien'] = $bien['id'];
					$infoProfil['id_user'] = $_SESSION['user']['identity']['id'];
					echo "<pre>"; print_r($infoProfil); echo "</pre>";
					$this->Profil->hydrate($infoProfil);
					$this->Profil->inserer($infoProfil);

					//remplissage de la table description
					$infoDescription['id_bien'] = $bien['id'];
					echo "<pre>"; print_r($infoDescription); echo "</pre>";
					$this->Description->hydrate($infoDescription);
					$this->Description->inserer($infoDescription);
					$bien['id_description'] = $this->Description->recupIdByIdbien('id_bien');

					//remplissage de la table localisation
					$infoLocalisation['id_bien'] = $bien['id'];
					echo "<pre>"; print_r($infoLocalisation); echo "</pre>";
					$this->Localisation->hydrate($infoLocalisation);
					$this->Localisation->inserer($infoLocalisation);
					$bien['id_localisation'] = $this->Localisation->recupIdByIdbien('id_bien');

					//remplissage de la table equipement
					$infoEquipement['id_bien'] = $bien['id'];
					echo "<pre>"; print_r($infoEquipement); echo "</pre>";
					$this->Equipement->hydrate($infoEquipement);
					$this->Equipement->inserer($infoEquipement);
					$bien['id_equipement'] = $this->Equipement->recupIdByIdbien('id_bien');
					$this->Bien->hydrate($bien);
					$this->Bien->update($bien); 
				//*/
				// redirection
					$this->session->set_flashdata('registered','Merci d\'avoir utilisé cette platte forme.');
					redirect(site_url(array('Home','index')));

			}
		}

		public function ajoutAnnonce(){
			$_SESSION['ajoutAnnonce'] = "true";
			$_SESSION['listeTypeBien'] = $this->Typebien->recuperer();
			redirect(site_url(array('Annonces','addAnnonce')));
		}

		public function addAnnonce(){
			$_SESSION['etape'] = 'profil';
			// echo "la session typeAnnonce: ";  print_r($_SESSION['typeAnnonce']);
			if (!isset($_SESSION['etape'])) {
					$_SESSION['etape'] = "";
				}

			if(isset($_SESSION['typeAnnonce']) && ($_SESSION['typeAnnonce'] == 'Offre de Services')){

				echo "etapes pour offre de service dans Annonces/addAnnonce";
				unset($_SESSION['typeAnnonce']);
			}else{

				switch ($_SESSION['etape']) {
					case 'profil':
						unset($_SESSION['etape']);
						$this->addDescription();
						break;
					
					case 'description':
						unset($_SESSION['etape']);
						$this->addLocalisationBien();
						break;
					
					case 'localisation':
						unset($_SESSION['etape']);
						$this->addEquipement();
						break;
					
					default:
						$this->addProfil();
						break;
				}
			}
		}


		public function recupType(){

			if (!empty($_POST)) { //definit la session

				$_SESSION['typeAnnonce'] = $_POST['type'];
				echo json_encode(array('status'=>'ok'));
			}else{
				echo json_encode(array('status'=>'aborted'));

			}
		} 
		
		public function listeTypeBien(){
			return array('listeTypeBien'=>$this->Typebien->recuperer());
		} 

		public function addBien(){

		} 

		public function addEquipement(){
			echo "etape equipement";

		} 

		public function addDescription(){

			if (isset($_SESSION['status']) && (preg_match("/termine/i", $_SESSION['status']) != 0)){
				unset($_SESSION['status']);
				$this->session->set_flashdata('etape', 'description');
				redirect(site_url(array('Annonces','addAnnonce')));
				
			}else{
				//on charge les vues.
				$data['right'] = $this->load->view('Annonce/description','',TRUE);
				$data['left'] = $this->load->view('Annonce/typeAnnonce','',TRUE);

				$this->load->view('Annonce/addTemplate',$data);
			}
		} 

		public function saveDescription(){
			print_r($_POST);
			print_r($_FILES);
		} 

		public function addLocalisationBien(){
			echo "etape localisation";

		} 

		public function addLocalisationUser(){
			echo "etape description";

		} 

		public function addProfil(){
			if (isset($_SESSION['status']) && (preg_match("/termine/i", $_SESSION['status']) != 0)){

				unset($_SESSION['status']);
				if(isset($_SESSION['typeAnnonce']) && ($_SESSION['typeAnnonce'] == 'Offre de Services')){
					echo "redirection pour prochaine etape d'ajout d'annonce offre de service dans Annonces/addProfil";
				}else{
					$this->session->set_flashdata('etape', 'profil');
					redirect(site_url(array('Annonces','addAnnonce')));
				}
				
				
			}else{
				//on charge les vues.
				if (isset($_SESSION['user']['profil'])) {
					$profil = $_SESSION['user']['profil'];
					$data['right'] = $this->load->view('Annonce/profil', array('profil'=>$profil), TRUE);
				}else{
					$data['right'] = $this->load->view('Annonce/profil','',TRUE);
				}

				$data['left'] = $this->load->view('Annonce/typeAnnonce','',TRUE);

				$this->load->view('Annonce/addTemplate',$data);
			}
			 
		} 

		public function saveProfil(){
			$infoProfil;
			if (isset($_FILES['image']) AND $_FILES['image']['error'] == 0 ){
    			// Testons si le fichier n'est pas trop gros
                if ($_FILES['image']['size'] <= 100000000){
                // if ($_FILES['image']['size'] <= 10){
                    // Testons si l'extension est autorisée
                    $infosfichier =pathinfo($_FILES['image']['name']);

					if ( preg_match("/jpeg/i", $infosfichier['extension']) + (preg_match("/jpg/i", $infosfichier['extension']) + preg_match("/png/i", $infosfichier['extension']) + preg_match("/gif/i", $infosfichier['extension']) ) !=0 ) {
	                    // $extension_upload = $infosfichier['extension'];
	                    print_r($infosfichier);
	                    $config =$infosfichier['filename'].'-'.date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i').$_SESSION['user']['identity']['nom'].'-p';
							$ma_variable = str_replace('.', '_', $config);
						$ma_variable = str_replace(' ', '_', $config);
						$config = $ma_variable.'.'.$infosfichier['extension'];
						move_uploaded_file($_FILES['image']['tmp_name'],'assets/images/user_profil/'.$config);
						//infos a stocker en bd
						$infoProfil['image'] = $config;
					}else{
						$this->session->set_flashdata('error', 'Type de fichier non Autorisée');
                		redirect(site_url(array('Annonces','addProfil')));
					}
					
                }else{

                	$this->session->set_flashdata('error', 'Fichier trop Volumineux');
                	redirect(site_url(array('Annonces','addProfil')));
                }
			}else{
				$this->session->set_flashdata('error', 'Erreur de chargement du fichier');
                redirect(site_url(array('Annonces','addProfil')));
			}

			if (isset($_POST['description'])) {

				$infoProfil['description'] = $_POST['description'];
				$_SESSION['user']['identity']['id'] = 1;
				$infoProfil['id_user'] = $_SESSION['user']['identity']['id'];

				$this->Profil->hydrate($infoProfil);
				$this->Profil->inserer($infoProfil);

				$this->session->set_flashdata('status', 'Termine');
           		redirect(site_url(array('Annonces','addProfil')));
			}
			
		}  

		public function saveProfiluser(){
			
			// if (isset($_POST['profil'])) {
			if (!empty($_POST)) {
			
				$infoProfil;
				if (isset($_FILES['image']) AND $_FILES['image']['error'] == 0 ){
	    			// Testons si le fichier n'est pas trop gros
	                if ($_FILES['image']['size'] <= 100000000){
	                // if ($_FILES['image']['size'] <= 10){
	                    // Testons si l'extension est autorisée
	                    $infosfichier =pathinfo($_FILES['image']['name']);

						if ( (preg_match("/jpg/i", $infosfichier['extension']) + preg_match("/jpg/i", $infosfichier['extension']) + preg_match("/jpg/i", $infosfichier['extension']) ) !=0 ) {
		                    // $extension_upload = $infosfichier['extension'];
		                    print_r($infosfichier);
		                    $config =$infosfichier['filename'].'-'.date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i').$_SESSION['user']['identity']['nom'].'-p';
								$ma_variable = str_replace('.', '_', $config);
							$ma_variable = str_replace(' ', '_', $config);
							$config = $ma_variable.'.'.$infosfichier['extension'];
							move_uploaded_file($_FILES['image']['tmp_name'],'assets/images/user_profil/'.$config);
							//infos a stocker en bd
							$infoProfil['image'] = $config;
						}else{
							$this->session->set_flashdata('error', 'Type de fichier non Autorisée');
	                		redirect(site_url(array('Annonces','addProfil')));
						}
						
	                }else{

	                	$this->session->set_flashdata('error', 'Fichier trop Volumineux');
	                	redirect(site_url(array('Annonces','addProfil')));
	                }
				}else{
					$this->session->set_flashdata('error', 'Erreur de chargement du fichier');
	                redirect(site_url(array('Annonces','addProfil')));
				}

				if (isset($_POST['description'])) {

					$infoProfil['description'] = $_POST['description'];
					$_SESSION['user']['identity']['id'] = 1;
					$infoProfil['id_user'] = $_SESSION['user']['identity']['id'];

					$this->Profil->hydrate($infoProfil);
					$this->Profil->update($infoProfil, 'id_user');

					//etape suivante
					$this->session->set_flashdata('status', 'Termine');
	           		redirect(site_url(array('Annonces','addProfil')));
				}

			}else{
				
				//etape suivante
				$this->session->set_flashdata('status', 'Termine');
	           	redirect(site_url(array('Annonces','addProfil')));
			}

		
		} 

		public function addContact(){

		} 	
	//

		
	}
?>
