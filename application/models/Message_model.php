<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Message_model extends CI_Model{
		
	private $id;
	private $id_user_emetteur;
	private $id_user_cible;
	private $contenu;
	private $date_creation;
	private $etat;

	protected $table= 'message';


	function __construct()
		{
			
		}
		
	// Hydrater un reservation

		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value){
				$method = 'set'.ucfirst($key);
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}


	// setteurs


		public function setId($id){
			$this->id=$id;
		}

		public function setId_user_emetteur($id_user_emetteur){
			$this->id_user_emetteur=$id_user_emetteur;
		}
		
		public function setId_user_cible($id_user_cible){
			$this->id_user_cible=$id_user_cible;
		}

		public function setContenu($contenu){
			$this->contenu=$contenu;
		}

		public function setDate_creation($date_creation){
			$this->date_creation=$date_creation;
		}

		public function setEtat($etat){
			$this->etat=$etat;
		}


	// getteurs

		public function getId(){
			return $this->id;
		}
		
		public function getId_user_emetteur(){
			return $this->id_user_emetteur;
		}

		public function getId_user_cible(){
			return $this->id_user_cible;
		}

		public function getContenu(){
			return $this->contenu;
		}

		public function getDate_creation(){
			return $this->date_creation;
		}

		public function getEtat(){
			return $this->etat;
		}

}


?>

