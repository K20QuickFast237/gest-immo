<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class TypeAnnonce_model extends CI_Model{
		
	private $id;
	private $nom;

	protected $table= 'typeannonce';


	function __construct()
		{
			
		}
		
	// Hydrater un user

		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value){
				$method = 'set'.ucfirst($key);
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}	

	// inserer un typeannonce

		public function inserer($nom = ''){
			if (empty($nom)) {
				$this->db->set('nom', $this->nom)
						 ->insert($this->table);
			}else{
				$this->db->set('nom', $nom)
						 ->insert($this->table);
			}
		
		}	

	// recuperer un typeannonce 

		public function recuperer(){

			$data = $this->db->select('*')
								->from($this->table)
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['nom']=$row->nom;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;			
		
		}


	// setters
		public function setId($id){
			$this->id=$id;
		}


		public function setNom($nom){
			$this->nom=$nom;
		}

	// getteurs

		public function getId(){
			return $this->id;
		}
		
		public function getNom(){
			return $this->nom;
		}
					
	
}


?>
