<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class User_model extends CI_Model{
		
	private $id;
	private $nom;
	private $email;
	private $password;
	private $id_contact;
	private $id_profil;
	private $id_localisation;
	private $niveau;
	private $nombre_publication;
	private $nombre_avis ;
	private $nombre_mesg_non_lu;
	private $nombre_notification;
	private $nombre_annonce;
	private $etat_abonnement;
	private $date_inscription;

	protected $table= 'user';


	function __construct()
		{
			
		}
		
	// Hydrater un user

		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value){
				$method = 'set'.ucfirst($key);
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}	

	// inserer un user

		public function inserer(array $data){

			foreach ($data as $key=>$value){
				$this->db->set($key, $this->$key);
			}

			$this->db->insert($this->table);			
		
		}	

	// update un user par id

		public function actualise(array $data,$id){

			foreach ($data as $key=>$value){
				$this->db->set($key, $this->$key)
						 ->where('id', $id);
			}

			$this->db->update($this->table);			
		
		}	

	// update un user par indiq

		public function update(array $data, array $indiq){
			$cle = key($indiq);

			foreach ($data as $key=>$value){
				if (isset($this->$key)) {
					$this->db->set($key, $this->$key)
						 ->where($cle, $indiq[$cle]);
				}else{
					$this->db->set($key, $data[$key])
						 ->where($cle, $indiq[$cle]);
				}
				
			}

			$this->db->update($this->table);			
		
		}

	// recuperer un user 

		public function recuperer(array $tab){
			$key = key($tab);

			$data = $this->db->select('*')
								->from($this->table)
								->where($key, $tab[$key])
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){
			       	$donnees[$i]['id']=$row->id;
			       	$donnees[$i]['nom']=$row->nom;
			       	$donnees[$i]['email']=$row->email;
			       	$donnees[$i]['password']=$row->password;
			       	$donnees[$i]['id_contact']=$row->id_contact;
			       	$donnees[$i]['id_profil']=$row->id_profil;
			       	$donnees[$i]['id_localisation']=$row->id_localisation;
			       	$donnees[$i]['id_services']=$row->id_services;
			       	$donnees[$i]['niveau']=$row->niveau;
			       	$donnees[$i]['nombre_publication']=$row->nombre_publication;
			       	$donnees[$i]['nombre_avis']=$row->nombre_avis;
			       	$donnees[$i]['nombre_mesg_non_lu']=$row->nombre_mesg_non_lu;
			       	$donnees[$i]['nombre_notification']=$row->nombre_notification;
			       	$donnees[$i]['nombre_annonce']=$row->nombre_annonce;
			       	$donnees[$i]['etat_abonnement']=$row->etat_abonnement;
			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;			
		
		}

	// recuperer tous les users
		public function recupererAll(){
			$data = $this->db->select('*')
					->from($this->table)
					->get()
					->result();

			$i=0;
			$donnees['data'] = 'non';	
			
			foreach ($data as $row){

		       	foreach($row as $attrit=>$val){
					$donnees[$i][$attrit]=$val;
				}

		       	$i++;
		       	$donnees['data']='ok';
			}
			
			$donnees['total']=$i;
			return $donnees;			
		}

	// existance d'un email retourne true ou false
		public function email_exist($email){
			$data =$this->db->select('email')
							->from($this->table)
							->where('email', $email)
							->limit(1)
							->get()
							->result();
					// var_dump($data);
							
			foreach ($data as $row){
		       	$donnees['email']=$row->email;
			}

			if(isset($donnees['email'])){
				return TRUE;
				// return "true";
			}else{
				return FALSE;
				// return "false";
			}
				
		}
	// setteurs


		public function setId($id){
			$this->id=$id;
		}


		public function setNom($nom){
			$this->nom=$nom;
		}
		
		public function setEmail($email){
			$this->email=$email;
		}

		public function setPassword($password){
			$this->password=$password;
		}

		public function setId_contact($id_contact){
			$this->id_contact=$id_contact;
		}
		public function setId_profil($id_profil){
			$this->id_profil=$id_profil;
		}
		public function setId_localisation($id_localisation){
			$this->id_localisation=$id_localisation;
		}
		
		public function setNiveau($niveau){
			$this->niveau=$niveau;
		}
		
		public function setNombre_publication($nombre_publication){
			$this->nombre_publication=$nombre_publication;
		}
		
		public function setNombre_avis($nombre_avis){
			$this->nombre_avis=$nombre_avis;
		}
		
		public function setNombre_mesg_non_lu($nombre_mesg_non_lu){
			$this->nombre_mesg_non_lu=$nombre_mesg_non_lu;
		}
		public function setNombre_notification($nombre_notification){
			$this->nombre_notification=$nombre_notification;
		}
		public function setNombre_annonce($nombre_annonce){
			$this->nombre_annonce=$nombre_annonce;
		}
		public function setEtat_abonnement($etat_abonnement){
			$this->etat_abonnement=$etat_abonnement;
		}
		public function setDate_inscription($date_inscription){
			$this->date_inscription=$date_inscription;
		}
		

	// getteurs

		public function getId(){
			return $this->id;
		}
		
		public function getNom(){
			return $this->nom;
		}

		public function getEmail(){
			return $this->email;
		}

		public function getPassword(){
			return $this->password;
		}

		public function getId_contact(){
			return $this->id_contact;
		}
		public function getId_localisation(){
			return $this->id_localisation;
		}

		public function getNiveau(){
			return $this->niveau;
		}

		public function getNombre_publication(){
			return $this->nombre_publication;
		}

		public function getNombre_avis(){
			return $this->nombre_avis;
		}

		public function getNombre_mesg_non_lu(){
			return $this->nombre_mesg_non_lu;
		}

		public function getNombre_notification(){
			return $this->nombre_notification;
		}

		public function getNombre_annonce(){
			return $this->nombre_annonce;
		}
			
		public function getEtat_abonnement(){
			return $this->etat_abonnement;
		}
			
		public function getDate_inscription(){
			return $this->date_inscription;
		}
					
	
}


?>
