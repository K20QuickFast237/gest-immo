<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Avis_model extends CI_Model{
		
	private $id;
	private $id_user_emetteur;
	private $id_user_cible;
	private $id_bien;
	private $note;
	private $commentaire;
	private $date_creation;

	protected $table= 'avis';


	function __construct()
		{
			
		}
		
	// Hydrater un reservation

		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value){
				$method = 'set'.ucfirst($key);
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}



	// setteurs

		public function setId($id){
			$this->id=$id;
		}
		
		public function setId_user_emetteur($id_user_emetteur){
			$this->id_user_emetteur=$id_user_emetteur;
		}
		
		public function setId_user_cible($id_user_cible){
			$this->id_user_cible=$id_user_cible;
		}
		
		public function setId_bien($id_bien){
			$this->id_bien=$id_bien;
		}
		
		public function setNote($note){
			$this->note=$note;
		}

		public function setCommentaire($commentaire){
			$this->commentaire=$commentaire;
		}
	
		public function setDate_creation($date_creation){
			$this->date_creation=$date_creation;
		}


	// getteurs

		public function getId(){
			return $this->id;
		}

		public function getId_user_emetteur(){
			return $this->id_user_emetteur;
		}

		public function getId_user_cible(){
			return $this->id_user_cible;
		}

		public function getId_bien(){
			return $this->id_bien;
		}

		public function getNote(){
			return $this->note;
		}
	
		public function getCommentaire(){
			return $this->commentaire;
		}

		public function getDate_creation(){
			return $this->date_creation;
		}

}


?>

