<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Profil_model extends CI_Model{
		
	private $id;
	private $id_user;
	private $id_bien;
	private $id_services;
	private $image;
	private $description;
	private $date_creation;

	protected $table= 'profil';


	function __construct()
		{
			
		}
		
	// Hydrater un reservation

		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value){
				$method = 'set'.ucfirst($key);
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}

	// inserer un profil

		public function inserer(array $data){

			foreach ($data as $key=>$value){
				$this->db->set($key, $this->$key);
			}

			$this->db->insert($this->table);			
			
			$data = $this->db->select('id')
							 ->from($this->table)
							 ->where('id_bien', $this->id_bien)
							 ->limit(1)
							 ->get()
							 ->result();	
				
				foreach ($data as $row){

			       	$donnees=$row->id;
				}
				
				return $donnees;
		}

	// update un profil

		public function update(array $data, string $where){

			foreach ($data as $key=>$value){
				$this->db->set($key, $this->$key);
			}

			$this->db->where($where, $this->$where)			
					 ->update($this->table);			
		
		}	


	// setteurs


		public function setId($id){
			$this->id=$id;
		}

		public function setId_user($id_user){
			$this->id_user=$id_user;
		}

		public function setId_bien($id_bien){
			$this->id_bien=$id_bien;
		}
		
		public function setId_services($id_services){
			$this->id_services=$id_services;
		}

		public function setImage($image){
			$this->image=$image;
		}
		public function setDescription($description){
			$this->description=$description;
		}
		public function setDate_creation($date_creation){
			$this->date_creation=$date_creation;
		}



	// getteurs

		public function getId(){
			return $this->id;
		}
		
		public function getId_user(){
			return $this->id_user;
		}
		
		public function getId_bien(){
			return $this->id_bien;
		}

		public function getId_services(){
			return $this->id_services;
		}

		public function getImage(){
			return $this->image;
		}

		public function getDescription(){
			return $this->description;
		}

		public function getDate_creation(){
			return $this->date_creation;
		}

}


?>
