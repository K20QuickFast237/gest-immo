	<?php
	if ( !defined('BASEPATH')) exit('No direct script access allowed'); 


class Description_model extends CI_Model{
		
	private $id;
	private $id_bien;
	private $superficie;
	private $nombre_piece;
	private $prix;
	private $image;
	private $autre;
	private $nombre_chambre;
	private $nombre_salle_manger;

	protected $table= 'description';


	function __construct()
		{
			
		}
		
	// Hydrater un reservation

		public function hydrate(array $donnees){
			foreach ($donnees as $key => $value){
				$method = 'set'.ucfirst($key);
				if (method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}

	// inserer une description

		public function inserer(array $data){

			foreach ($data as $key=>$value){
				$this->db->set($key, $this->$key);
			}

			$this->db->insert($this->table);			
		
		}

	// recuperer recois un string representant un champ de la table et renvoie la description correspondante

		public function recuperer($key){
			// $key = key($tab);

			$data = $this->db->select('*')
								->from($this->table)
								// ->where($key, $tab[$key])
								->where($key, $this->$key)
								->get()
								->result();

				$i=0;
				$donnees['data'] = 'non';	
				
				foreach ($data as $row){

			       	foreach($row as $attrit=>$val){
						$donnees[$i][$attrit]=$val;
					}

			       	$i++;
			       	$donnees['data']='ok';
				}
				
				$donnees['total']=$i;
				return $donnees;			
		
		}

	// recupIdByIdbien retourne l'id_bien de la Description concerne

		public function recupIdByIdbien(){

			$data = $this->db->select('id')
								->from($this->table)
								->where('id_bien', $this->id_bien)
								->limit(1)
								->get()
								->result();	
				
				foreach ($data as $row){

			       	$donnees=$row->id;
				}
				
				return $donnees;			
		
		}

	// setteurs

		public function setId($id){
			$this->id=$id;
		}
		
		public function setId_bien($id_bien){
			$this->id_bien=$id_bien;
		}
		
		public function setSuperficie($superficie){
			$this->superficie=$superficie;
		}
		
		public function setNombre_piece($nombre_piece){
			$this->nombre_piece=$nombre_piece;
		}
		
		public function setPrix($prix){
			$this->prix=$prix;
		}
		
		public function setImage($image){
			$this->image=$image;
		}
		
		public function setAutre($autre){
			$this->autre=$autre;
		}

		public function setNombre_chambre($nombre_chambre){
			$this->nombre_chambre=$nombre_chambre;
		}
	
		public function setNombre_salle_manger($nombre_salle_manger){
			$this->nombre_salle_manger=$nombre_salle_manger;
		}



	// getteurs

		public function getId(){
			return $this->id;
		}

		public function getId_bien(){
			return $this->id_bien;
		}

		public function getSuperficie(){
			return $this->superficie;
		}

		public function getNombre_piece(){
			return $this->nombre_piece;
		}

		public function getPrix(){
			return $this->prix;
		}

		public function getImage(){
			return $this->image;
		}

		public function getAutre(){
			return $this->autre;
		}
	
		public function getNombre_chambre(){
			return $this->nombre_chambre;
		}

		public function getNombre_salle_manger(){
			return $this->nombre_salle_manger;
		}

}


?>

