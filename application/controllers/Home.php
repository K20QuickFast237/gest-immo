<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Home extends CI_Controller {

	private $data;
	private $trouve;
		
		public function index()
		{
			if (isset($this->data['resultatrecherche'])) {
				if ( $this->data['resultatrecherche']['total'] != 0 ) {
					$data['resultatrecherche'] = $this->data['resultatrecherche'];
				}else{
					$data['resultatrecherche'] = '';
				}
			}
			$val = $this->Typebien->recuperer();
			$data['recherchepopulaire'] = $this->Bien->recupererPresentation();
			$data['listeTypeBien'] = $val['liste'];		
			$this->load->view('HOME/header');
			$this->load->view('HOME/index', $data);
			$this->load->view('HOME/footer'); 
		} 

		public function rechercher() {
			// echo "<pre>";
			// print_r($_POST);
			// echo "</pre>";
			if (isset($_POST)) {
				// echo "le Post: <pre>"; print_r($_POST); echo "</pre><br>";
			 	// if (TRUE) {
				if (!empty($_POST['type'])) {

					$this->data['type'] = $_POST['type'];
					$this->Bien->hydrate($this->data);
					$trouve = $this->Bien->recuperer('type');

					// echo "Premier trouve dans type <br><pre>"; print_r($trouve); echo "</pre><br>";
					if ($trouve['data']=='ok') {
						$this->data = [];
						$this->data = $trouve;
					}else{
						$this->data['total'] = 0;
						$this->data['data'] = 'non';
					}
					// echo "dans type: <br><pre>"; print_r($this->data); echo "</pre><br>";

				}

				if (!empty($_POST['prix']) && ($this->data['total'] != 0)) {
					$result = [];
					$cpt = 0;
					// filtrons data
					for ($i=0; $i < $this->data['total']; $i++) { 
						if ($this->data[$i]['prix'] <= $_POST['prix']) {
							$result[] = $this->data[$i]; 
							$cpt++;
						}
					}
					$this->data = []; // on vide data
					//puis on la reformate avec les valeurs filtrees
					$this->data = $result;
					$this->data['data'] = 'ok';
					$this->data['total'] = $cpt;
				// echo "Dans prix: <br><pre>"; print_r($this->data); echo "</pre><br>";

				}

				if (!empty($_POST['action']) && ($this->data['total'] != 0)) {
					$result = [];
					$cpt = 0;
					// filtrons data
					for ($i=0; $i < $this->data['total']; $i++) { 
						$pattern = "/".$_POST['action']."/i";
						if (preg_match($pattern, $this->data[$i]['etat']) != 0) {
						// if ($this->data[$i]['etat'] == $_POST['action']) {
							$result[] = $this->data[$i]; 
							$cpt++;
						}
					}
					$this->data = []; // on vide data
					//puis on la reformate avec les valeurs filtrees
					$this->data = $result;
					$this->data['data'] = 'ok';
					$this->data['total'] = $cpt;
				// echo "dans action: <br><pre>"; print_r($this->data); echo "</pre><br>";

				}

				if (!empty($_POST['ville']) && ($this->data['total'] != 0)) {
					// echo "dans ville";
					// echo "la data dans ville: <br><pre>"; print_r($this->data); echo "</pre><br>";
					$result = [];
					$cpt = 0;
					// filtrons data
					for ($i=0; $i < $this->data['total']; $i++) { 

						$ville = htmlspecialchars(strip_tags($_POST['ville']));
						$pattern = "/".$ville."/i";
						$localisation = $this->Localisation->VillebyId($this->data[$i]['id_localisation']);
						$localite = $this->Localisation->AdressebyId($this->data[$i]['id_localisation']);
						if (preg_match($pattern, $localisation) != 0) {  // validation de l'element
							$result[] = $this->data[$i]; 
							$cpt++;
						}else if (preg_match($pattern, $localite) != 0) {  // validation de l'element
							$result[] = $this->data[$i]; 
							$cpt++;
						}
					}
					$this->data = []; // on vide data
					//puis on la reformate avec les valeurs filtrees
					$this->data = $result;
					$this->data['data'] = 'ok';
					$this->data['total'] = $cpt;

				// echo "Dans ville: <br><pre>"; print_r($this->data); echo "</pre><br>";

				}

				if (isset($this->data['type'])) {
					unset($this->data['type']);
				}
				// echo "A la fin: <br><pre>"; print_r($this->data); echo "</pre><br>";
				// return $this->data;
				if ($this->data['total']!=0) {
					for ($i=0; $i < $this->data['total']; $i++) { 
						//on recupere l'image le nbre de peces et la superficie dans la table descripotion correspondante au bien
						// echo "data depart: <pre>"; print_r($this->data); echo "</pre>";

						$this->Description->hydrate(array('id'=>$this->data[$i]['id_description']));
						$details = $this->Description->recuperer('id');
						$details = $details[0];
						$image = json_decode($details['image']);
						$this->data[$i]['image'] = $image->{1}; //la premiere image uploadee est prise comme image principale
						// echo "data milieu: <pre>"; print_r($this->data); echo "</pre>";

						$this->data[$i]['nbrepiece'] = $details['nombre_piece'];
						$this->data[$i]['superficie'] = $details['superficie'];
						$this->data[$i]['ville'] = $this->Localisation->VillebyId($this->data[$i]['id_localisation']);
						
					}
					$data = $this->data;
					$this->data = [];
					$this->session->set_flashdata('registered','recherches termminées');
					$this->data['resultatrecherche'] = $data;
					$this->index();
					
				}else{
					$this->data['resultatrecherche'] = array('data'=>'ok', 'total'=>0);
					$this->session->set_flashdata('registered','recherches termminées');
					$this->index();
				}
			}
		}

		public function saveAnnonce(){
			if (isset($_SESSION['typeAnnonce']) && ($_SESSION['typeAnnonce'] === 'Offre de Services')) {
				// code...
			}else{
				$this->saveAnnonceClassique();
			}
		}

		public function saveAnnonceClassique(){
			/*if (isset($_POST)) {
				// echo "<br>post: <pre>"; print_r($_POST); echo "<pre> fin post";
				foreach($_POST as $key=>$value){
					$_SESSION['annonce'][$key] = $value;
				}
				// echo "<br>session: <pre>"; print_r($_SESSION); echo "<pre> fin session";

			}*/
			$_SESSION['user']['identity']['nombre_annonce']++;
			if (isset($_POST['confirmation']) && ($_POST['confirmation']==='ok')) {
				unset($_POST['confirmation']);
			
				$infoProfil;
				$infoDescription;
				$infoLocalisation;
				$infoEquipement = [];
				// echo "<pre>"; print_r($_SESSION); echo "</pre>";
				// echo "<pre>"; print_r($_POST); echo "</pre>";
				// echo "<pre>"; print_r($_FILES); echo "</pre>";
				//traitements pour le profil
					if (isset($_FILES['imageProfil']) AND $_FILES['imageProfil']['error'] == 0 ){
		    			// Testons si le fichier n'est pas trop gros
		                if ($_FILES['imageProfil']['size'] <= 100000000){
		                    // Testons si l'extension est autorisée
		                    $infosfichier =pathinfo($_FILES['imageProfil']['name']);

							if ( preg_match("/jpeg/i", $infosfichier['extension']) + (preg_match("/jpg/i", $infosfichier['extension']) + preg_match("/png/i", $infosfichier['extension']) + preg_match("/gif/i", $infosfichier['extension']) ) !=0 ) {
			                    $config =$infosfichier['filename'].'-'.date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i').$_SESSION['user']['identity']['nom'].'-p';
								$ma_variable = str_replace('.', '_', $config);
								$ma_variable = str_replace(' ', '_', $config);
								$config = $ma_variable.'.'.$infosfichier['extension'];
								move_uploaded_file($_FILES['imageProfil']['tmp_name'],'assets/images/user_profil/'.$config);
								
								//infos a stocker en bd
								$infoProfil['image'] = $config;
							}else{
								// echo "erreur 3";
								$this->session->set_flashdata('errorProfil', 'Type de fichier non Autorisée');
                				// $this->load->view('Annonce/addAnnonce#profil');
		                		// redirect(site_url(array('Annonces','addAnnonce#profil')));
		                		$this->index();
							}
							
		                }else{
		                	// echo "erreur 4";
		                	$this->session->set_flashdata('errorProfil', 'Fichier trop Volumineux');
                			// $this->load->view('Annonce/addAnnonce#profil');
		                	// redirect(site_url(array('Annonces','addAnnonce#profil')));
		                	$this->index();
		                }
					}

					if (isset($_POST['description'])) {

						$infoProfil['description'] = htmlspecialchars(strip_tags(trim($_POST['description'])));
						$infoProfil['id_user'] = $_SESSION['user']['identity']['id'];
					}else{
						$this->index();
					}
				// echo "profil<pre>"; print_r($infoProfil); echo "</pre>fin profil";
				//traitements pour la description
					// traitement de toutes les images
					for ($i=1; $i <count($_FILES) ; $i++) { 

						//traitement de chaque image
						$name = 'imgBien'.$i;
						if (isset($_FILES[$name]) && $_FILES[$name]['error'] == 0) {
							if ($_FILES[$name]['size'] <= 100000000){
		                		// if ($_FILES['image']['size'] <= 10){
			                    // Testons si l'extension est autorisée
			                    $infosfichier =pathinfo($_FILES[$name]['name']);

								if ( preg_match("/jpeg/i", $infosfichier['extension']) + (preg_match("/jpg/i", $infosfichier['extension']) + preg_match("/png/i", $infosfichier['extension']) + preg_match("/gif/i", $infosfichier['extension']) ) !=0 ) {
				                    $config =$infosfichier['filename'].'-'.date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i').'-'.$_SESSION['user']['identity']['nom'].'-B';
									$ma_variable = str_replace('.', '_', $config);
									$ma_variable = str_replace(' ', '_', $config);
									$config = $ma_variable.'.'.$infosfichier['extension'];
									move_uploaded_file($_FILES[$name]['tmp_name'],'assets/images/user_bien/'.$config);
									
									//infos a stocker en bd
									$infoDescription['image'][$i] = $config;
								}else{
									// echo "erreur 5";
									$this->session->set_flashdata('errorDescription', 'Oups! Type de fichier non Autorisée');
                					// $this->index();
                					// $this->load->view('Annonce/addAnnonce#description');
			                		// redirect(site_url(array('Annonces','addAnnonce#description')));
			                		$this->index();
								}
							
			                }else{
			                	// echo "erreur 6";
			                	$this->session->set_flashdata('errorDescription', 'Oups! Fichier trop Volumineux');
                				// $this->index();
                				// $this->load->view('Annonce/addAnnonce#description');
			                	// redirect(site_url(array('Annonces','addAnnonce#description')));
			                	$this->index();
			                }
						
						}else{
						$this->index();
						}
					}
					$infoDescription['image'] = json_encode($infoDescription['image'], JSON_UNESCAPED_UNICODE);

					// traitement des descriptions autres
					$i = 1;
					$libele = 'autrelibele1';
					$valeur = 'autrevaleur1';
					$autre;
					$autre2;
					if (isset($_POST['autrelibele1']) && isset($_POST['autrevaleur1'])) {
						while(array_key_exists($libele, $_POST) && array_key_exists($valeur, $_POST)) {
							// on cree le tableau des autres descriptions
							$attri = htmlspecialchars(strip_tags(trim($_POST[$libele])));
							$vall = htmlspecialchars(strip_tags(trim($_POST[$valeur])));

							$autre[$attri] = $vall;

							$i++;
							$libele = 'autrelibele'.$i;
							$valeur = 'autrevaleur'.$i;
						}
						$infoDescription['autre'] = json_encode($autre, JSON_UNESCAPED_UNICODE);
					}else{
						$infoDescription['autre'] = json_encode(array(), JSON_UNESCAPED_UNICODE);
					}
					// traitement des champs de la table description
					if (!isset($_FILES['imgBien1'])) {
						$this->session->set_flashdata('errorDescription', 'Choisir au moins une image de Description');
                		// redirect(site_url(array('Annonces','index#description')));
                		$this->index();
                		// $this->load->view('Annonce/addAnnonce#description',$data);
					}
					$this->Description->hydrate(array('id'=>1));
					$champs = $this->Description->recuperer('id');
					$champs = $champs[0];
					unset($champs['id'],$champs['id_bien'],$champs['autre']);
					
					foreach($champs as $key=>$value){
						if (isset($_POST[$key])) {
							// echo "$_POST[$key]";
							$infoDescription = array_merge($infoDescription, array($key=>htmlspecialchars(strip_tags($_POST[$key]))));
						}		
					}
				// echo "description<pre>"; print_r($infoDescription); echo "</pre>fin description";
				//traitements pour la localisation
					if (isset($_POST['pays']) && isset($_POST['ville']) && isset($_POST['adresse'])) {
						$infoLocalisation['pays'] = htmlspecialchars(strip_tags(trim($_POST['pays'])));
						$infoLocalisation['ville'] = htmlspecialchars(strip_tags(trim($_POST['ville'])));
						$infoLocalisation['adresse'] = htmlspecialchars(strip_tags(trim($_POST['adresse'])));

						if (isset($_POST['longitude'])) {
							$infoLocalisation['longitude'] = htmlspecialchars(strip_tags(trim($_POST['longitude'])));
						}
						if (isset($_POST['latitude'])) {
							$infoLocalisation['latitude'] = htmlspecialchars(strip_tags(trim($_POST['latitude'])));
						}
						
					}else{
						// redirection vers le formulaire d'inscription avec une session flash
						$this->session->set_flashdata("errorLocalisation","Oups! Des donnees manquent.");
                		// $this->load->view('Annonce/addAnnonce#localisation');
						redirect(site_url(array('Annonces','addAnnonce#localisation')));

					} 
					// echo "localisation<pre>"; print_r($infoLocalisation); echo "</pre>fin localisation";
				//traitements pour les equipements

					//traitement des champs de la table equipement
					$this->Equipement->hydrate(array('id'=>1));
					$champs = $this->Equipement->recuperer('id');
					$champs = $champs[0];
					unset($champs['id'],$champs['id_bien'],$champs['autre']);
					
					foreach($champs as $key=>$value){
						if (isset($_POST[$key])) {
							$infoEquipement = array_merge($infoEquipement, array($key=>htmlspecialchars(strip_tags($_POST[$key]))));
							unset($_POST[$key]);
						}		
					}
					//equipements autres
					$equip = array_keys($_POST, 'ok');
			 		foreach($equip as $row){
			 			$autre2[$row] = 'ok';
			 		}
			 		if (!empty($equip)) {
			 			$infoEquipement['autre'] = json_encode($autre2, JSON_UNESCAPED_UNICODE);
			 		}else{
			 			$infoEquipement['autre'] = json_encode(array(), JSON_UNESCAPED_UNICODE);
			 		} 
					// echo "equipement<pre>"; print_r($infoEquipement); echo "</pre>fin equipement";
			 		
			 	//insertion des donnes
					// echo "<br>infoProfil: <pre>"; print_r($infoProfil); echo "<pre> fin profil";
					// echo "<br>infoDescription: <pre>"; print_r($infoDescription); echo "<pre> fin description";
					// echo "<br>infoLocalisation: <pre>"; print_r($infoLocalisation); echo "<pre> fin localisation";
					// echo "<br>infoEquipement: <pre>"; print_r($infoEquipement); echo "<pre> fin Equipement";
	
					$date = date("Y-m-d H:i:s");
					// $_SESSION['date'] = $date;
					$bien['prix'] = $infoDescription['prix'];
					$bien['type'] = htmlspecialchars($_POST['type']);
					$bien['etat'] = $date;
					// $bien['etat'] = $_SESSION['date'];
					$this->Bien->hydrate($bien);
					$this->Bien->inserer($bien);
					$bien['id'] = $this->Bien->recupIdByEtat();

					switch ($_SESSION['typeAnnonce']) {
						case 'Echange':
							$bien['etat'] = 'A Echanger';
							$bien['id_user_proprio'] = $_SESSION['user']['identity']['id'];
							$bien['id_user_utilisateur'] = $_SESSION['user']['identity']['id'];
							$this->User->hydrate(array('id'=>$_SESSION['user']['identity']['id']));
							$this->User->update(array('nombre_annonce'=>($_SESSION['user']['identity']['nombre_annonce'])),array('id'=>$_SESSION['user']['identity']['id']));
							//update de la validite abonnement
							$this->Abonnement->hydrate(array('id_user'=>$_SESSION['user']['identity']['id']));
							$_SESSION['user']['identity']['etat_abonnement'] = $this->Abonnement->upgrade('id_user');
							break;

						case 'Location':
							$bien['etat'] = 'A Louer';
							$bien['id_user_proprio'] = $_SESSION['user']['identity']['id'];
							//update du niveau
							if ($_SESSION['user']['identity']['niveau'] <= 2) {
								$_SESSION['user']['identity']['niveau'] = 2;
								$this->User->hydrate(array('id'=>$_SESSION['user']['identity']['id']));
								$this->User->update(array('niveau'=>2),array('id'=>$_SESSION['user']['identity']['id']));
							}
							//update du nombre d'annonce
							$this->User->update(array('nombre_annonce'=>($_SESSION['user']['identity']['nombre_annonce'])),array('id'=>$_SESSION['user']['identity']['id']));
							//update de la validite abonnement
							$this->Abonnement->hydrate(array('id_user'=>$_SESSION['user']['identity']['id']));
							$_SESSION['user']['identity']['etat_abonnement'] = $this->Abonnement->upgrade('id_user');
							break;

						case 'Vente':
							$bien['etat'] = 'A Vendre';
							$bien['id_user_proprio'] = $_SESSION['user']['identity']['id'];
							//update du niveau
							if ($_SESSION['user']['identity']['niveau'] <= 2) {
								$_SESSION['user']['identity']['niveau'] = 2;
								$this->User->hydrate(array('id'=>$_SESSION['user']['identity']['id']));
								$this->User->update(array('niveau'=>2),array('id'=>$_SESSION['user']['identity']['id']));
							}
							//update du nombre d'annonce
							$this->User->hydrate(array('id'=>$_SESSION['user']['identity']['id']));
							$this->User->update(array('nombre_annonce'=>($_SESSION['user']['identity']['nombre_annonce'])),array('id'=>$_SESSION['user']['identity']['id']));
							//update de la validite abonnement
							$this->Abonnement->hydrate(array('id_user'=>$_SESSION['user']['identity']['id']));
							$_SESSION['user']['identity']['etat_abonnement'] = $this->Abonnement->upgrade('id_user');
							break;
						
						default:
							break;
					}
					/*echo "<br>infoProfil: <pre>"; print_r($infoProfil); echo "<pre> fin profil";
					echo "<br>infoDescription: <pre>"; print_r($infoDescription); echo "<pre> fin description";
					echo "<br>infoLocalisation: <pre>"; print_r($infoLocalisation); echo "<pre> fin localisation";
					echo "<br>infoEquipement: <pre>"; print_r($infoEquipement); echo "<pre> fin Equipement";
					echo "<br>bien :<pre>"; print_r($bien ); echo "<pre> fin bien ";/*/
	
					//remplissage de la table profil
					$infoProfil['id_bien'] = $bien['id'];
					$infoProfil['id_user'] = $_SESSION['user']['identity']['id'];
					// echo "<pre>"; print_r($infoProfil); echo "</pre>";
					$this->Profil->hydrate($infoProfil);
					$idProfil = $this->Profil->inserer($infoProfil);

					//remplissage de la table description
					$infoDescription['id_bien'] = $bien['id'];
					// echo "<pre>"; print_r($infoDescription); echo "</pre>";
					$this->Description->hydrate($infoDescription);
					$this->Description->inserer($infoDescription);
					$bien['id_description'] = $this->Description->recupIdByIdbien('id_bien');

					//remplissage de la table localisation
					$infoLocalisation['id_bien'] = $bien['id'];
					// echo "<pre>"; print_r($infoLocalisation); echo "</pre>";
					$this->Localisation->hydrate($infoLocalisation);
					$this->Localisation->inserer($infoLocalisation);
					$bien['id_localisation'] = $this->Localisation->recupIdByIdbien('id_bien');

					//remplissage de la table equipement
					$infoEquipement['id_bien'] = $bien['id'];
					// echo "<pre>"; print_r($infoEquipement); echo "</pre>";
					$this->Equipement->hydrate($infoEquipement);
					$this->Equipement->inserer($infoEquipement);
					//remplissage de la table bien
					$bien['id_equipement'] = $this->Equipement->recupIdByIdbien('id_bien');
					$this->Bien->hydrate($bien);
					$this->Bien->update($bien); 

					//remplissage de la table publication avec l'etat invalide
					$publication['id_user'] = $_SESSION['user']['identity']['id'];
					$publication['id_bien'] = $bien['id'];
					$publication['id_profil'] = $idProfil;
					$publication['type'] = $_SESSION['typeAnnonce'];
					unset($_SESSION['typeAnnonce']);
					$publication['etat'] = 'Traitement en cours';
					$publication['date_disponibilite'] = $_SESSION['disponibilite'];
					unset($_SESSION['disponibilite']);

					$this->Publication->hydrate($publication);
					$this->Publication->inserer($publication);
				//*/
				// redirection
					// $this->session->set_flashdata('registered','Merci d\'avoir utilisé cette platte forme.');
					$this->session->set_flashdata('registered','Annonce ajoutee, Personnalisez la dans votre espace personnel.');
					// redirect(site_url(array('Home','index')));
					// $val = $this->Typebien->recuperer();
					// $data['listeTypeBien'] = $val['liste'];
					// $this->load->view('HOME/header');
					// $this->load->view('HOME/index', $data);
					// $this->load->view('HOME/footer');
					$this->index();

			}
		}
        public function register()
		{
			
		$this->load->view('HOME/header1');
		$this->load->view('HOME/register');
		$this->load->view('HOME/footer1');

		}  

		public function recover()
		{
			
		$this->load->view('HOME/header1');
		$this->load->view('HOME/recover');
		$this->load->view('HOME/footer1');

		}  
		/* Cette fonction s'utilise avec une session de email*/
		public function Change_password()
		{	
		$this->load->view('HOME/header1');
		$this->load->view('HOME/newPassword');
		$this->load->view('HOME/footer1');
		}  

		public function login()
		{
		if (isset($_SESSION['user']['identity'])) {
			redirect(site_url(array('Home','index')));
		}
			
		$this->load->view('HOME/header1');
		$this->load->view('HOME/login');
		$this->load->view('HOME/footer1');

		}


		public function echanger() 
		{

		$this->load->view('HOME/header');
		$this->load->view('HOME/echanger');
		$this->load->view('HOME/footer');
		}

		public function acheter() 
		{

		$this->load->view('HOME/header');
		$this->load->view('HOME/acheter');
		$this->load->view('HOME/footer');
		}

		public function louer() 
		{

		$this->load->view('HOME/header');
		$this->load->view('HOME/louer');
		$this->load->view('HOME/footer');
		}

		public function service() 
		{

		$this->load->view('HOME/header');
		$this->load->view('HOME/service');
		$this->load->view('HOME/footer');
		}

		public function recupType(){

			if (!empty($_POST)) { //definit la session

				$_SESSION['typeAnnonce'] = $_POST['type'];
				$_SESSION['disponibilite'] = $_POST['date'];
				echo json_encode(array('status'=>'ok','message'=>[$_SESSION['typeAnnonce'],$_SESSION['disponibilite']]));
			}else{
				echo json_encode(array('status'=>'aborted'));

			}
		} 
		
	}
?>