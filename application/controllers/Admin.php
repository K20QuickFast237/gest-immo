<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Admin extends CI_Controller {

		public function index(){
			// $this->load->view('dashbord/Template_header');
			// $this->load->view('dashbord/Template_body');
			// $this->load->view('dashbord/details_annonce_echange');
			// $this->load->view('dashbord/Template_footer');
			$this->allAnnonces();
		}

		public function lastAnnonces(){
			$data['lastannonces'] = $this->Publication->findlastpublication();
			print_r($data);
		}


		public function allAnnonces(){
			// effacer la ligne de definition session ci dessous car elle a ete utilise pour une simulation
			// $_SESSION['user']['identity'] = 1;
			$data['allannonces'] = $this->Publication->findAllpublication( $_SESSION['user']['identity']['id'] );
			$this->load->view('dashbord/Template_header');
			$this->load->view('dashbord/Template_body');
			$this->load->view('dashbord/allAnnonces',$data);
			$this->load->view('dashbord/Template_footer');
		}	

		public function publication(){
			// effacer la ligne de definition session ci dessous car elle a ete utilise pour une simulation
			// $_SESSION['user']['identity'] = 1;
			$data['publication'] = $this->Publication->findAddedpublication();
			// echo "<pre>"; print_r($data); echo "</pre>";
			
			$this->load->view('dashbord/Template_header');
			$this->load->view('dashbord/Template_body');
			$this->load->view('dashbord/publication',$data);
			$this->load->view('dashbord/Template_footer');
		}


		public function listeProriete(){
			$this->Bien->hydrate(array('id_user_proprio'=>$_SESSION['user']['identity']['id']));
			$data['bien'] = $this->Bien->recuperer('id_user_proprio');

			for ($i=0; $i < $data['bien']['total'] ; $i++) { 
            	if (isset($data['bien'][$i])) {
            		$data['bien'][$i]['localisation'] = $this->Localisation->VillebyId($data['bien'][$i]['id_localisation']);
            		unset($data['bien'][$i]['id_localisation']);
            	}
            }
			// echo "<pre>"; print_r($data); echo "</pre>";
			
			$this->load->view('dashbord/Template_header');
			$this->load->view('dashbord/Template_body');
			$this->load->view('dashbord/listePropriete',$data);
			$this->load->view('dashbord/Template_footer');
			
		}

		public function Abonnement(){
			$this->Abonnement->hydrate( array('id_user'=>$_SESSION['user']['identity']['id']));
			$data['allannonces'] = $this->Abonnement->recuperer('id_user');
			$data['allannonces'] = $data['allannonces'][0];
			$this->load->view('dashbord/Template_header');
			$this->load->view('dashbord/Template_body');
			$this->load->view('dashbord/Abonnement',$data);
			$this->load->view('dashbord/Template_footer');
		}

		Public function updateAbonnement(){
			$this->Abonnement->hydrate(array('id_user'=>$_SESSION['user']['identity']['id']));
			$_SESSION['user']['identity']['etat_abonnement'] = $this->Abonnement->upgrade('id_user', $_POST['duree']);
			redirect(site_url(array('Admin','Abonnement')));
			// $this->Abonnement();
		}

		Public function disablePublication(){
			$this->Publication->update(array('etat'=>'disabled'), array('id'=>$_POST['id']));
			redirect(site_url(array('Admin','publication')));
			// $this->Abonnement();
		}


		//Deconnexion
		public function deconnexion(){
		   	session_destroy();
		   	redirect(site_url(array('Home','index')));
		}

		
	}