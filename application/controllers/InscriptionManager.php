<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class InscriptionManager extends CI_Controller {

		public function index(){
			// traitement de soumission du formulaire
			if (!empty($_POST['nom']) && !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['confirmpassword']) && isset($_POST['verified']) && ($_POST['verified'] == 'ok')) {
				if ($_POST['password'] == $_POST['confirmpassword']) {
					if (!$this->User->email_exist(strip_tags($_POST['email']))) {
						$random = $this->randomise();
						$data['random'] =  hash('sha512', $random);	
						$data['password'] = hash('sha512', $_POST['password']);
						$data['email'] = strip_tags($_POST['email']);
						$data['nom'] = strip_tags($_POST['nom']);

						$existant = $this->Connexion->recuperer(array('email'=>$data['email']));
						// echo "<pre>"; print_r($existant); echo "</pre>";
						if ($existant['total'] != 0) {
							$this->Connexion->delete(array('email'=>$data['email']));
						}
						$this->Connexion->saveData($data);

						// $mailcontent = "Cliquez <a href='".base_url()."InscriptionManager/validate?random=".$data['random'].">ICI</a> pour confirmer votre inscription.";
						$mailcontent = "Cliquez sur le lien ci-dessous pour valider votre inscription: \r\n'".base_url()."InscriptionManager/validate?random=".$data['random'].".";
						// echo "Validez cet email: <br>".$mailcontent;

						/*
							Envoyons le mail contenant le message 
							stocké dans $mailcontent //*/
							$to  = $data['email'];

							$headers = "From: vvymmo@gmail.com";
							$body = "Message: ".$mailcontent." \r\n";

							mail($to, "Confirmation Inscription", $body, $headers);

						/*	Puis redirigeons vers la page d'accueil avec un toast : un Email de confirmation vous a ete envoye
						*/
						$this->session->set_flashdata('registered','Un Mail de confirmation vous a été envoyé');
						redirect(site_url(array('Home','index')));
					}else{
						$this->session->set_flashdata('error','Cet Email est déjà utilisé');
						$this->load->view('HOME/header1');
						$this->load->view('HOME/register');
						$this->load->view('HOME/footer1');
					}
				}else{
					$this->session->set_flashdata('error','vos informations sont incorrectes');
					$this->load->view('HOME/header1');
					$this->load->view('HOME/register');
					$this->load->view('HOME/footer1');
					// redirect(site_url(array('Home','register'))); //redirection vers le formulaire d'inscription
				}
			}else{
				$this->session->set_flashdata('error','Validez la verification');
				$this->load->view('HOME/header1');
				$this->load->view('HOME/register');
				$this->load->view('HOME/footer1');
				// redirect(site_url(array('Home','register'))); //redirection vers le formulaire d'inscription
			}

		}

		public function validate(){
			//traitement du lien de confirmation
			if (!empty($_GET['random'])) {
				$user = $this->Connexion->recuperer(array('random'=>$_GET['random']));
				
				if ($user['total']!=0) {
					unset($user[0]['random']);
					print_r($user);
					$this->User->hydrate($user[0]);
					$this->User->inserer($user[0]);
					$user = $this->User->recuperer(array('email'=>$user[0]['email']));
					unset($user[0]['password']);

					$this->Connexion->delete(array('random'=>$_GET['random']));

					$_SESSION['user']['identity'] = $user[0];
					//on cree un abonnement
					$this->Abonnement->hydrate(array('id_user'=>$_SESSION['user']['identity']['id']));
					$_SESSION['user']['identity']['etat_abonnement'] = $this->Abonnement->upgrade('id_user');
					// $_SESSION['user']['identity']['etat_abonnement'] = $this->Abonnement->upgrade($_SESSION['user']['identity']['id']);
					//on redirige
					$this->session->set_flashdata('registered','Bienvenue');
					redirect(site_url(array('Home', 'index'))); //redirection vers la page d'accueil tout simplement. 
				}else{
					$this->session->set_flashdata('error','Votre adresse email a déjà été validéé, Veuillez vous connecter');
					redirect(site_url(array('Home','login'))); //redirection vers le formulaire d'inscription
				}
			}else{
				$this->session->set_flashdata('error','Compte introuvable');
				redirect(site_url(array('Home','login'))); //redirection vers le formulaire d'inscription
			}


		}

		public function recuperation(){
			//traitement du lien de confirmation
			if (!empty($_GET['random'])) {
				$user = $this->Connexion->recuperer(array('random'=>$_GET['random']));
				print_r($user);
				if ($user['total']==1) {
					$this->session->set_flashdata('email',$user[0]['email']);
					$this->Connexion->delete(array('random'=>$_GET['random']));

					redirect(site_url(array('HOME','Change_password'))); 
			 	}else{
					$this->session->set_flashdata('toastError','Votre lien a été endommagé reprenez la procedure');
					redirect(site_url(array('Home','index'))); //redirection vers le formulaire d'inscription
				}
			}else{
				$this->session->set_flashdata('error','adresse mail introuvable');
				redirect(site_url(array('Home','login'))); //redirection vers le formulaire d'inscription 
			}

		}

		public function saveRecuperation(){
			//traitement du lien de confirmation
			if ($_POST['password'] == $_POST['confirmpassword']) {
				echo "<pre>"; print_r($_POST); echo "</pre>";
				$pass = htmlspecialchars(strip_tags($_POST['password']));
				$pass = hash('sha512', $pass);
				$this->User->update(array('password'=>$pass),array('email'=>$_POST['email']));
				echo "fin modification";
				$this->connecte();
				$this->session->set_flashdata('registered','Votre mot de passe à été correctement mis a jour');
				redirect(site_url(array('HOME','index')));
			}else{	
				$this->load->view('HOME/header1');
				$this->load->view('HOME/newPassword');
				$this->load->view('HOME/footer1');
			}
		}

		public function recupere(){
			//traitement du lien de confirmation
			
			$email = htmlspecialchars(strip_tags($_POST['email']));
			
			$user = $this->User->recuperer(array('email'=>$email));
			echo "<pre>"; print_r($user); echo "</pre>";
			if ($user['total']!=0) {
				//on cree un nombre aleatoire 
				$random = $this->randomise(9);
				//on stocke dans la table connexion
				$data['email'] = $email;
				$data['nom'] = 'recuperation';
				$data['random'] = hash('sha512',$random);
				$this->Connexion->saveData($data);
				$mailcontent = "Cliquez sur le lien ci-dessous pour valider votre inscription: \r\n'".base_url()."InscriptionManager/recuperation?random=".$data['random'].".";
				//Envoyons le mail contenant le message stocké dans $mailcontent //*/
				$to  = $data['email'];

				$headers = "From: vvymmo@gmail.com";
				$body = "Message: ".$mailcontent." \r\n";

				mail($to, "Recuperation de mot de passe", $body, $headers);
				//redirection
				$this->session->set_flashdata('registered','Un Mail de recuperation vous a été envoyé');
				redirect(site_url(array('Home','index')));
			}else{
				$this->session->set_flashdata('error','Compte introuvable');	
				$this->load->view('HOME/header1');
				$this->load->view('HOME/recover');
				$this->load->view('HOME/footer1');
			}
		}

		public function connecte(){
			//redirection si deja connecte
			if (isset($_SESSION['user']['identity'])) {
				redirect(site_url(array('Home','index'))); //redirection vers la page d'accueil
			} 
			// echo "<pre>"; print_r($_POST); echo "</pre>";
			if (!empty($_POST['email']) && !empty($_POST['password'])) {
				
				$data['password'] = hash('sha512', $_POST['password']);
				$user = $this->User->recuperer(array('password'=>$data['password']));
				// echo "<pre>"; print_r($user); echo "</pre>";
				if ($user['total']!=0 && ($user[0]['email'] == $_POST['email'])) {
					unset($user[0]['password']);
					$_SESSION['user']['identity'] = $user[0];
					// verification de l'abonnement et mise a jour
					$this->Abonnement->hydrate(array('id_user'=>$user[0]['id']));
					$_SESSION['user']['identity']['etat_abonnement'] = $this->Abonnement->upgrade('id_user'); 
					$this->session->set_flashdata('registered','Bienvenue');
					redirect(site_url(array('Home','index')));
				}else{
					$this->session->set_flashdata('error','Verifiez vos informations');
					$this->load->view('HOME/header1');
					$this->load->view('HOME/login');
					$this->load->view('HOME/footer1');
					// redirect(site_url(array('Home','login'))); //redirection vers le formulaire d'inscription
				}
				
			}else{
				$this->session->set_flashdata('error','Verifiez vos informations');
				$this->load->view('HOME/header1');
				$this->load->view('HOME/login');
				$this->load->view('HOME/footer1');
				// redirect(site_url(array('Home','login'))); //redirection vers le formulaire d'inscription
			} 
		
		} 

	   //Deconnexion
		public function deconnexion(){
		   	session_destroy();
		   	redirect(site_url(array('Home','index')));
		}

	   //Generation d'un mot aleatoire de 12 caracteres
		public function randomise(int $n=12){
		   	$str = "Abcdefghijkl[];'/.,mnopqrstuvwxyz1234567890!@#$%^&*()-_+abCDEFGHIJKLM?><\"{}|\NOPQRSTUVWXYZ";
		   	$str = str_shuffle($str);

		   	return substr($str, 0, $n);
		}

	}