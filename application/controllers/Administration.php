<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administration extends CI_Controller {


		
			
	public function index(){
		
		 if (isset($_SESSION['ADMIN'])) {
			$this->load->view('ADMIN/index');
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/home');
			$this->load->view('ADMIN/footer');
		
				
		}else{
	   	 session_destroy();
		 redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}
	 		
	
	// fonction qui charge le formulaire de connexion pour un administrateur
	public function formulaireConnexion(){
		
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_SESSION['ADMIN'])) {
				redirect(site_url(array('Administration','index')));
			}else{
				session_destroy();
				redirect(site_url(array('Administration','formulaireConnexion')));
			}
		}else{
			$this->load->view('gestion_admin/formulaire_connexion');
		}
	}
	
	public function manageConnexion(){
		
		if (isset($_POST['email']) && isset($_POST['password'])) {
			
			$admin = $this->Admin->findAllAdminBd();
			for ($i=0; $i < $admin['total']; $i++) { 
				if ($admin[$i]['email'] == $_POST['email'] && $admin[$i]['password'] == $_POST['password']) {
					$_SESSION['ADMIN'] = $admin[$i];
				}
			}
			
			if (isset($_SESSION['ADMIN'])) {
				redirect(site_url(array('Administration','index')));
			}else{
				$_SESSION['ERR'] = 'Les paramtres recus ne correspondent a auccun adminstrateur dans notre Database.<br> <b>Veillez recommencer SVP</b>';
				redirect(site_url(array('Administration','formulaireConnexion')));
			}
		}else{
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}

	// Gestions des Administrateurs


	public function manageAdmin(){
		
		if (isset($_SESSION['ADMIN'])) {
			
			$data['AllAdmin']=$this->Admin->findAllAdminBd();
			$this->load->view('WELCOME/index',$data);
			$this->load->view('template_al/navigation');
			$this->load->view('ADMIN/home_admin');
			$this->load->view('WELCOME/footer');
			
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}
	}
	public function testExitAdmin($email){
        $etat=0;
        $data['infoAdmin']=$this->Admin->findAllAdminBd();
        if ($data['infoAdmin']['total']<=0) {
            
        }else{
            for ($i=0; $i <$data['infoAdmin']['total'] ; $i++) { 
                if ($data['infoAdmin'][$i]['email']==$email) {
                    $etat=1;
                    break;
                }else{
                    $etat=0;
                }
            }
        }
        return $etat;
	}
	

	public function addAdmin(){
		if (isset($_SESSION['ADMIN'])) {
			if (isset($_POST)) {
				$etat=$this->testExitAdmin($_POST['email']);
				if ($etat==0) {
					if (isset($_FILES['profil']) AND $_FILES['profil']['error'] == 0 ){
	        		// Testons si le fichier n'est pas trop gros
	                    if ($_FILES['profil']['size'] <= 100000000){
	                        // Testons si l'extension est autorisée
	                        $infosfichier =pathinfo($_FILES['profil']['name']);
	                        $extension_upload = $infosfichier['extension'];

	                        $config =$_FILES['profil']['name'].date('d').'-'.date('m').'-'.date('Y').'a'.date('H').'-'.date('i').$_SESSION['ADMIN']['id'];
	 						$ma_variable = str_replace('.', '_', $config);
							$ma_variable = str_replace(' ', '_', $config);
							$config = $ma_variable.'.'.$extension_upload;
							move_uploaded_file($_FILES['profil']['tmp_name'],'assets/images/user_profil/'.$config);
							$data['profil']=$config;
							
	                    }else{
	                        $data['message_save']="La taille du fichier choisie  est très grande veuillez le remplacer svp !!";
	                        $data['message']='non';
	                    }
	                }else{
	                    $data['message_save']="L'image choisie  est endommagée  veuillez le remplacer svp !!";
	                    $data['message']='non';
	                }
					
					$data['nom']=$_POST['nom'];
					$data['email']=$_POST['email'];
					$data['password']=$_POST['password'];
					$data['telephone']=$_POST['telephone'];
					
					$data['date']=date('Y-m-d H:i:s');
					$this->Admin->hydrate($data);
					$this->Admin->addAdmin();
					$_SESSION['message_save']="Administrateurs enregistré avec success !!";
			 		$_SESSION['success']='ok';
			 		redirect(site_url(array('Administration','manageAdmin')));
					
				}else{
					session_destroy();
					direct(site_url(array('Administration','formulaireConnexion')));
				}
			}else{
				session_destroy();
				redirect(site_url(array('Administration','formulaireConnexion')));
			}
		}else{
			session_destroy();
			redirect(site_url(array('Administration','formulaireConnexion')));
		}

	}


	public function deconnexion(){
		if (isset($_SESSION['ADMIN'])) {
			session_destroy();
		}
		redirect(site_url(array('Administration','index')));
	}

	


	
}
