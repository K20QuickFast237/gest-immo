<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rechercher extends CI_Controller {

	private $data;
	private $trouve;
		
			
	public function index(){
		$this->main();
	}
	 		
	public function main2(){
		// if (isset($_POST)) {
		if (TRUE) {
			if (isset($_POST['type'])) {
				$this->data['type'] = $_POST['type'];
				$this->trouve[] = 'type';
			}
			if (isset($_POST['prix'])) {
				$this->data['prix'] = (int)strip_tags($_POST['prix']) ;
				$this->trouve[] = 'prix';
			}
			if (isset($_POST['action'])) {
				$this->data['action'] = $_POST['action'];
				$this->trouve[] = 'action';
			}
			if (isset($_POST['ville'])) {
				$this->data['ville'] = $_POST['ville'];
				$this->trouve[] = 'ville';
			}

			$this->Bien->hydrate($this->data);
			$resultat = $this->Bien->recuperer($this->trouve);
		}
	}	
	 		
	public function main(){
		if (isset($_POST)) {
			echo "le Post: <pre>"; print_r($_POST); echo "</pre><br>";
		// if (TRUE) {
			if (!empty($_POST['type'])) {
				$this->data['type'] = $_POST['type'];

				$this->Bien->hydrate($this->data);
				$trouve = $this->Bien->recuperer('type');
				echo "Premier trouve dans type <br><pre>"; print_r($trouve); echo "</pre><br>";
				if ($trouve['data']=='ok') {
					$this->data = [];
					$this->data = $trouve;
				}else{
					$this->data['total'] = 0;
					$this->data['data'] = 'non';
				}
			echo "dans type: <br><pre>"; print_r($this->data); echo "</pre><br>";

			}

			if (!empty($_POST['prix']) && ($this->data['total'] != 0)) {
				$result = [];
				$cpt = 0;
				// filtrons data
				for ($i=0; $i < $this->data['total']; $i++) { 
					if ($this->data[$i]['prix'] <= $_POST['prix']) {
						$result[] = $this->data[$i]; 
						$cpt++;
					}
				}
				$this->data = []; // on vide data
				//puis on la reformate avec les valeurs filtrees
				$this->data = $result;
				$this->data['data'] = 'ok';
				$this->data['total'] = $cpt;
			echo "Dans prix: <br><pre>"; print_r($this->data); echo "</pre><br>";

			}

			if (!empty($_POST['action']) && ($this->data['total'] != 0)) {
				$result = [];
				$cpt = 0;
				// filtrons data
				for ($i=0; $i < $this->data['total']; $i++) { 
					$pattern = "/".$_POST['action']."/i";
					if (preg_match($pattern, $this->data[$i]['etat']) != 0) {
					// if ($this->data[$i]['etat'] == $_POST['action']) {
						$result[] = $this->data[$i]; 
						$cpt++;
					}
				}
				$this->data = []; // on vide data
				//puis on la reformate avec les valeurs filtrees
				$this->data = $result;
				$this->data['data'] = 'ok';
				$this->data['total'] = $cpt;
			echo "dans action: <br><pre>"; print_r($this->data); echo "</pre><br>";

			}

			if (!empty($_POST['ville']) && ($this->data['total'] != 0)) {
				// echo "dans ville";
				echo "la data dans ville: <br><pre>"; print_r($this->data); echo "</pre><br>";
				$result = [];
				$cpt = 0;
				// filtrons data
				for ($i=0; $i < $this->data['total']; $i++) { 

					$ville = htmlspecialchars(strip_tags($_POST['ville']));
					$pattern = "/".$ville."/i";
					$localisation = $this->Localisation->VillebyId($this->data[$i]['id_localisation']);
					$localite = $this->Localisation->AdressebyId($this->data[$i]['id_localisation']);
					if (preg_match($pattern, $localisation) != 0) {  // validation de l'element
						$result[] = $this->data[$i]; 
						$cpt++;
					}else if (preg_match($pattern, $localite) != 0) {  // validation de l'element
						$result[] = $this->data[$i]; 
						$cpt++;
					}
				}
				$this->data = []; // on vide data
				//puis on la reformate avec les valeurs filtrees
				$this->data = $result;
				$this->data['data'] = 'ok';
				$this->data['total'] = $cpt;

			echo "Dans ville: <br><pre>"; print_r($this->data); echo "</pre><br>";

			}

			// if (isset($this->data['type'])) {
			// 	unset($this->data['type']);
			// }
			echo "A la fin: <br><pre>"; print_r($this->data); echo "</pre><br>";
			// return $this->data;
			if ($this->data['total']!=0) {
				for ($i=0; $i < $this->data['total']; $i++) { 
					//on recupere l'image le nbre de peces et la superficie dans la table descripotion correspondante au bien
					echo "data depart: <pre>"; print_r($this->data); echo "</pre>";

					$this->Description->hydrate(array('id'=>$this->data[$i]['id_description']));
					$details = $this->Description->recuperer('id');
					$details = $details[0];
					$image = json_decode($details['image']);
					$this->data[$i]['image'] = $image->{1}; //la premiere image uploadee est prise comme image principale
					echo "data milieu: <pre>"; print_r($this->data); echo "</pre>";

					$this->data[$i]['nbrepiece'] = $details['nombre_piece'];
					$this->data[$i]['superficie'] = $details['superficie'];
					$this->data[$i]['ville'] = $this->Localisation->VillebyId($this->data[$i]['id_localisation']);
					
				}


			 }//else{
			// 	$this->session->set_flashdata('recherche',$data);
			// 	redirect(site_url(array('Home','index')));
				
			}
		}
	}	


}
