<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Inscription extends CI_Controller {

		private $data = [
							"user" => "",
							"contact" => "",
							"location" => "",
						];
		
		public function index(){

			$this->inscrire();

		}

		public function inscrire(){
			
			$this->load->view('inscription');

		}

		public function manage_inscription(){ 
			// Pour la table user
				//si les donnes sont bien recues
				if ($this->data["user"]) {
				// if (isset($_POST['nom'])) {
					//on les verifient
					if(($this->User->email_exist(strip_tags($_POST['email'])))) {  // verifie si cet email existe
						// redirection vers le formulaire de connexion avec une session flash
						$this->session->set_flashdata("error","Oups! Cette adresse Email est déjà utilisée");

					}else{
						if ($_POST['password'] == $_POST['password_confirm']) {   // verifie  la confirmation du password
							// insertion des donnes dans la base 
							$data = [
										"nom" => strip_tags($_POST['nom']),
										"email" => strip_tags($_POST['email']),
										"password" => strip_tags($_POST['password']),   // encoder le password
									];

							$this->User->hydrate($data);
							$this->User->inserer($data);
							// definition de la session 
							// redirection 
							redirect(site_url(array("Welcome","accueil")));

						}else{
						// redirection vers le formulaire de connexion avec une session flash
							$this->session->set_flashdata("error","Oups! Cette adresse Email est déjà utilisée");

						}
					}
					//on definit la session
				}
		}

		public function save_userdata(){
			// if (isset($_SESSION['user'])) {
				// Pour la table user
				//si les donnes sont bien recues
				if (!($this->data["user"] == "")) {
					$this->User->hydrate($this->data['user']);
					$this->User->inserer($this->data['user']);
					// definition de la session 
					$infos = $this->User->recuperer(array("email"=>$this->data['user']['email']));
					$_SESSION['user']['identity'] = $infos[0];

				}
				if (!($this->data["contact"] == "")) {
					$this->data['contact']['email'] = $_SESSION['user']['identity']['email'];
					$this->data['contact']['id_user'] = $_SESSION['user']['identity']['id'];
					$this->Contact->hydrate($this->data['contact']);
					$this->Contact->inserer($this->data['contact']);
					$infos = $this->Contact->recuperer(array("id_user"=>$_SESSION['user']['identity']['id']));
					$_SESSION['user']['contact'] = $infos[0];
					// update le user correspondant
					$this->User->hydrate(array("id_contact"=>$_SESSION['user']['contact']['id']));
					$this->User->actualise(array('id_contact'=>$_SESSION['user']['contact']['id']),$_SESSION['user']['identity']['id']);

				}
				if (!($this->data["location"] == "")) {
					$this->data['location']['id_user'] = $_SESSION['user']['identity']['id'];
					$this->Localisation->hydrate($this->data['location']);
					$this->Localisation->inserer($this->data['location']);
					$infos = $this->Localisation->recuperer(array("id_user"=>$_SESSION['user']['identity']['id']));
					$_SESSION['user']['location'] = $infos[0];
					// update le user correspondant
					$this->User->hydrate(array("id_localisation"=>$_SESSION['user']['location']['id']));
					$this->User->actualise(array('id_localisation'=>$_SESSION['user']['location']['id']),$_SESSION['user']['identity']['id']);
				}	
			// }
		}

		public function add_datauser(){
			//recuperation des donnees
			if (isset($_SESSION['user']['identity'])) {
				if (isset($_POST['nom']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['password_confirm'])) {
					if ($_POST['password'] == $_POST['password_confirm']) {   // verifie  la confirmation du password
						$this->data['user'] = [
									"nom" => strip_tags($_POST['nom']),
									"email" => strip_tags($_POST['email']),
									"password" => strip_tags($_POST['password']),   // encoder le password
								];
						$this->User->hydrate($this->data['user']['identity']);
						$this->User->actualise($this->data['user']['identity'],$_SESSION['user']['id']);
					}

				}

			}
			if (isset($_POST['nom']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['password_confirm'])) {
				// Traitement 
				//on les verifient
				if($this->User->email_exist(strip_tags($_POST['email']))) {  // verifie si cet email existe
					// Definition de session flash
					$this->session->set_flashdata(array("error"=>"Oups! Cette adresse Email est déjà utilisée.","status"=>"fail"));
					redirect(site_url(array("Inscription","inscrire")));

				}else{
					if ($_POST['password'] == $_POST['password_confirm']) {   // verifie  la confirmation du password
						// insertion des donnes dans la base 
						$this->data['user'] = [
									"nom" => strip_tags($_POST['nom']),
									"email" => strip_tags($_POST['email']),
									"password" => strip_tags($_POST['password']),   // encoder le password
								];
						$this->save_userdata();

						// definition de la session 
						// $_SESSION['user']['identity'] = $data['user'];
						unset($_SESSION['error']);

						// redirection 
						$this->session->set_flashdata("status","identified");
						redirect(site_url(array("Inscription","inscrire")));
						// print_r(site_url(array("Inscription","inscrire")));

					}else{
					// redirection vers le formulaire d'inscription avec une session flash
						$_SESSION=(array("error"=>"Oups! Confirmation Incorrecte.","status"=>"fail"));
						redirect(site_url(array("Inscription","inscrire")));

					}
				}
			}else{
				// redirection vers le formulaire d'inscription avec une session flash
				$this->session->set_flashdata(array("error"=>"Oups! il nous manque une donne.","status"=>"fail"));
				redirect(site_url(array("Inscription","inscrire")));
			}

		}

		public function add_datalocation(){
			// recuperation des donnees
			if (isset($_POST['pays']) && isset($_POST['ville']) && isset($_POST['adresse'])) {
				// Traitement
				$this->data['location'] = [
							"pays" => strip_tags($_POST['pays']),
							"ville" => strip_tags($_POST['ville']),
							"adresse" => strip_tags($_POST['adresse']),  
						];
				if (isset($_POST['longitude'])) {
					$this->data['location']['longitude'] = strip_tags($_POST['longitude']);
				}
				if (isset($_POST['latitude'])) {
					$this->data['location']['latitude'] = strip_tags($_POST['latitude']);
				}
				
				// Enregistrement en bdd
				$this->save_userdata();
				
				// redirection
				$this->session->set_flashdata("status","located");
				redirect(site_url(array("Welcome","accueil")));

				

			}else{
				// redirection vers le formulaire d'inscription avec une session flash
				$this->session->set_flashdata("error","Oups! Nous avons perdu une donne de localisation.");
				redirect(site_url(array("Inscription","inscrire")));

			}
		}

		public function add_datacontact(){
			// recuperation des donnees
			if (isset($_POST['tel1']) && isset($_POST['whatsapp'])) {
				// Traitement
				$this->data['contact'] = [
							"tel1" => (int) strip_tags($_POST['tel1']),
							"whatsapp" => (int) strip_tags($_POST['whatsapp']),  
						];
				if (isset($_POST['tel2'])) {
					$this->data['contact']['tel2'] = (int) strip_tags($_POST['tel2']);
				}
				if (isset($_POST['facebook'])) {
					$this->data['contact']['facebook'] = strip_tags($_POST['facebook']);
				}
				if (isset($_POST['twitter'])) {
					$this->data['contact']['twitter'] = strip_tags($_POST['twitter']);
				}
				if (isset($_POST['instagram'])) {
					$this->data['contact']['instagram'] = strip_tags($_POST['instagram']);
				}
				if (isset($_POST['linkedin'])) {
					$this->data['contact']['linkedin'] = strip_tags($_POST['linkedin']);
				}
				if (isset($_POST['telegram'])) {
					$this->data['contact']['telegram'] = strip_tags($_POST['telegram']);
				}

				// $_SESSION['user']['contact'] = $data['contact'];
				//redirection
				$this->save_userdata();
				$this->session->set_flashdata("status","contacted");
				redirect(site_url(array("Inscription","inscrire")));


			}else{
				// redirection vers le formulaire d'inscription avec une session flash
				$this->session->set_flashdata("error","Oups! Nous avons perdu une donne de contact.");
				redirect(site_url(array("Inscription","inscrire")));
			}

		}

		public function verifyemail(){
			if (isset($_POST['email']) && $_POST['email']!='') {
				// print_r($_POST);
				if( $this->User->email_exist(strip_tags($_POST['email'])) ) {  // verifie si cet email existe
					// si oui
					$this->session->set_flashdata("error","Oups! Cette adresse Email est déjà utilisée");
					echo json_encode(array("response"=>"Email allready exist."));
				}else{
					echo json_encode(array("response"=>"done"));
				}
			}else{
				echo json_encode(array("response"=>"Email is requied."));
			}
		}
		public function RetourAcceuil(){

			redirect(site_url(array("Welcome","accueil")));
			// echo "accueil";

		}
	}
?>