
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
      <img src="<?php echo base_url()."assets/images/logo_icon3.png" ?>" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">TABLEAU DE BORD</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
          <!-- <li class="nav-item">
            <a href="#" class="nav-link <?php// if(isset($profil)){ echo 'active' ;} ?>">
              <i class="nav-icon fas fa-user"></i>
              <p>
                Mon Profil
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="display: none;">
              <li class="nav-item">
                <a href="<?php// echo site_url(array('Praticien','loadProfil')) ?>" class="nav-link <?php// if(isset($perso)){ echo 'active' ;} ?>">
                  <i class="fa fa-caret-right"></i>
                  <p>Informations Personnel</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php// echo site_url(array('Praticien','loadInfoprof')) ?>" class="nav-link <?php// if(isset($prof)){ echo 'active' ;}; if(isset($prof)){ echo 'active';} ?>">
                  <i class="fa fa-caret-right"></i>
                  <p>Espace Pro</p>
                </a>
              </li>
            </ul>
          </li -->>

          <li class="nav-item">
            <a href="#" class="nav-link <?php// if(isset($profil)){ echo 'active' ;} ?>">
              <i class="nav-icon fas fa-bullhorn"></i>
              <p>
                Mes Annonces 
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="display: none;">
              <li class="nav-item">
                <a href="<?php echo site_url(array('Admin','allAnnonces')) ?>" class="nav-link <?php// if(isset($prof)){ echo 'active' ;}; if(isset($prof)){ echo 'active';} ?>">
                  <i class="fa fa-caret-right"></i>
                  <p>Toutes les Annonces</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link <?php// if(isset($profil)){ echo 'active' ;} ?>">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Mes Prorietés 
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="display: none;">
              <li class="nav-item">
                <a href="<?php echo site_url(array('Admin','listeProriete')) ?>" class="nav-link <?php// if(isset($perso)){ echo 'active' ;} ?>">
                  <i class="fa fa-caret-right"></i>
                  <p>Liste des Proprietés</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link <?php// if(isset($profil)){ echo 'active' ;} ?>">
              <i class="nav-icon fas fa-credit-card"></i>
              <p>
                Mon Abonnements
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview" style="display: none;">
              <li class="nav-item">
                <a href="<?php echo site_url(array('Admin','Abonnement')) ?>" class="nav-link <?php// if(isset($perso)){ echo 'active' ;} ?>">
                  <i class="fa fa-caret-right"></i>
                  <p>Abonnement Actuel </p>
                </a>
              </li>
             <!--  <li class="nav-item">
                <a href="<?php// echo site_url(array('Praticien','loadInfoprof')) ?>" class="nav-link <?php// if(isset($prof)){ echo 'active' ;}; if(isset($prof)){ echo 'active';} ?>">
                  <i class="fa fa-caret-right"></i>
                  <p>Renouveler</p>
                </a>
              </li> -->
            </ul>
          </li>
          <?php if ($_SESSION['user']['identity']['niveau'] > 4) { ?>
            <li class="nav-item">
              <a href="#" class="nav-link <?php// if(isset($profil)){ echo 'active' ;} ?>">
                <i class="nav-icon far fa-clipboard"></i>
                <p>
                  Publications
                  <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview" style="display: none;">
                <li class="nav-item">
                  <a href="<?php echo site_url(array('Admin','publication')) ?>" class="nav-link <?php// if(isset($perso)){ echo 'active' ;} ?>">
                    <i class="fa fa-caret-right"></i>
                    <p>Publications actives </p>
                  </a>
                </li>
              </ul>
            </li>
          <?php }?>
            

          <li class="nav-item">
            <a href="<?php echo site_url(array('Admin','deconnexion')) ?>" class="nav-link">
              <i class="nav-icon fas fa-power-off text-danger"></i>
              <p>
                Deconnexion
              </p>
            </a>
          </li> 
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
  </aside>


