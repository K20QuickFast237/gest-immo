
<div class="content-wrapper" style="min-height: 1604.44px;">

  <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Mon Abonnement</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Acceuil</a></li>
              <li class="breadcrumb-item">Mon Abonnement</li>
              <li class="breadcrumb-item active">Abonnement actuel</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
  </section>

  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-body p-0">
        <table  class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Type Abonnement</th>
                    <th>Date de Debut</th>
                    <th>Date de Fin</th>
                    <th>Etat</th>
                    <th>Souscription</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td id="type">
                        <?=$allannonces['type'];?>
                    </td>
                    <td id="debut">
                        <?=$allannonces['date_debut'];?>
                    </td>
                    <td id="fin">
                        <?=$allannonces['date_fin'];?> 
                    </td>
                    <?php if(preg_match('/invalide/i', $allannonces['etat'])) { ?>
                    <td class="text-danger" id="etat">
                        <?=$allannonces['etat'];?>
                    </td>
                    <?php } else{ ?>
                    <td class="text-success" id="etat">
                        <?=$allannonces['etat'];?>
                    </td>
                    <?php } ?>
                    
                    <td class="d-flex justify-content-center text-center">
                    <!-- <td class="text-center"> -->
                        <form action="<?=site_url(array('Admin','updateAbonnement'));?>" method="POST">
                            <input type="hidden" name="duree" value="10">    
                            <button type="submit" class="btn btn-success btn-sm ml-4" href="#" title="XXX XAF">
                                <i class="fas fa-plus">&nbsp;
                                </i>
                                10 jours
                            </button>
                        </form>
                        <form action="<?=site_url(array('Admin','updateAbonnement'));?>" method="POST">
                            <input type="hidden" name="duree" value="30">    
                             <button type="submit" class="btn btn-success btn-sm ml-4" href="#">
                                <i class="fas fa-plus">&nbsp;
                                </i>
                                30 jours
                            </button>
                        </form>
                        <form action="<?=site_url(array('Admin','updateAbonnement'));?>" method="POST">
                            <input type="hidden" name="duree" value="365">    
                            <button type="submit" class="btn btn-success btn-sm ml-4" href="#">
                                <i class="fas fa-plus">&nbsp;
                                </i>
                                365 jours
                            </button>
                        </form>
                    </td>
                </tr>
            </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->

  </section>

</div>