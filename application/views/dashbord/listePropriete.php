
<div class="content-wrapper" style="min-height: 1604.44px;">

  <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Mes Proprietes</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Acceuil</a></li>
              <li class="breadcrumb-item">Mes Proprietes</li>
              <li class="breadcrumb-item active">Liste Proprietes</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
  </section>

   <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- <div class="row"> -->
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Type</th>
                    <th>Etat</th>
                    <th>Prix</th>
                    <th>Localisation</th>
                    <!-- <th>Equipements</th> -->
                    <th>Nombre d'avis</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                    $produit[0]['nom'] = 'lerob';
                    $produit[0]['quantite'] = 'lerob';
                    $produit[0]['date_expiration'] = 'lerob';
                    $produit[0]['date_entree'] = 'lerob';
                    $produit[0]['niv_bas'] = 'lerob';
                    $produit[0]['niv_critique'] = 'lerob';

                    $produit[1]['nom'] = 'lerob';
                    $produit[1]['quantite'] = 'lerob';
                    $produit[1]['date_expiration'] = 'lerob';
                    $produit[1]['date_entree'] = 'lerob';
                    $produit[1]['niv_bas'] = 'lerob';
                    $produit[1]['niv_critique'] = 'lerob';

                    $produit[2]['nom'] = 'lerob';
                    $produit[2]['quantite'] = 'lerob';
                    $produit[2]['date_expiration'] = 'lerob';
                    $produit[2]['date_entree'] = 'lerob';
                    $produit[2]['niv_bas'] = 'lerob';
                    $produit[2]['niv_critique'] = 'lerob';
                  ?>
                  <?php for ($i=0; $i < $bien['total'] ; $i++) { 
                      if (isset($bien[$i])) {  ?>
                      <tr>
                           <td><?= $bien[$i]['type'] ?></td>
                           <td><?= $bien[$i]['etat'] ?></td>
                           <td><?= $bien[$i]['prix'] ?></td>
                           <td><?= $bien[$i]['localisation'] ?></td>
                           <!-- <td><?//= $bien[$i]['id_equipement'] ?></td> -->
                           <td><?= $bien[$i]['nombre_avis'] ?></td>
                      </tr>
                  <?php }
                       } ?>
                  </tbody>
                  <tfoot>
                  <tr>
                    <th>Type</th>
                    <th>Etat</th>
                    <th>Prix</th>
                    <th>Localisation</th>
                    <!-- <th>Equipements</th> --> 
                    <th>Nombre d'avis</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          <!-- </div> -->
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

</div>