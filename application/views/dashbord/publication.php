
<div class="content-wrapper" style="min-height: 1604.44px;">

  <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Publications</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Acceuil</a></li>
              <li class="breadcrumb-item">Publications</li>
              <li class="breadcrumb-item active">Publications actives</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
  </section>

  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-body p-0">
        <table class="table table-striped projects">
            <thead>
                <tr>
                    <th>N <sup>o</sup></th>
                    <th>Type</th>
                    <th>Date Publication</th>
                    <th>Etat</th>
                    <!-- <th>Duree Reservation</th>
                    <th>Description</th>
                    <th class="text-center">Date Publication</th> -->
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
              <?php for ($i=0; $i < $publication['total']; $i++) { 
                $a = explode(' ', $publication[$i]['date_creation']);
                $jour = $a[0] ;
                $heure = $a[1];
              ?>
                <tr>
                    <td><?=$i+1?></td>
                    <td class="text-left">
                        <?=$publication[$i]['type'];?>
                    </td>
                    <td class="text-left">
                        <?=$jour?>  à <span style="color: blue;"><?=$heure?></span> 
                    </td>
                    <?php if ($publication[$i]['etat'] == 'Traitement en cours') { ?>
                        <td class="text-left" style="color: blue;">
                            publié
                        </td>
                    <?php }else{?>
                        <td class="text-left" style="color: red;">
                            <?=$publication[$i]['etat'];?>
                        </td>
                    <?php }?>
                    <td class="project-actions text-left">
                        <form action="<?=site_url(array('Admin','disablePublication'));?>" method="POST">
                            <input type="hidden" name="id" value="<?=$publication[$i]['id']?>">    
                            <button type="submit" class="btn btn-danger btn-sm" >
                                <i class="fas fa-trash">&nbsp;</i>
                                Retirer
                            </button>
                        </form>
                        <!-- <a class="btn btn-danger btn-sm" href="#">
                            <i class="fas fa-trash">&nbsp;
                            </i>
                            Retirer
                        </a> -->
                    </td>
                </tr>
              <?php } ?>
            </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->

  </section>

</div>