  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Invoice</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Invoice</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">


            <!-- Main content -->
            <div class="invoice p-3 mb-3">
              <!-- title row -->
              
              <!-- info row -->
              <div class="row invoice-info">
                <div class="col-sm-4 invoice-col">
                  <div class="card bg-light d-flex flex-fill">
                    <div class="card-header text-muted border-bottom-0">
                      USER NAME
                    </div>
                    <div class="card-body pt-0">
                      <div class="row">
                        <div class="col-12 text-center">
                          <img src="<?= base_url().'assets/dist';?>/img/user1-128x128.jpg" alt="user-avatar" class="img-circle img-fluid">
                        </div>
                      </div>
                    </div>
                    <div class="card-footer d-flex justify-content-around">
                      <!-- <div class="text-right"> -->
                        <a href="#" class="btn btn-sm btn-primary ml-3">
                          <i class="fas fa-user">&nbsp;</i> Voir Profil
                        </a>
                        <a href="#" class="btn btn-sm btn-primary ml-2">
                          <i class="fas fa-phone">&nbsp;</i> Voir Contact
                        </a>
                      <!-- </div> -->
                    </div>
                  </div>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  <div class="d-flex flex-fill">
                    <div class="table-responsive">
                      <table class="table">
                        <tr>
                          <th style="width:20%">Quoi:</th>
                          <td>$type de bien de la publication</td>
                        </tr>
                        <tr>
                          <th>situé:</th>
                          <td>situé:</td>
                        </tr>
                        <tr>
                          <th>Localite:</th>
                          <td>$Localite</td>
                        </tr>
                        <tr>
                          <th>Ville</th>
                          <td>$Ville</td>
                        </tr>
                        <tr>
                          <th>Pays</th>
                          <td>$Pays</td>
                        </tr>
                        <tr>
                          <th>latitude</th>
                          <td>$latitude</td>
                        </tr>
                        <tr>
                          <th>longitude</th>
                          <td>$longitude</td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div>
                <!-- /.col -->
                <div class="col-sm-4 invoice-col">
                  <div class="card bg-light d-flex flex-fill">
                    <div class="card-header text-muted border-bottom-0">
                      Digital Strategist
                    </div>
                    <div class="card-body pt-0">
                      <div class="row">
                        <div class="col-7">
                          <h2 class="lead"><b>Nicole Pearson</b></h2>
                          <p class="text-muted text-sm"><b>About: </b> Web Designer / UX / Graphic Artist / Coffee Lover </p>
                          <ul class="ml-4 mb-0 fa-ul text-muted">
                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span> Address: Demo Street 123, Demo City 04312, NJ</li>
                            <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Phone #: + 800 - 12 12 23 52</li>
                          </ul>
                        </div>
                        <div class="col-5 text-center">
                          <img src="<?= base_url().'assets/dist';?>/img/user1-128x128.jpg" alt="user-avatar" class="img-circle img-fluid">
                        </div>
                      </div>
                    </div>
                    <div class="card-footer">
                      <div class="text-right">
                        <a href="#" class="btn btn-sm bg-teal">
                          <i class="fas fa-comments"></i>
                        </a>
                        <a href="#" class="btn btn-sm btn-primary">
                          <i class="fas fa-user"></i> View Profile
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->

            </div>
            <!-- /.invoice -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->  