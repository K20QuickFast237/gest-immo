
<div class="content-wrapper" style="min-height: 1604.44px;">

  <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Mes annonces</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Acceuil</a></li>
              <li class="breadcrumb-item">Mes annonces</li>
              <li class="breadcrumb-item active">Liste annonces</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
  </section>

  <section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-body p-0">
        <table class="table table-striped projects">
            <thead>
                <tr>
                    <th>N <sup>o</sup></th>
                    <th>Type Publication</th>
                    <th>Etat</th>
                    <th>Disponibilite</th>
                    <th>Duree Reservation</th>
                    <th>Description</th>
                    <th class="text-center">Date Publication</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
              <?php for ($i=0; $i < $allannonces['total']; $i++) { 
                $a = explode(' ', $allannonces[$i]->{'date_creation'});
                $jour = $a[0] ;
                $heure = $a[1];
              ?>
                <tr>
                    <td><?=$i+1?></td>
                    <td>
                        <small>
                            <?=$allannonces[$i]->{'type'}?>
                        </small>
                    </td>
                    <td>
                        <small>
                            <?=$allannonces[$i]->{'etat'}?>
                        </small>
                    </td>
                    <td class="project_progress">
                        <small>
                          Disponible jusqu'au  <?=$allannonces[$i]->{'date_disponibilite'}?>
                        </small>
                    </td>
                    <td class="project-state">
                        <small>
                            <?=$allannonces[$i]->{'duree_reservation'}?>
                        </small>
                    </td>
                    <td class="project-state">
                        <small>
                            <?=$allannonces[$i]->{'description'}?>
                        </small>
                    </td>
                    <td class="project-state">
                        <small>
                            <?=$jour?>  à <span style="color: blue;"><?=$heure?></span> 
                        </small>
                    </td>
                    <td class="project-actions text-right">
                        <a class="btn btn-danger btn-sm" href="#">
                            <i class="fas fa-trash">&nbsp;
                            </i>
                            Retirer
                        </a>
                    </td>
                </tr>
              <?php } ?>
            </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->

  </section>

</div>