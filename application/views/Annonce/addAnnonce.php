<?php if (isset($_SESSION['user']['identity']['etat_abonnement']) && ($_SESSION['user']['identity']['etat_abonnement'] == 'invalide')) {
  $this->session->set_flashdata("modalShow","Votre Abonnement n'est plus valide. \n Veuillez le renouveller afin de déposer une nouvelle annonce");
  redirect(site_url(array('Home','index')));
} ?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="VYMMO Annonce Bien Immobilier">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <title>Ajouter Une Annonce</title>

    

    <!-- Bootstrap core CSS -->
    <?= css("bootstrap.min");?>

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
    <style type="text/css">
      .sized{
        font-size: 1.2rem;
        font-weight: 600;
      }

      @media (max-width: 767px) {
        .loader{
          padding: auto;
          margin-top: 1rem;
        }

      }

      @media (min-width: 768px) {
        .loader{
          padding-right: 0px;
          margin-top: 3rem;
        }
        .descris{
          padding-left: 0px;
        }
        .soumet{
          padding-left: 0px;
        }
      }

      @media (min-width: 992px) {
        .loader{
          padding-right: 0px;
          margin-top: 4rem;
        }
      }
    </style>
    
    <!-- Custom styles for this template -->
    <?= css("carousel");?>

  </head>
  <body>
    
<header>
  <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#"></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo site_url(array('Home','index')); ?>">ACCEUIL
            <span class="sr-only">(current)</span>
          </a>
        </li>
      </ul>
    </div>
  </nav>
</header>

<main role="main">

     <!-- <img  src="<?= img_url('annonce.gif');?>" alt="VYMMO" class="img-fluid" style="height: 600px; width: 100%;"> -->
    
       

  <!-- <form method="POST" action="<?php echo site_url(array('Annonces','saveAnnonce'));?>" enctype="multipart/form-data"> -->
  <form method="POST" action="<?php echo site_url(array('Home','saveAnnonce'));?>" enctype="multipart/form-data">

  <!-- Bloc de contenu -->
  <div class="jumbotron p-3" id="profil">
    <div class="container">
      <h1 class="display-3">Profil</h1>
      <p>Editez votre Profil.</p>
    </div>
  </div>
  <?php if(isset($_SESSION['errorProfil'])){ echo '<h4 style="color: red;">'.$_SESSION['errorProfil'].'</h4>'; }?>
  <div class="container marketing">
    <div class="row featurette">
      <div class="col-md-7">
        <hr class="mb-4 mt-0">
        <div class="row">
          <div class="col-sm-6" id="imgcontainer" style="border: 1px solid; height: 10rem;">
            <img id="img" src="<?= img_url('person.svg');?>" style="width: 100%; height: 100%;">
          </div>
          
          <div class="col-sm-5 form-group loader">
            <!-- <label for="imageProfil" class=" btn btn-primary" style=""> Charger une Image de Profil</label> -->
            <label for="imageloader" class="btn btn-primary" style=""> Charger une Image de Présentation</label>
            <input type="file" accept="image/jpeg, image/png, image/jpg, image/gif" name="imageProfil" onchange="loadFile(event)" id="imageloader" class="form-control-file" style="display: none;" value="<?=set_value('imageProfil');?>">
            <p style="font-size: 0.7rem; font-weight: bold;">*Types: png, jpg et jpeg maximum 10Mo</p>
          </div>
        </div>
        <div class="row">
          <div class="col form-group descris">
            <label for="desc" class="sized">Renseignez une Description pour votre profil</label>
            <textarea class="form-control" id="dsc" rows="3" required name="description" value="<?=set_value('description');?>"></textarea>
          </div>
        </div>
        <hr class="mb-4">
      </div>
      <div class="col-md-5 no-small">
         <img  src="<?= img_url('profil.png');?>" alt="VYMMO" style="width: 100%; height: 100%;">
      </div>
    </div>
  </div>

  <!-- Bloc de contenu -->
  <div class="jumbotron p-3 mt-4" id="description">
    <div class="container">
      <h1 class="display-3">Description du Bien</h1>
      <p>Ajoutez une description.</p>
    </div>
  </div>
  <?php if(isset($_SESSION['errorDescription'])){ echo "<h4 style='color: red;'>".$_SESSION['errorDescription']."</h4>"; }?>
  <div class="container marketing">
    <div class="row featurette">
      <div class="col-md-7 order-md-2">
        <hr class="mb-4 mt-0">
          <div class="form-group row"> 
            <label for="type" class="col-md-4 col-form-label col-form-label-md sized" style="margin-top: -1%;">Categorie:</label>
            <div class="col-md-8">
              <select class="form-control" id="type" required name="type" value="<?=set_value('type');?>">
                <option disabled selected>Choisir</option>
                <?php if (isset($listeTypeBien)) {
                  for ($i=0; $i < count($listeTypeBien) ; $i++) { 
                    echo '<option value="'.$listeTypeBien[$i].'">'.$listeTypeBien[$i].'</option>';
                  }
                }else{
                  $liste = array('listeTypeBien'=>$this->Typebien->recuperer());
                  $_SESSION = array_merge($_SESSION, $liste);

                  for ($i=0; $i < $_SESSION['listeTypeBien']['total'] ; $i++) { 
                    echo '<option value="'.$_SESSION['listeTypeBien'][$i]['nom'].'">'.$_SESSION['listeTypeBien'][$i]['nom'].'</option>';
                  }
                  
                  ?>
                <?php }?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label for="prix" class="col-md-4 col-form-label sized">Prix:</label>
            <div class="col-md-5">
              <input type="number" class="form-control" id="prix" required name="prix" placeholder="En XAF" value="<?=set_value('prix')?>" min="0">
            </div>
          </div>
          <div class="form-group row">
            <label for="superficie" class="col-md-4 col-form-label sized">Superficie:</label>
            <div class="col-md-5">
              <input type="number" class="form-control" id="superficie" required name="superficie" placeholder="Valeur en m²" value="<?=set_value('superficie');?>" min="0">
            </div>
          </div>
          <div class="form-group row">
            <label for="nombre_piece" class="col-md-4 col-form-label sized" style="margin-top: -3.3%;">Nombre de Pieces:</label>
            <div class="col-md-5">
              <input type="number" class="form-control" id="nombre_piece" required name="nombre_piece" placeholder="Exemple 4" value="<?=set_value('nombre_piece');?>" min="0">
            </div>
          </div>
          <div class="form-group row">
            <label for="nombre_chambre" class="col-md-4 col-form-label sized" style="margin-top: -3.3%;">Nombre de Chambre:</label>
            <div class="col-md-5">
              <input type="number" class="form-control" id="nombre_chambre" required name="nombre_chambre" placeholder="Exemple 2" value="<?=set_value('nombre_chambre');?>" min="0">
            </div>
          </div>

          <div class="form-group row" id="row1">
            <label class="col-sm-4 col-form-label sized">Images:<br><span style="font-size: 0.6rem; font-weight: bold;">*Types: png, jpg et jpeg maximum 10Mo</span></label>
            <label for="imgBien1" class="col-sm-4 btn btn-light" id="imgbtn" style="font-size: 22px; height: 3rem;"> Charger</label>
            <input type="file" accept="image/jpeg, image/png, image/jpg, image/gif" name="imgBien1" onchange="loader(event)" id="imgBien1" style="display: none;" value="<?=set_value('imgBien1');?>">
            <div class="col-sm-4" id="imglayer1" style="display: none;">
              <img src="" id="imgLoader1" class="img-thumbnail" style="width: 100%; height: 100%;">
            </div>
            <button type="button" class="col-sm-2 btn btn-outline-info order-xs-1" id="add" style="padding: 0!important; height: 3rem; display: none;"><span class="fa fa-add"></span> Ajouter </button>
          </div>
          <div id="mywrapper"></div>
          
          <div id="4autre"></div>
          <div class="form-group row">
            <button role="button" id="autre" class="btn btn-light btn-block" style="font-size: 22px; height: 3rem;">Ajouter une Description Particuliere</button>
          </div>
          
        <hr class="mb-4">

      </div>
      <div class="col-md-5 order-md-1 no-small">
          <img  src="<?= img_url('description.png');?>" alt="VYMMO" style="width: 100%; height: 90%;">
      </div>
    </div>
  </div>



  <!-- Bloc de Localisation -->
  <div class="jumbotron p-3" id="localisation" style="border-radius: 0; margin-top:25px;">
    <div class="container">
      <h1 class="display-3">Localisation</h1>
      <p>Où est situé votre bien ?</p>
    </div>
  </div>
  <?php if(isset($_SESSION['errorLocalisation'])){ echo "<h4 style='color: red;'>".$_SESSION['errorLocalisation']."</h4>"; }?>
  <div class="container marketing">
    <div class="row featurette">
      <div class="col-md-7">
        <hr class="mb-4 mt-0">
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="pays" class="sized">Pays</label>
            <input type="text" class="form-control" id="pays" placeholder="requis" name="pays" required value="<?=set_value('pays');?>">
          </div>
          <div class="col-md-6 mb-3">
            <label for="ville" class="sized">ville</label>
            <input type="text" class="form-control" id="ville" placeholder="requis" name="ville" required value="<?=set_value('ville');?>">
          </div>
        </div>
        <div class="row"> 
          <div class="col-md-6 mb-3">
            <label for="adresse" class="sized">Adresse</label>
            <input type="text" class="form-control" id="adresse" placeholder="requis" name="adresse" required value="<?=set_value('adresse');?>">
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 mb-3">
            <label for="longitude" class="sized">Longitude</label>
            <input type="text" class="form-control" id="longitude" placeholder="Optionnel" name="longitude" value="<?=set_value('longitude');?>">
          </div>
          <div class="col-md-6 mb-3">
            <label for="latitude" class="sized">Latitude</label>
            <input type="text" class="form-control" id="latitude" placeholder="Optionnel" name="latitude" value="<?=set_value('latitude');?>">
          </div>
        </div>
        <hr class="mb-4">
      </div>
      <div class="col-md-5 no-small">
          <img  src="<?= img_url('localisation.png');?>" alt="VYMMO" style="width: 100%; height: 90%;">
      </div>
    </div>
  </div>

  <!-- Bloc de contenu -->
  <div class="jumbotron p-3 mt-4 " id="equipement">
    <div class="container">
      <h1 class="display-3">Equipements du Bien</h1>
      <p>Précisez des équipements.</p>
    </div>
  </div>
  <?php if(isset($_SESSION['errorEquipement'])){ echo "<h4 style='color: red;'>".$_SESSION['errorEquipement']."</h4>"; }?>
  <div class="container marketing">
    <div class="row featurette">
      <div class="col-md-7 order-md-2">

        <hr class="mb-4 mt-0">
        
        <div class="row" id="4equip"> 
          <?php if (isset($listeEquipement)) {
            for ($i=0; $i < count($listeEquipement) ; $i++) { ?>
              <div class="col-xs-6 col-md-6">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="<?= $listeEquipement[$i];?>" name="<?= $listeEquipement[$i];?>" value ="ok">
                  <label class="custom-control-label sized" for="<?= $listeEquipement[$i];?>"> <?= $listeEquipement[$i];?> </label>
                </div>
              </div>
          <?php  }
          }?>
        </div>
        <div class="form-group row mt-4">
          <button role="button" id="equip" class="btn btn-light btn-block" style="font-size: 22px; height: 3rem;">Ajouter Des Equipement.</button>
        </div>

        <hr class="mb-4">

      </div>
      <div class="col-md-5 order-md-1 no-small">
       <img  src="<?= img_url('equipements.png');?>" alt="VYMMO" style="width: 100%; height: 90%;">

      </div>
    </div>
  </div>

  <div class="form-group row justify-content-center mt-4">
    <div class="col-md-8">
      <div class="custom-control custom-checkbox">
        <input type="checkbox" id="confirm" class="custom-control-input" name="confirmation" value ="ok" required>
        <label class="custom-control-label sized" for="confirm"> Je confirme avoir rempli les bonnes informations </label>
      </div>
    </div>
    <div class="col-md-2">
      <input type="submit" class="form-control" value="Enregistrer">
    </div>
  </div>
  

  </form>


  <!-- FOOTER -->
<!--   <footer class="container">
    <p class="float-right"><a href="#">Back to top</a></p>
    <p>&copy; 2017-2021 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
  </footer> -->
</main>

      <?= js('jquery.slim.min');?>
      <!-- <script>window.jQuery || document.write('<script src="../assets/dist/js/jquery.slim.min.js"><\/script>')</script> -->
      <?= js('bootstrap.bundle.min');?>

      <script type="text/javascript">
        var loadFile = function(event) {
          var image = document.getElementById('img');
          // var image = ($('#img'));
          image.removeAttribute("src");
          image.setAttribute("src", "");
          image.src = URL.createObjectURL(event.target.files[0]);
          image.onload = function() {
            URL.revokeObjectURL(image.src) // free memory
          }
          image.setAttribute('class', 'img-thumbnail');

          var container = document.getElementById('imgcontainer');
          container.removeAttribute('style');
          container.setAttribute('style', 'height: 10rem;');

        };

        var i=1;
        var j=1;
        var k=1;
        var inputName = 'imgBien1'; //initialisation
        var inputId;
        var imgid = 'imgLoader1'; //initialisation
        var imglayerid = 'imglayer1'; //initialisation
        var rowid = 'row1'; //initialisation
        
        var add = document.getElementById('add');   // le bouton ajouter image
        var autre = document.getElementById('autre');   // le bouton ajouter une description
        var equip = document.getElementById('equip');   // le bouton ajouter un equipement

        var autrelayer = document.getElementById('4autre');   // le bouton ajouter description
        var equiplayer = document.getElementById('4equip');   // le bouton ajouter equipement
        var wrapper = document.getElementById('mywrapper'); // la div pour contenir les elmts crees dynamiquement

        var loader = function(event){
          // inputName = "imgBien"+i;
          var imginput = document.getElementById(inputName); // le input
          var imglayer = document.getElementById(imglayerid); // la div contenant l'image
          var img = document.getElementById(imgid); // la balise img
          var row = document.getElementById(rowid); // la balise row principale
          var imgbtn = document.querySelector('label[for="'+inputName+'"'); // la balise img

          //onprevisualise
          img.src = URL.createObjectURL(event.target.files[0]);
          img.onload = function(){
            URL.revokeObjectURL(img.src)
          }

          //on affiche et cache
          add.setAttribute('style', 'padding: 0!important; height: 3rem; display: block;');
          imglayer.setAttribute('style', 'display: block;');
          // imgbtn.removeAttribute('style');
          imgbtn.setAttribute('style', 'display: none;');
          row.removeChild(imgbtn);
          imginput.removeAttribute('id');
          imglayer.removeAttribute('id');
          imgbtn.removeAttribute('id');
          img.removeAttribute('id');

          //limite du nombre de fichier
          if (i>=4) {
            add.setAttribute('style','display: none;');
          }

        }

        add.addEventListener('click', function(event){
          i++;
          add.setAttribute('style','display: none;')

          inputName = "imgBien"+i;
          inputId = inputName;
          imglayerid = "imglayer"+i;
          imgid = "imgLoader"+i;
          rowid = "row"+i;
          var imginput = document.getElementById(inputName); // le input

          //on cree l'element
          var div = document.createElement('div');
          // console.log('le contenant d\'image: '+imginput);

          // div = setAttribute('class','form-group row');
          div.innerHTML = `
            <div class="form-group row" id="`+rowid+`">
              <label class="col-sm-4 col-form-label sized" style="opacity: 0;">Images:</label>
              <label for="`+inputName+`" class="col-sm-4 btn btn-light" id="imgbtn" style="font-size: 22px;"> Charger</label>
              <input type="file" accept="image/jpeg, image/png, image/jpg, image/gif" name="`+inputName+`" onchange="loader(event)" id="`+inputId+`" style="display: none;">
              <div class="col-sm-4" id="`+imglayerid+`" style="display: none;">
                <img src="" id="`+imgid+`" class="img-thumbnail" style="width: 100%; height: 100%;">
              </div>
            </div>
            `;
          // div = setAttribute('class','form-group row');
          wrapper.appendChild(div);
          
        });

        autre.addEventListener('click', function(event){
          event.preventDefault();
          var div = document.createElement('div');
          div.setAttribute('class','form-group row');
          div.innerHTML = `
                <div class="col-md-6">
                  <label>Libele`+j+`</label>
                  <input type="text" class="form-control" id="autrelibele" name="autrelibele`+j+`" placeholder="Ex: Emplacement">
                </div>
                <div class="col-md-6">
                  <label>valeur`+j+`</label>
                  <input type="text" class="form-control" id="autrevaleur" name="autrevaleur`+j+`" placeholder="Ex: bord de mer">
                </div>
          `;
          autrelayer.appendChild(div);

          var input1 = document.getElementById('autrelibele');   // le bouton ajouter image
          var input2 = document.getElementById('autrevaleur');   // le bouton ajouter image
          input1.removeAttribute('id');
          input2.removeAttribute('id');

          if (j>=10) { //condition d'arret
            autre.setAttribute('style','display: none;')
          }

          j++;

        });

        equip.addEventListener('click', function(event){
          event.preventDefault();

          var val = prompt("Nom de l'Equipement: (*Au plus 10 élémnts à ajouter)", "Lave linge");
          var lib = val;
          var div = document.createElement('div');
          div.setAttribute('class', 'col-xs-6 col-md-6');
          div.innerHTML = `
              <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="`+k+`" name="`+val+`" value ="ok" selected>
                <label class="custom-control-label sized" for="`+k+`">`+lib+`</label>
              </div>
          `;
          if (val == null) {

          }else{
            equiplayer.appendChild(div);
            k++;
          }
          

          if (k>=10) { //condition d'arret
            equip.setAttribute('style','display: none;')
          }

        });
     
      </script>

      
  </body>
</html>
