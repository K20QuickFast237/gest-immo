<div class="popup" id="register">
            <h3>S'inscrire</h3>
            <div class="popup-form">
                <?php if (isset($_SESSION['error'])) { ?>
                    <h4 style="color: red;"> <?= $_SESSION['error'];?> </h4>
                <?php } ?>
                <form action="<?= site_url(array('InscriptionManager','index'));?>" method="POST">
                    <div class="form-field">
                        <input type="text" name="nom" placeholder="Nom d'utilisateur">
                    </div>
                    <div class="form-field">
                        <input type="email" name="email" placeholder="Email">
                    </div>
                    <div class="form-field">
                        <input type="text" name="password" placeholder="Mot de passe">
                    </div>
                    <div class="form-field">
                        <input type="text" name="confirmpassword" placeholder="Confirmer votre mot de passe">
                    </div>
                    <div class="form-cp">
                        <div class="form-field">
                            <div class="input-field">
                                <input type="checkbox" name="verified" id="cc2" value="ok">
                                <label for="cc2">
                                    <span></span>
                                    <small>J'accepte les termes </small>
                                </label>
                            </div>
                        </div>
                        <a href="#" title="" class="signin-op">Avez-vous un compte ?</a>
                    </div><!--form-cp end-->
                    <button type="submit" class="btn2">S'inscrire</button>
                </form>
            </div>
        </div>