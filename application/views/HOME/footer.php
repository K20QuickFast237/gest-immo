
 <a href="#" title="">    
            <section class="cta section-padding">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="cta-text">
                                <h2>Découvrez des biens où vous adorerez rester</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </a>

        <section class="bottom section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-sm-6 col-md-4">
                        <div class="bottom-logo">
                           <img  src="<?= img_url('Vymm.png');?>" alt="VYMMO" class="img-fluid" style="height: 80% ; width:80%;">
                        </div>
                    </div>
                    <div class="col-xl-3 col-sm-6 col-md-3">
                        <div class="bottom-list">
                            <h3>Liens utiles</h3>
                            <ul>
                                <li>
                                    <a href="18_Half_Map.html" title="">Plans</a>
                                </li>
                                <li>
                                    <a href="#" title="">S'inscrirer</a>
                                </li>
                                <li>
                                    <a href="#" title="">Prix</a>
                                </li>
                                <li>
                                    <a href="#" title="">Ajouter une annonce</a>
                                </li>
                            </ul>
                        </div><!--bottom-list end-->
                    </div>
                    <div class="col-xl-5 col-sm-12 col-md-5 pl-0">
                        <div class="bottom-desc">
                            <h3>Information Supplémentaire</h3>
                            <p>L'immobilier de qualité par des personnes qui s'en soucient, VYMMO Votre plateforme de gestion immobilière 100% CM.<i class="fa fa-globe" aria-hidden="true"></i>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>


    </div><!--wrapper end-->

        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="footer-content">
                            <div class="row justify-content-between">
                                <div class="col-xl-6 col-md-6">
                                    <div class="copyright">
                                        <p>&copy;  VYMMO. All Rights Reserved | Designed by Digital Zangalewa</p>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6">
                                    <div class="footer-social">
                                        <a href="#">
                                            <i class="fa fa-facebook"></i>
                                        </a>
                                        <a href="#">
                                            <i class="fa fa-twitter"></i>
                                        </a>
                                        <a href="#">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                        <a href="#">
                                            <i class="fa fa-linkedin"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <!--footer end-->


  <?php 


   // echo js('slick');
   echo js('scripts');


   // echo js('infobox.min');
   // echo js('markerclusterer');
   // echo js('maps');
  
   ?>


    <!-- Maps -->
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDVwc4veKudU0qjYrLrrQXacCkDkcy3AeQ"></script> -->
    <!-- <script src="<?php// echo base_url()."assets/admin/plugins/select2/js/select2.full.min.js"?>"></script> -->

</body>

</html>
