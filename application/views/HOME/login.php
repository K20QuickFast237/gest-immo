	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic" data-tilt>
					<a href="<?php echo site_url(array('Home','index')); ?>">
          				<img  src="<?= img_url('Vymm.png');?>" alt="VYMMO"> 
          			</a>
				</div>

				<form class="login100-form validate-form" action="<?= site_url(array('InscriptionManager','connecte'));?>" method="POST">
					<span class="login100-form-title">
						Connexion
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="email" name="email" placeholder="Email" value="<?=set_value('email');?>">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div class="wrap-input100 validate-input" data-validate = "Password is required">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-lock" aria-hidden="true"></i>
						</span>
					</div>
					
					<div class="container-login100-form-btn">
						<?php if (isset($_SESSION['error'])) { ?>
							<h6 style="color: red;"> <?= $_SESSION['error'];?> </h6>
						<?php } 
						?>
						<button class="login100-form-btn">
							S'identifier
						</button>
					</div>

					<div class="text-center p-t-12">
						<span class="txt1">
						Réinitialiser /
						</span>
						<span><a class="txt" href="<?=site_url(array('HOME','recover'));?>">
							mot de passe oublié ?</a>
						</span>
					</div>

					<div class="text-center p-t-136">
						<span> 	<a  class="txt" href="<?php echo site_url(array('HOME','register')); ?>">
							Créer un compte
							<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i></a>
						</span>
					</div>
				</form>
			</div>
		</div>
	</div>
	
	
