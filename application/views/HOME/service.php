

<div class="wrapper">

    <header class="pb">

        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <a class="navbar-brand" href="<?php echo site_url(array('Home','index')); ?>">
                                <img  src="<?= img_url('logo.png');?>" alt="VYMMO"> 

                            </a>
                            <button class="menu-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent">
                                <span class="icon-spar"></span>
                                <span class="icon-spar"></span>
                                <span class="icon-spar"></span>
                            </button>

                            <div class="navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav mr-auto">
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo site_url(array('Home','echanger')); ?>" data-toggle="#" >
                                            ECHANGER
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="<?php echo site_url(array('Home','acheter')); ?>" data-toggle="#">
                                           ACHETER
                                       </a>
                                   </li>
                                   <li class="nav-item">
                                    <a class="nav-link" href="<?php echo site_url(array('Home','louer')); ?>" data-toggle="#">
                                        LOUER
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="<?php echo site_url(array('Home','service')); ?>" data-toggle="#"style="color: #f0c5a5;">
                                        SERVICES
                                    </a>
                                </li>
                            </ul>
                            <div class="d-inline my-2 my-lg-0">
                                <ul class="navbar-nav">
                                    <li class="nav-item signin-btn">
                                        <?php if (isset($_SESSION['user']['identity'])) { ?>
                                            <a href="<?php echo site_url(array('InscriptionManager','deconnexion')); ?>" class="nav-link">
                                                <i class="la la-sign-in"></i>
                                                <span>
                                                    <b>Deconnexion</b>
                                                </span>
                                            </a>
                                        <?php }else{ ?>
                                            <a href="<?php echo site_url(array('HOME','login')); ?>" class="nav-link">
                                                <i class="la la-sign-in"></i>
                                                <span>
                                                    <b>Connexion</b>
                                                </span>
                                            </a>
                                        <?php } ?>

                                    </li>
                                    <li class="nav-item submit-btn">
                                        <a href="#" class="my-2 my-sm-0 nav-link sbmt-btn">
                                            <i class="fa fa-plus"></i>
                                            <span>Ajouter une annonce</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <a href="#" title="" class="close-menu"><i class="la la-close"></i></a>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>

</header><!--header end-->

<section class="banner hp7">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner-content">
                    <h1>Les meilleures prestataire de <br/>services de l'immobilier.</h1>
                </div>
                <div class="widget-property-search">
                    <form action="#" class="row banner-search">
                        <div class="form_field full">
                            <div class="form-group">
                                <div class="drop-menu">
                                    <div class="select">
                                        <span>RECHERCHER UN SERVICE</span>
                                        <i class="fa fa-angle-down"></i>
                                    </div>
                                    <input type="hidden" name="gender">
                                    <ul class="dropeddown">
                                        <li>PLOMBIER</li>
                                        <li>MENUSIER</li>
                                        <li>ELECTRICIEN</li>
                                        <li>MACON</li>
                                        <li>AGENT IMMOBILIER</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="feat-srch">
                            <div class="more-feat">
                                <!-- <h3> <i class="la la-cog"></i> Afficher plus de fonctionnalités</h3> -->
                            </div><!--more-feat end-->
                            <div class="form_field">
                                <a href="#" class="btn btn-outline-primary ">
                                    <span>RECHERCHER</span>
                                </a>
                            </div>
                        </div>
                        
                    </form><!--baner-form end-->
                </div><!--widget-property-search end-->
            </div>
        </div>
    </div>
</section>

<section class="explore-feature hp7 section-padding">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6">
                <div class="section-heading">
                    <span>Explorer les fonctionnalités</span>
                    <h3>Service dont vous avez besoin</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 col-sm-6 col-md-6 col-lg-4">
                <a href="#" title="">
                    <div class="card">
                        <div class="card-body">
                            <i class="icon-home"></i>
                            <h3>Outils parfaits</h3>
                            <p>Des agents immobiliers, comme la transaction immobilière, la location, la gestion, etc..</p>
                        </div>
                    </div><!--card end-->
                </a>
            </div>
            <div class="col-xl-4 col-sm-6 col-md-6 col-lg-4">
                <a href="#" title="">
                    <div class="card">
                        <div class="card-body">
                            <i class="icon-cursor"></i>
                            <h3>Rechercher en clic</h3>
                            <p>comme les services commerciaux, financiers, les services immobiliers  etc.</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-xl-4 col-sm-6 col-md-6 col-lg-4">
                <a href="#" title="">
                    <div class="card">
                        <div class="card-body">
                            <i class="icon-lock"></i>
                            <h3>Contrôle utilisateur</h3>
                            <p>bancaires, prêts, hyptohèques, juridiques, immobiliers sociaux, notarial immobilier etc.</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>



