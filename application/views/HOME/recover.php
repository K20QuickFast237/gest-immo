	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<div class="login100-pic" data-tilt>
					<a href="<?php echo site_url(array('Home','index')); ?>">
          				<img  src="<?= img_url('Vymm.png');?>" alt="VYMMO"> 
          			</a>
				</div>

				<form class="login100-form validate-form" action="<?= site_url(array('InscriptionManager','recupere'));?>" method="POST">
					<span class="login100-form-title">
						Récuperation de mot de passe
					</span> <br>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="email" name="email" placeholder="Email">
						<span class="focus-input100"></span>
						<span class="symbol-input100">
							<i class="fa fa-envelope" aria-hidden="true"></i>
						</span>
					</div>

					<div >
						<p style="color: black;">
							Vous recevrez un mail de modification de mot de passe.
						</p>
					</div>
					
					<div class="container-login100-form-btn">
						<?php if (isset($_SESSION['error'])) { ?>
							<h6 style="color: red;"> <?= $_SESSION['error'];?> </h6>
						<?php } 
						?>
						<button class="login100-form-btn">
							Recevoir mon Email
						</button>
					</div>
					
				</form>
			</div>
		</div>
	</div>