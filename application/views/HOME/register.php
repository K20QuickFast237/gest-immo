
  <div class="limiter">
    <div class="container-login100">
      <div class="wrap-login100">
        <div class="login100-pic" data-tilt>
         <a href="<?php echo site_url(array('Home','index')); ?>">
             <img  src="<?= img_url('Vymm.png');?>" alt="VYMMO"> 
          </a>
        </div>

        <form class="login100-form validate-form"action="<?= site_url(array('InscriptionManager','index'));?>" method="POST">
          <span class="login100-form-title">
            Inscription
          </span>
          <div class="wrap-input100 validate-input" data-validate = "">
            <input class="input100" type="text" name="nom" placeholder="Nom d'utilisateur" value="<?=set_value('nom');?>">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-user" aria-hidden="true"></i>
            </span>
          </div> 
           <div class="wrap-input100 validate-input" data-validate = "Entrez un email valide: ex@abc.xyz">
            <input class="input100" type="email" name="email" placeholder="Email" value="<?=set_value('email');?>">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-envelope" aria-hidden="true"></i>
            </span>
          </div>

          <div class="wrap-input100 validate-input" data-validate = "Entrez un mot de passe">
            <input class="input100" type="password" name="password" placeholder="mot de passe" value="<?=set_value('password');?>">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-lock" aria-hidden="true"></i>
            </span>
          </div> <div class="wrap-input100 validate-input" data-validate = "Confirmez votre mot de passe">
            <input class="input100" type="password" name="confirmpassword" placeholder="Confirmer votre mot de passe" value="<?=set_value('confirmpassword');?>">
            <span class="focus-input100"></span>
            <span class="symbol-input100">
              <i class="fa fa-lock" aria-hidden="true"></i>
            </span>
          </div>
          <div class="form-cp">
                <div class="form-field">
                    <div class="input-field">
                        <input type="checkbox" name="verified" id="cc2" value="ok">
                          <label for="cc2">
                             <span></span>
                                <small>J'accepte les termes et les conditions.</small>
                          </label>
                    </div>
                </div>
          </div>
          <div class="container-login100-form-btn">
            <?php if (isset($_SESSION['error'])) { ?>
                    <h6 style="color: red;"> <?= $_SESSION['error'];?> </h6>
                <?php } 
            ?>
      
            <button class="login100-form-btn">
             S'inscrire
            </button>
          </div>

          <div class="text-center p-t-12">
            <a class="txt" href="<?php echo site_url(array('HOME','login')); ?>">
              Avez-vous un compte? Connexion
              <i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
            </a>
          </div>

        </form>
      </div>
    </div>
  </div>