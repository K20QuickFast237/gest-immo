
    <!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->


    <div class="wrapper">

        <header class="pb">

            <div class="header">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <nav class="navbar navbar-expand-lg navbar-light">
                                <a class="navbar-brand" href="<?php echo site_url(array('Home','index')); ?>">
                                    <img  src="<?= img_url('logo.png');?>" alt="VYMMO"> 

                                </a>
                                <button class="menu-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent">
                                    <span class="icon-spar"></span>
                                    <span class="icon-spar"></span>
                                    <span class="icon-spar"></span>
                                </button>

                                <div class="navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav mr-auto">
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo site_url(array('Home','echanger')); ?>" data-toggle="#" style="color: #f0c5a5;">
                                                ECHANGER
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo site_url(array('Home','acheter')); ?>" data-toggle="#">
                                               ACHETER
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo site_url(array('Home','louer')); ?>" data-toggle="#">
                                                LOUER
                                            </a>
                                        </li>
                                        <li class="nav-item dropdown">
                                            <a class="nav-link" href="<?php echo site_url(array('Home','service')); ?>" data-toggle="#">
                                                SERVICES
                                            </a>
                                           
                                        </li>
                                    </ul>
                                    <div class="d-inline my-2 my-lg-0">
                                        <ul class="navbar-nav">
                                            <li class="nav-item signin-btn">
                                                <?php if (isset($_SESSION['user']['identity'])) { ?>
                                                <a href="<?php echo site_url(array('InscriptionManager','deconnexion')); ?>" class="nav-link">
                                                    <i class="la la-sign-in"></i>
                                                    <span>
                                                        <b>Deconnexion</b>
                                                    </span>
                                                </a>
                                                <?php }else{ ?>
                                                <a href="<?php echo site_url(array('HOME','login')); ?>" class="nav-link">
                                                    <i class="la la-sign-in"></i>
                                                    <span>
                                                        <b>Connexion</b>
                                                    </span>
                                                </a>
                                                    <?php } ?>

                                            </li>
                                            <li class="nav-item submit-btn">
                                                <a href="#" class="my-2 my-sm-0 nav-link sbmt-btn">
                                                    <i class="fa fa-plus"></i>
                                                    <span>Ajouter une annonce</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <a href="#" title="" class="close-menu"><i class="la la-close"></i></a>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

        </header><!--header end-->

        <section class="banner hp7">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="banner-content">
                            <h1>Trouver les meilleures propriétés à <br/>Échanger dans un endroit</h1>
                        </div>
                        <div class="widget-property-search">
                            <form action="#" class="row banner-search">
                                <div class="form_field">
                                    <div class="form-group">
                                        <div class="drop-menu">
                                            <div class="select">
                                                <span>ECHANGER</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form_field">
                                    <div class="form-group">
                                        <div class="drop-menu">
                                            <div class="select">
                                                <span>VOUS-RECHERCHEZ ?</span>
                                                <i class="fa fa-angle-down"></i>
                                            </div>
                                            <input type="hidden" name="gender">
                                            <ul class="dropeddown">
                                                <li>Appartement</li>
                                                <li>Maison</li>
                                                <li>Villa</li>
                                                <li>Loft</li>
                                                <li>Chambre</li>
                                                <li>Terrain</li>
                                                <li>Propriété</li>
                                                <li>Immeuble</li>
                                                <li>Commerce</li>
                                                <li>Bureau</li>
                                                <li>Magasin</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="form_field">
                                    <div class="form-group ">
                                        <div class="drop-menu">
                                            <div class="select">
                                                <span>LOCALITÉS</span>
                                                <i class="fa fa-angle-down"></i>
                                            </div>
                                            <input type="hidden" name="gender">
                                            <ul class="dropeddown">
                                                <li>Ville 1</li>
                                                <li>Ville 2</li>
                                                <li>Ville 3</li>
                                                <li>Ville 4</li>
                                                <li>Ville 5</li>
                                                <li>Ville 6</li>
                                                <li>Ville 7</li>
                                                <li>Ville 8</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="form_field">
                                    
                                         <a href="#" class="btn btn-outline-primary ">
                                            <span>RECHERCHER</span>
                                         </a>
                                    
                                </div>
                            </form>
                            <!--baner-form end-->
                        </div><!--widget-property-search end-->

                    </div>
                </div>
            </div>
        </section>

        <div class="popup" id="sign-popup">
            <h3>Connectez-vous à votre compte</h3>
            <div class="popup-form">
                <form>
                    <div class="form-field">
                        <input type="text" name="username" placeholder="Nom d'utilisateur">
                    </div>
                    <div class="form-field">
                        <input type="text" name="password" placeholder="Mot de passe">
                    </div>
                    <div class="form-cp">
                        <div class="form-field">
                            <div class="input-field">
                                <input type="checkbox" name="ccc" id="cc1">
                                <label for="cc1">
                                    <span></span>
                                    <small>se souvenir de moi</small>
                                </label>
                            </div>
                        </div>
                        <a href="#" title="">Mot de passe oublié?</a>
                    </div><!--form-cp end-->
                    <button type="submit" class="btn2">S'identifier</button>
                </form>
                <!-- <a href="#" title="" class="fb-btn"> <i class="fa fa-facebook"></i>Connectez-vous avec Facebook</a> -->
            </div>
        </div><!--popup end-->

        <div class="popup" id="register-popup">
            <h3>S'inscrire</h3>
            <div class="popup-form">
                <form>
                    <div class="form-field">
                        <input type="text" name="username" placeholder="Nom d'utilisateur">
                    </div>
                    <div class="form-field">
                        <input type="text" name="email" placeholder="Email">
                    </div>
                    <div class="form-field">
                        <input type="text" name="password" placeholder="Mot de passe">
                    </div>
                    <div class="form-field">
                        <input type="text" name="password" placeholder="Confirmer votre mot de passe">
                    </div>
                    <div class="form-cp">
                        <div class="form-field">
                            <div class="input-field">
                                <input type="checkbox" name="ccc" id="cc2">
                                <label for="cc2">
                                    <span></span>
                                    <small>J'accepte les termes </small>
                                </label>
                            </div>
                        </div>
                        <a href="#" title="" class="signin-op">Avez-vous un compte ?</a>
                    </div><!--form-cp end-->
                    <button type="submit" class="btn2">S'inscrire</button>
                </form>
            </div>
        </div><!--popup end-->

        <section class="popular-listing hp2 section-padding">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6">
                        <div class="section-heading">
                            <h3>Echanges populaires</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="card">
                            <a href="24_Property_Single.html" title="">
                                <div class="img-block">
                                    <div class="overlay"></div>
                                    <img src="https://via.placeholder.com/370x295" alt="" class="img-fluid">
                                    <div class="rate-info">
                                        <h5>550.000 XAF</h5>
                                        <span>A Louer</span>
                                    </div>
                                </div>
                            </a>
                            <div class="card-body">
                                <a href="24_Property_Single.html" title="">
                                    <h3>Appartements Traditionnels</h3>
                                    <p><i class="la la-map-marker"></i>Rue Joffre , Akwa</p>
                                </a>
                                <ul>
                                    <li>3 Salles de bain</li>
                                    <li>2 Lits</li>
                                    <li>Superficie 555 m carrés</li>
                                </ul>
                            </div>
                            <div class="card-footer">
                                <a href="#" class="pull-left">
                                    <i class="la la-heart-o"></i>
                                </a>
                                <a href="#" class="pull-right">
                                    <i class="la la-calendar-check-o"></i> Il y a 25 jours</a>
                            </div>
                            <a href="24_Property_Single.html" title="" class="ext-link"></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="card">
                            <a href="24_Property_Single.html" title="">
                                <div class="img-block">
                                    <div class="overlay"></div>
                                    <img src="https://via.placeholder.com/370x295" alt="" class="img-fluid">
                                    <div class="rate-info">
                                        <h5>550.000 XAF</h5>
                                        <span>A Louer</span>
                                    </div>
                                </div>
                            </a>
                            <div class="card-body">
                                <a href="24_Property_Single.html" title="">
                                    <h3>Appartements Traditionnel</h3>
                                    <p><i class="la la-map-marker"></i>Ndokotti, Douala</p>
                                </a>
                                <ul>
                                    <li>3 Salles de bain</li>
                                    <li>2 Lits</li>
                                    <li>Superficie 555 m carrés</li>
                                </ul>
                            </div>
                            <div class="card-footer">
                                <a href="#" class="pull-left">
                                    <i class="la la-heart-o"></i>
                                </a>
                                <a href="#" class="pull-right">
                                    <i class="la la-calendar-check-o"></i> Il y a 20 jours</a>
                            </div>
                            <a href="24_Property_Single.html" title="" class="ext-link"></a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="card">
                            <a href="24_Property_Single.html" title="">
                                <div class="img-block">
                                    <div class="overlay"></div>
                                    <img src="https://via.placeholder.com/370x295" alt="" class="img-fluid">
                                    <div class="rate-info">
                                        <h5>750.000 XAF</h5>
                                        <span>A Louer</span>
                                    </div>
                                </div>
                            </a>
                            <div class="card-body">
                                <a href="24_Property_Single.html" title="">
                                    <h3> Appartements</h3>
                                    <p><i class="la la-map-marker"></i>Rue Koloko, Bonapriso </p>
                                </a>
                                <ul>
                                    <li>3 Salles de Bains</li>
                                    <li>2 Lits</li>
                                    <li>Superficie 655 m carrés</li>
                                </ul>
                            </div>
                            <div class="card-footer">
                                <a href="#" class="pull-left">
                                    <i class="la la-heart-o"></i>
                                </a>
                                <a href="#" class="pull-right">
                                    <i class="la la-calendar-check-o"></i> 25 Days Ago</a>
                            </div>
                            <a href="24_Property_Single.html" title="" class="ext-link"></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
 
       
