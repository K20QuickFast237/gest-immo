<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <div class="login100-pic" data-tilt>
                <a href="<?php echo site_url(array('Home','index')); ?>">
                  <img  src="<?= img_url('Vymm.png');?>" alt="VYMMO"> 
              </a>
          </div>

          <form class="login100-form validate-form" action="<?= site_url(array('InscriptionManager','saveRecuperation'));?>" method="POST">
            <span class="login100-form-title">
                Changez votre mot de passe
            </span> <br>

                <input class="input100" type="hidden" name="email" placeholder="Email" value=<?='"'.$_SESSION['email'].'"';?>>

            <div class="wrap-input100 validate-input" data-validate = "Password is required">
                <input class="input100" type="password" name="password" placeholder="mot de passe" value="<?=set_value('password');?>">
                <span class="focus-input100"></span>
                <span class="symbol-input100">
                    <i class="fa fa-lock" aria-hidden="true"></i>
                </span>
            </div>

            <div class="wrap-input100 validate-input" data-validate = "confirmation is required">
                <input class="input100" type="password" name="confirmpassword" placeholder="confirmation mot de passe" value="<?=set_value('confirmpassword');?>">
                <span class="focus-input100"></span>
                <span class="symbol-input100">
                    <i class="fa fa-lock" aria-hidden="true"></i>
                </span>
            </div>

            <div class="container-login100-form-btn">
                <button class="login100-form-btn">
                    Mettre A Jour
                </button>
            </div>

          </form>
        </div>
    </div>
</div>