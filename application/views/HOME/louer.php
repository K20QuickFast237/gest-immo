
    <!--[if lte IE 9]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
  <![endif]-->


    <div class="wrapper">

        <header class="pb">

            <div class="header">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <nav class="navbar navbar-expand-lg navbar-light">
                                <a class="navbar-brand" href="<?php echo site_url(array('Home','index')); ?>">
                                    <img  src="<?= img_url('logo.png');?>" alt="VYMMO"> 

                                </a>
                                <button class="menu-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent">
                                    <span class="icon-spar"></span>
                                    <span class="icon-spar"></span>
                                    <span class="icon-spar"></span>
                                </button>

                                <div class="navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav mr-auto">
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo site_url(array('Home','echanger')); ?>" data-toggle="#" >
                                                ECHANGER
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo site_url(array('Home','acheter')); ?>" data-toggle="#">
                                               ACHETER
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo site_url(array('Home','louer')); ?>" data-toggle="#"style="color: #f0c5a5;">
                                                LOUER
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo site_url(array('Home','service')); ?>" data-toggle="#">
                                                SERVICES
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="d-inline my-2 my-lg-0">
                                        <ul class="navbar-nav">
                                            <li class="nav-item signin-btn">
                                                <?php if (isset($_SESSION['user']['identity'])) { ?>
                                                <a href="<?php echo site_url(array('InscriptionManager','deconnexion')); ?>" class="nav-link">
                                                    <i class="la la-sign-in"></i>
                                                    <span>
                                                        <b>Deconnexion</b>
                                                    </span>
                                                </a>
                                                <?php }else{ ?>
                                                <a href="<?php echo site_url(array('HOME','login')); ?>" class="nav-link">
                                                    <i class="la la-sign-in"></i>
                                                    <span>
                                                        <b>Connexion</b>
                                                    </span>
                                                </a>
                                                    <?php } ?>

                                            </li>
                                            <li class="nav-item submit-btn">
                                                <a href="#" class="my-2 my-sm-0 nav-link sbmt-btn">
                                                    <i class="fa fa-plus"></i>
                                                    <span>Ajouter une annonce</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <a href="#" title="" class="close-menu"><i class="la la-close"></i></a>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>

        </header><!--header end-->

        <section class="banner hp7">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="banner-content">
                            <h1>Trouver les meilleures propriétés <br> à Louer</h1>
                        </div>
                        <div class="widget-property-search">
                            <form action="#" class="row banner-search">
                                <div class="form_field">
                                    <div class="form-group">
                                        <div class="drop-menu">
                                            <div class="select">
                                                <span>LOUER</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form_field">
                                    <div class="form-group">
                                        <div class="drop-menu">
                                            <div class="select">
                                                <span>VOUS-RECHERCHEZ ?</span>
                                                <i class="fa fa-angle-down"></i>
                                            </div>
                                            <input type="hidden" name="gender">
                                            <ul class="dropeddown">
                                                <li>Appartement</li>
                                                <li>Maison</li>
                                                <li>Villa</li>
                                                <li>Loft</li>
                                                <li>Chambre</li>
                                                <li>Terrain</li>
                                                <li>Propriété</li>
                                                <li>Immeuble</li>
                                                <li>Commerce</li>
                                                <li>Bureau</li>
                                                <li>Magasin</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="form_field">
                                    <div class="form-group ">
                                        <div class="drop-menu">
                                            <div class="select">
                                                <span>LOCALITÉS</span>
                                                <i class="fa fa-angle-down"></i>
                                            </div>
                                            <input type="hidden" name="gender">
                                            <ul class="dropeddown">
                                                <li>Ville 1</li>
                                                <li>Ville 2</li>
                                                <li>Ville 3</li>
                                                <li>Ville 4</li>
                                                <li>Ville 5</li>
                                                <li>Ville 6</li>
                                                <li>Ville 7</li>
                                                <li>Ville 8</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="form_field">
                                    <div class="form-group">
                                        <div class="form_field full">
                                             <input type="text" class="form-control" placeholder="BUDGET MAX [XAF]">
                                        </div>
                                    </div>
                                </div>
                                <div class="feat-srch">
                                    <div class="more-feat">
                                        <!-- <h3> <i class="la la-cog"></i> Afficher plus de fonctionnalités</h3> -->
                                    </div><!--more-feat end-->
                                    <div class="form_field">
                                        <a href="#" class="btn btn-outline-primary ">
                                            <span>RECHERCHER</span>
                                        </a>
                                    </div>
                                </div><!--more-feat end-->
                                <div class="features_list">
                                    <ul>
                                        <li class="input-field">
                                            <input type="checkbox" name="bb" id="b1">
                                            <label for="b1">
                                                <span></span>
                                                <small>Parking</small>
                                            </label>
                                        </li>
                                        <li class="input-field">
                                            <input type="checkbox" name="bb" id="b2">
                                            <label for="b2">
                                                <span></span>
                                                <small>Gym</small>
                                            </label>
                                        </li>
                                        <li class="input-field">
                                            <input type="checkbox" name="bb" id="b3">
                                            <label for="b3">
                                                <span></span>
                                                <small>Machine à laver</small>
                                            </label>
                                        </li>
                                        <li class="input-field">
                                            <input type="checkbox" name="bb" id="b4">
                                            <label for="b4">
                                                <span></span>
                                                <small>Ascenseur</small>
                                            </label>
                                        </li>
                                        <li class="input-field">
                                            <input type="checkbox" name="bb" id="b5">
                                            <label for="b5">
                                                <span></span>
                                                <small>Espace de rangement</small>
                                            </label>
                                        </li>
                                        <li class="input-field">
                                            <input type="checkbox" name="bb" id="b6">
                                            <label for="b6">
                                                <span></span>
                                                <small>Climatiseur</small>
                                            </label>
                                        </li>
                                        <li class="input-field">
                                            <input type="checkbox" name="bb" id="b7">
                                            <label for="b7">
                                                <span></span>
                                                <small>Internet sans fil</small>
                                            </label>
                                        </li>
                                        <li class="input-field">
                                            <input type="checkbox" name="bb" id="b8">
                                            <label for="b8">
                                                <span></span>
                                                <small>Chauffage</small>
                                            </label>
                                        </li>
                                        <li class="input-field">
                                            <input type="checkbox" name="bb" id="b9">
                                            <label for="b9">
                                                <span></span>
                                                <small>Piscine</small>
                                            </label>
                                        </li>
                                        <li class="input-field">
                                            <input type="checkbox" name="bb" id="b10">
                                            <label for="b10">
                                                <span></span>
                                                <small>Cinéma maison</small>
                                            </label>
                                        </li>
                                    </ul>
                                </div><!--features_list end-->
                            </form><!--baner-form end-->
                        </div><!--widget-property-search end-->
                    </div>
                </div>
            </div>
        </section>

        <div class="popup" id="sign-popup">
            <h3>Connectez-vous à votre compte</h3>
            <div class="popup-form">
                <form>
                    <div class="form-field">
                        <input type="text" name="username" placeholder="Nom d'utilisateur">
                    </div>
                    <div class="form-field">
                        <input type="text" name="password" placeholder="Mot de passe">
                    </div>
                    <div class="form-cp">
                        <div class="form-field">
                            <div class="input-field">
                                <input type="checkbox" name="ccc" id="cc1">
                                <label for="cc1">
                                    <span></span>
                                    <small>se souvenir de moi</small>
                                </label>
                            </div>
                        </div>
                        <a href="#" title="">Mot de passe oublié?</a>
                    </div><!--form-cp end-->
                    <button type="submit" class="btn2">S'identifier</button>
                </form>
                <!-- <a href="#" title="" class="fb-btn"> <i class="fa fa-facebook"></i>Connectez-vous avec Facebook</a> -->
            </div>
        </div><!--popup end-->

        <div class="popup" id="register-popup">
            <h3>S'inscrire</h3>
            <div class="popup-form">
                <form>
                    <div class="form-field">
                        <input type="text" name="username" placeholder="Nom d'utilisateur">
                    </div>
                    <div class="form-field">
                        <input type="text" name="email" placeholder="Email">
                    </div>
                    <div class="form-field">
                        <input type="text" name="password" placeholder="Mot de passe">
                    </div>
                    <div class="form-field">
                        <input type="text" name="password" placeholder="Confirmer votre mot de passe">
                    </div>
                    <div class="form-cp">
                        <div class="form-field">
                            <div class="input-field">
                                <input type="checkbox" name="ccc" id="cc2">
                                <label for="cc2">
                                    <span></span>
                                    <small>J'accepte les termes </small>
                                </label>
                            </div>
                        </div>
                        <a href="#" title="" class="signin-op">Avez-vous un compte ?</a>
                    </div><!--form-cp end-->
                    <button type="submit" class="btn2">S'inscrire</button>
                </form>
            </div>
        </div><!--popup end-->

        <section class="listing-main-sec section-padding2">
            <div class="container">
                <div class="listing-main-sec-details">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="listing-directs">
                                <div class="list-head">
                                    <div class="sortby">
                                        <span>Trier par:</span>
                                        <div class="drop-menu">
                                            <div class="select">
                                                <span>Pertinent</span>
                                                <i class="la la-caret-down"></i>
                                            </div>
                                            <input type="hidden" name="gender">
                                            <ul class="dropeddown">
                                                <li>A Vendre</li>
                                                <li>A Louer</li>
                                            </ul>
                                        </div>
                                    </div><!--sortby end-->
                                    <div class="view-change">
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link" id="grid-tab1" data-toggle="tab" href="#grid-view-tab1" role="tab" aria-controls="grid-view-tab1" aria-selected="true"><i class="la la-th-large"></i></a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link active" id="grid-tab2" data-toggle="tab" href="#grid-view-tab2" role="tab" aria-controls="grid-view-tab2" aria-selected="true"><i class="la la-th-list"></i></a>
                                            </li>
                                        </ul>
                                    </div><!--view-change end-->
                                </div><!--list-head end-->
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade" id="grid-view-tab1" role="tabpanel" aria-labelledby="grid-view-tab1">
                                        <div class="list_products">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="card">
                                                        <a href="24_Property_Single.html" title="">
                                                            <div class="img-block">
                                                                <div class="overlay"></div>
                                                                <img src="https://via.placeholder.com/370x295" alt="" class="img-fluid">
                                                                <div class="rate-info">
                                                                    <h5>$550.000</h5>
                                                                    <span>For Rent</span>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <div class="card-body">
                                                            <a href="24_Property_Single.html" title="">
                                                                <h3>Traditional Apartments</h3>
                                                                <p>
                                                                    <i class="la la-map-marker"></i>212 5th Ave, New York
                                                                </p>
                                                            </a>
                                                            <ul>
                                                                <li>3 Bathrooms</li>
                                                                <li>2 Beds</li>
                                                                <li>Area 555 Sq Ft</li>
                                                            </ul>
                                                        </div>
                                                        <div class="card-footer">
                                                            <a href="#" class="pull-left">
                                                                <i class="la la-heart-o"></i>
                                                            </a>
                                                            <a href="#" class="pull-right">
                                                                <i class="la la-calendar-check-o"></i> 25 Days Ago</a>
                                                        </div>
                                                        <a href="24_Property_Single.html" title="" class="ext-link"></a>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="card">
                                                        <a href="24_Property_Single.html" title="">
                                                            <div class="img-block">
                                                                <div class="overlay"></div>
                                                                <img src="https://via.placeholder.com/370x295" alt="" class="img-fluid">
                                                                <div class="rate-info">
                                                                    <h5>$550.000</h5>
                                                                    <span>For Rent</span>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <div class="card-body">
                                                            <a href="24_Property_Single.html" title="">
                                                                <h3>Traditional Apartments</h3>
                                                                <p>
                                                                    <i class="la la-map-marker"></i>212 5th Ave, New York
                                                                </p>
                                                            </a>
                                                            <ul>
                                                                <li>3 Bathrooms</li>
                                                                <li>2 Beds</li>
                                                                <li>Area 555 Sq Ft</li>
                                                            </ul>
                                                        </div>
                                                        <div class="card-footer">
                                                            <a href="#" class="pull-left">
                                                                <i class="la la-heart-o"></i>
                                                            </a>
                                                            <a href="#" class="pull-right">
                                                                <i class="la la-calendar-check-o"></i> 25 Days Ago</a>
                                                        </div>
                                                        <a href="24_Property_Single.html" title="" class="ext-link"></a>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="card">
                                                        <a href="24_Property_Single.html" title="">
                                                            <div class="img-block">
                                                                <div class="overlay"></div>
                                                                <img src="https://via.placeholder.com/370x295" alt="" class="img-fluid">
                                                                <div class="rate-info">
                                                                    <h5>$550.000</h5>
                                                                    <span>For Rent</span>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <div class="card-body">
                                                            <a href="24_Property_Single.html" title="">
                                                                <h3>Traditional Apartments</h3>
                                                                <p>
                                                                    <i class="la la-map-marker"></i>212 5th Ave, New York
                                                                </p>
                                                            </a>
                                                            <ul>
                                                                <li>3 Bathrooms</li>
                                                                <li>2 Beds</li>
                                                                <li>Area 555 Sq Ft</li>
                                                            </ul>
                                                        </div>
                                                        <div class="card-footer">
                                                            <a href="#" class="pull-left">
                                                                <i class="la la-heart-o"></i>
                                                            </a>
                                                            <a href="#" class="pull-right">
                                                                <i class="la la-calendar-check-o"></i> 25 Days Ago</a>
                                                        </div>
                                                        <a href="24_Property_Single.html" title="" class="ext-link"></a>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="card">
                                                        <a href="24_Property_Single.html" title=""> 
                                                            <div class="img-block">
                                                                <div class="overlay"></div>
                                                                <img src="https://via.placeholder.com/370x295" alt="" class="img-fluid">
                                                                <div class="rate-info">
                                                                    <h5>$550.000</h5>
                                                                    <span>For Rent</span>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <div class="card-body">
                                                            <a href="24_Property_Single.html" title="">
                                                                <h3>Traditional Apartments</h3>
                                                                <p>
                                                                    <i class="la la-map-marker"></i>212 5th Ave, New York
                                                                </p>
                                                            </a>
                                                            <ul>
                                                                <li>3 Bathrooms</li>
                                                                <li>2 Beds</li>
                                                                <li>Area 555 Sq Ft</li>
                                                            </ul>
                                                        </div>
                                                        <div class="card-footer">
                                                            <a href="#" class="pull-left">
                                                                <i class="la la-heart-o"></i>
                                                            </a>
                                                            <a href="#" class="pull-right">
                                                                <i class="la la-calendar-check-o"></i> 25 Days Ago</a>
                                                        </div>
                                                        <a href="24_Property_Single.html" title="" class="ext-link"></a>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="card">
                                                        <a href="24_Property_Single.html" title="">
                                                            <div class="img-block">
                                                                <div class="overlay"></div>
                                                                <img src="https://via.placeholder.com/370x295" alt="" class="img-fluid">
                                                                <div class="rate-info">
                                                                    <h5>$550.000</h5>
                                                                    <span>For Rent</span>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <div class="card-body">
                                                            <a href="24_Property_Single.html" title="">
                                                                <h3>Traditional Apartments</h3>
                                                                <p>
                                                                    <i class="la la-map-marker"></i>212 5th Ave, New York
                                                                </p>
                                                            </a>
                                                            <ul>
                                                                <li>3 Bathrooms</li>
                                                                <li>2 Beds</li>
                                                                <li>Area 555 Sq Ft</li>
                                                            </ul>
                                                        </div>
                                                        <div class="card-footer">
                                                            <a href="#" class="pull-left">
                                                                <i class="la la-heart-o"></i>
                                                            </a>
                                                            <a href="#" class="pull-right">
                                                                <i class="la la-calendar-check-o"></i> 25 Days Ago</a>
                                                        </div>
                                                        <a href="24_Property_Single.html" title="" class="ext-link"></a>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="card">
                                                        <a href="24_Property_Single.html" title="">
                                                            <div class="img-block">
                                                                <div class="overlay"></div>
                                                                <img src="https://via.placeholder.com/370x295" alt="" class="img-fluid">
                                                                <div class="rate-info">
                                                                    <h5>$550.000</h5>
                                                                    <span>For Rent</span>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <div class="card-body">
                                                            <a href="24_Property_Single.html" title="">
                                                                <h3>Traditional Apartments</h3>
                                                                <p>
                                                                    <i class="la la-map-marker"></i>212 5th Ave, New York
                                                                </p>
                                                            </a>
                                                            <ul>
                                                                <li>3 Bathrooms</li>
                                                                <li>2 Beds</li>
                                                                <li>Area 555 Sq Ft</li>
                                                            </ul>
                                                        </div>
                                                        <div class="card-footer">
                                                            <a href="#" class="pull-left">
                                                                <i class="la la-heart-o"></i>
                                                            </a>
                                                            <a href="#" class="pull-right">
                                                                <i class="la la-calendar-check-o"></i> 25 Days Ago</a>
                                                        </div>
                                                        <a href="24_Property_Single.html" title="" class="ext-link"></a>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="card">
                                                        <a href="24_Property_Single.html" title="">
                                                            <div class="img-block">
                                                                <div class="overlay"></div>
                                                                <img src="https://via.placeholder.com/370x295" alt="" class="img-fluid">
                                                                <div class="rate-info">
                                                                    <h5>$550.000</h5>
                                                                    <span>For Rent</span>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <div class="card-body">
                                                            <a href="24_Property_Single.html" title="">
                                                                <h3>Traditional Apartments</h3>
                                                                <p>
                                                                    <i class="la la-map-marker"></i>212 5th Ave, New York
                                                                </p>
                                                            </a>
                                                            <ul>
                                                                <li>3 Bathrooms</li>
                                                                <li>2 Beds</li>
                                                                <li>Area 555 Sq Ft</li>
                                                            </ul>
                                                        </div>
                                                        <div class="card-footer">
                                                            <a href="#" class="pull-left">
                                                                <i class="la la-heart-o"></i>
                                                            </a>
                                                            <a href="#" class="pull-right">
                                                                <i class="la la-calendar-check-o"></i> 25 Days Ago</a>
                                                        </div>
                                                        <a href="24_Property_Single.html" title="" class="ext-link"></a>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6">
                                                    <div class="card">
                                                        <a href="24_Property_Single.html" title="">
                                                            <div class="img-block">
                                                                <div class="overlay"></div>
                                                                <img src="https://via.placeholder.com/370x295" alt="" class="img-fluid">
                                                                <div class="rate-info">
                                                                    <h5>$550.000</h5>
                                                                    <span>For Rent</span>
                                                                </div>
                                                            </div>
                                                        </a>
                                                        <div class="card-body">
                                                            <a href="24_Property_Single.html" title="">
                                                                <h3>Traditional Apartments</h3>
                                                                <p>
                                                                    <i class="la la-map-marker"></i>212 5th Ave, New York
                                                                </p>
                                                            </a>
                                                            <ul>
                                                                <li>3 Bathrooms</li>
                                                                <li>2 Beds</li>
                                                                <li>Area 555 Sq Ft</li>
                                                            </ul>
                                                        </div>
                                                        <div class="card-footer">
                                                            <a href="#" class="pull-left">
                                                                <i class="la la-heart-o"></i>
                                                            </a>
                                                            <a href="#" class="pull-right">
                                                                <i class="la la-calendar-check-o"></i> 25 Days Ago</a>
                                                        </div>
                                                        <a href="24_Property_Single.html" title="" class="ext-link"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!--list_products end-->
                                        
                                    </div>
                                    <div class="tab-pane fade show active" id="grid-view-tab2" role="tabpanel" aria-labelledby="grid-view-tab2">
                                        <div class="list-products">
                                            <div class="card">
                                                <a href="24_Property_Single.html" title="">
                                                    <div class="img-block">
                                                        <div class="overlay"></div>
                                                        <img src="https://via.placeholder.com/370x295" alt="" class="img-fluid">
                                                        <div class="rate-info">
                                                            <h5>$550.000</h5>
                                                            <span>For Rent</span>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="card_bod_full">
                                                    <div class="card-body">
                                                        <a href="24_Property_Single.html" title="">
                                                            <h3>Traditional Apartments</h3>
                                                            <p><i class="la la-map-marker"></i>212 5th Ave, New York</p>
                                                        </a>
                                                        <ul>
                                                            <li>3 Bathrooms</li>
                                                            <li>2 Beds</li>
                                                            <li>Area 555 Sq Ft</li>
                                                        </ul>
                                                    </div>
                                                    <div class="card-footer">
                                                        <div class="crd-links">
                                                            <a href="#" class="pull-left">
                                                                <i class="la la-heart-o"></i>
                                                            </a>
                                                            <a href="#" class="plf">
                                                                <i class="la la-calendar-check-o"></i> 25 Days Ago
                                                            </a>
                                                        </div><!--crd-links end-->
                                                        <a href="24_Property_Single.html" title="" class="btn-default">View Details</a>
                                                    </div>
                                                </div><!--card_bod_full end-->
                                                <a href="24_Property_Single.html" title="" class="ext-link"></a>
                                            </div><!--card end-->
                                            <div class="card">
                                                <a href="24_Property_Single.html" title="">
                                                    <div class="img-block">
                                                        <div class="overlay"></div>
                                                        <img src="https://via.placeholder.com/370x295" alt="" class="img-fluid">
                                                        <div class="rate-info">
                                                            <h5>$550.000</h5>
                                                            <span>For Rent</span>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="card_bod_full">
                                                    <div class="card-body">
                                                        <a href="24_Property_Single.html" title="">
                                                            <h3>Traditional Apartments</h3>
                                                            <p><i class="la la-map-marker"></i>212 5th Ave, New York</p>
                                                        </a>
                                                        <ul>
                                                            <li>3 Bathrooms</li>
                                                            <li>2 Beds</li>
                                                            <li>Area 555 Sq Ft</li>
                                                        </ul>
                                                    </div>
                                                    <div class="card-footer">
                                                        <div class="crd-links">
                                                            <a href="#" class="pull-left">
                                                                <i class="la la-heart-o"></i>
                                                            </a>
                                                            <a href="#" class="plf">
                                                                <i class="la la-calendar-check-o"></i> 25 Days Ago
                                                            </a>
                                                        </div><!--crd-links end-->
                                                        <a href="24_Property_Single.html" title="" class="btn-default">View Details</a>
                                                    </div>
                                                </div><!--card_bod_full end-->
                                                <a href="24_Property_Single.html" title="" class="ext-link"></a>
                                            </div><!--card end-->
                                            <div class="card">
                                                <a href="24_Property_Single.html" title="">
                                                    <div class="img-block">
                                                        <div class="overlay"></div>
                                                        <img src="https://via.placeholder.com/370x295" alt="" class="img-fluid">
                                                        <div class="rate-info">
                                                            <h5>$550.000</h5>
                                                            <span>For Rent</span>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="card_bod_full">
                                                    <div class="card-body">
                                                        <a href="24_Property_Single.html" title="">
                                                            <h3>Traditional Apartments</h3>
                                                            <p><i class="la la-map-marker"></i>212 5th Ave, New York</p>
                                                        </a>
                                                        <ul>
                                                            <li>3 Bathrooms</li>
                                                            <li>2 Beds</li>
                                                            <li>Area 555 Sq Ft</li>
                                                        </ul>
                                                    </div>
                                                    <div class="card-footer">
                                                        <div class="crd-links">
                                                            <a href="#" class="pull-left">
                                                                <i class="la la-heart-o"></i>
                                                            </a>
                                                            <a href="#" class="plf">
                                                                <i class="la la-calendar-check-o"></i> 25 Days Ago
                                                            </a>
                                                        </div><!--crd-links end-->
                                                        <a href="24_Property_Single.html" title="" class="btn-default">View Details</a>
                                                    </div>
                                                </div><!--card_bod_full end-->
                                                <a href="24_Property_Single.html" title="" class="ext-link"></a>
                                            </div><!--card end-->
                                            <div class="card">
                                                <a href="24_Property_Single.html" title="">
                                                    <div class="img-block">
                                                        <div class="overlay"></div>
                                                        <img src="https://via.placeholder.com/370x295" alt="" class="img-fluid">
                                                        <div class="rate-info">
                                                            <h5>$550.000</h5>
                                                            <span>For Rent</span>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="card_bod_full">
                                                    <div class="card-body">
                                                        <a href="24_Property_Single.html" title="">
                                                            <h3>Traditional Apartments</h3>
                                                            <p><i class="la la-map-marker"></i>212 5th Ave, New York</p>
                                                        </a>
                                                        <ul>
                                                            <li>3 Bathrooms</li>
                                                            <li>2 Beds</li>
                                                            <li>Area 555 Sq Ft</li>
                                                        </ul>
                                                    </div>
                                                    <div class="card-footer">
                                                        <div class="crd-links">
                                                            <a href="#" class="pull-left">
                                                                <i class="la la-heart-o"></i>
                                                            </a>
                                                            <a href="#" class="plf">
                                                                <i class="la la-calendar-check-o"></i> 25 Days Ago
                                                            </a>
                                                        </div><!--crd-links end-->
                                                        <a href="24_Property_Single.html" title="" class="btn-default">View Details</a>
                                                    </div>
                                                </div><!--card_bod_full end-->
                                                <a href="24_Property_Single.html" title="" class="ext-link"></a>
                                            </div><!--card end-->
                                            <div class="card">
                                                <a href="24_Property_Single.html" title="">
                                                    <div class="img-block">
                                                        <div class="overlay"></div>
                                                        <img src="https://via.placeholder.com/370x295" alt="" class="img-fluid">
                                                        <div class="rate-info">
                                                            <h5>$550.000</h5>
                                                            <span>For Rent</span>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="card_bod_full">
                                                    <div class="card-body">
                                                        <a href="24_Property_Single.html" title="">
                                                            <h3>Traditional Apartments</h3>
                                                            <p><i class="la la-map-marker"></i>212 5th Ave, New York</p>
                                                        </a>
                                                        <ul>
                                                            <li>3 Bathrooms</li>
                                                            <li>2 Beds</li>
                                                            <li>Area 555 Sq Ft</li>
                                                        </ul>
                                                    </div>
                                                    <div class="card-footer">
                                                        <div class="crd-links">
                                                            <a href="#" class="pull-left">
                                                                <i class="la la-heart-o"></i>
                                                            </a>
                                                            <a href="#" class="plf">
                                                                <i class="la la-calendar-check-o"></i> 25 Days Ago
                                                            </a>
                                                        </div><!--crd-links end-->
                                                        <a href="24_Property_Single.html" title="" class="btn-default">View Details</a>
                                                    </div>
                                                </div><!--card_bod_full end-->
                                                <a href="24_Property_Single.html" title="" class="ext-link"></a>
                                            </div><!--card end-->
                                            <div class="card">
                                                <a href="24_Property_Single.html" title="">
                                                    <div class="img-block">
                                                        <div class="overlay"></div>
                                                        <img src="https://via.placeholder.com/370x295" alt="" class="img-fluid">
                                                        <div class="rate-info">
                                                            <h5>$550.000</h5>
                                                            <span>For Rent</span>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="card_bod_full">
                                                    <div class="card-body">
                                                        <a href="24_Property_Single.html" title="">
                                                            <h3>Traditional Apartments</h3>
                                                            <p><i class="la la-map-marker"></i>212 5th Ave, New York</p>
                                                        </a>
                                                        <ul>
                                                            <li>3 Bathrooms</li>
                                                            <li>2 Beds</li>
                                                            <li>Area 555 Sq Ft</li>
                                                        </ul>
                                                    </div>
                                                    <div class="card-footer">
                                                        <div class="crd-links">
                                                            <a href="#" class="pull-left">
                                                                <i class="la la-heart-o"></i>
                                                            </a>
                                                            <a href="#" class="plf">
                                                                <i class="la la-calendar-check-o"></i> 25 Days Ago
                                                            </a>
                                                        </div><!--crd-links end-->
                                                        <a href="24_Property_Single.html" title="" class="btn-default">View Details</a>
                                                    </div>
                                                </div><!--card_bod_full end-->
                                                <a href="24_Property_Single.html" title="" class="ext-link"></a>
                                            </div><!--card end-->
                                            <div class="card">
                                                <a href="24_Property_Single.html" title="">
                                                    <div class="img-block">
                                                        <div class="overlay"></div>
                                                        <img src="https://via.placeholder.com/295x235" alt="" class="img-fluid">
                                                        <div class="rate-info">
                                                            <h5>$550.000</h5>
                                                            <span>For Rent</span>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="card_bod_full">
                                                    <div class="card-body">
                                                        <a href="24_Property_Single.html" title="">
                                                            <h3>Traditional Apartments</h3>
                                                            <p><i class="la la-map-marker"></i>212 5th Ave, New York</p>
                                                        </a>
                                                        <ul>
                                                            <li>3 Bathrooms</li>
                                                            <li>2 Beds</li>
                                                            <li>Area 555 Sq Ft</li>
                                                        </ul>
                                                    </div>
                                                    <div class="card-footer">
                                                        <div class="crd-links">
                                                            <a href="#" class="pull-left">
                                                                <i class="la la-heart-o"></i>
                                                            </a>
                                                            <a href="#" class="plf">
                                                                <i class="la la-calendar-check-o"></i> 25 Days Ago
                                                            </a>
                                                        </div><!--crd-links end-->
                                                        <a href="24_Property_Single.html" title="" class="btn-default">View Details</a>
                                                    </div>
                                                </div><!--card_bod_full end-->
                                                <a href="24_Property_Single.html" title="" class="ext-link"></a>
                                            </div><!--card end-->
                                            <div class="card">
                                                <a href="24_Property_Single.html" title="">
                                                    <div class="img-block">
                                                        <div class="overlay"></div>
                                                        <img src="https://via.placeholder.com/295x235" alt="" class="img-fluid">
                                                        <div class="rate-info">
                                                            <h5>$550.000</h5>
                                                            <span>For Rent</span>
                                                        </div>
                                                    </div>
                                                </a>
                                                <div class="card_bod_full">
                                                    <div class="card-body">
                                                        <a href="24_Property_Single.html" title="">
                                                            <h3>Traditional Apartments</h3>
                                                            <p><i class="la la-map-marker"></i>212 5th Ave, New York</p>
                                                        </a>
                                                        <ul>
                                                            <li>3 Bathrooms</li>
                                                            <li>2 Beds</li>
                                                            <li>Area 555 Sq Ft</li>
                                                        </ul>
                                                    </div>
                                                    <div class="card-footer">
                                                        <div class="crd-links">
                                                            <a href="#" class="pull-left">
                                                                <i class="la la-heart-o"></i>
                                                            </a>
                                                            <a href="#" class="plf">
                                                                <i class="la la-calendar-check-o"></i> 25 Days Ago
                                                            </a>
                                                        </div><!--crd-links end-->
                                                        <a href="24_Property_Single.html" title="" class="btn-default">View Details</a>
                                                    </div>
                                                </div><!--card_bod_full end-->
                                                <a href="24_Property_Single.html" title="" class="ext-link"></a>
                                            </div><!--card end-->
                                        </div><!-- list-products end-->
                                    </div>
                                </div><!--tab-content end-->
                                <nav aria-label="Page navigation example">
                                    <ul class="pagination">
                                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item">
                                            <a class="page-link" href="#" aria-label="Next">
                                                <span aria-hidden="true"><i class="la la-arrow-right"></i></span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </li>
                                    </ul>
                                </nav><!--pagination end-->
                            </div><!--listing-directs end-->
                        </div>
                        <div class="col-lg-4">
                            <div class="sidebar layout2">
                                <div class="widget widget-property-search">
                                    <h3 class="widget-title">Recherche de propriété</h3>
                                    <form action="#" class="row banner-search">
                                        <!-- <div class="form_field">
                                            <input type="text" class="form-control" placeholder="Enter Address, City or State">
                                        </div> -->
                                        <div class="form_field">
                                            <div class="form-group">
                                                <div class="drop-menu">
                                                    <div class="select">
                                                        <span>LOUER</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form_field">
                                            <div class="form-group">
                                                <div class="drop-menu">
                                                    <div class="select">
                                                        <span>VOUS-RECHERCHEZ ?</span>
                                                    </div>
                                                    <input type="hidden" name="gender">
                                                    <ul class="dropeddown">
                                                        <li>300$</li>
                                                        <li>400$</li>
                                                        <li>500$</li>
                                                        <li>200$</li>
                                                        <li>600$</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form_field">
                                            <div class="form-group">
                                                <div class="drop-menu">
                                                    <div class="select">
                                                        <span>LOCALITÉS</span>
                                                    </div>
                                                    <input type="hidden" name="gender">
                                                    <ul class="dropeddown">
                                                        <li>2000</li>
                                                        <li>3000</li>
                                                        <li>4000</li>
                                                        <li>5000</li>
                                                        <li>6000</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form_field">
                                            <div class="form-group">
                                                <div class="drop-menu">
                                                    <div class="select">
                                                        <span>BUDGET-MAX ?</span>
                                                    </div>
                                                    <input type="hidden" name="gender">
                                                    <ul class="dropeddown">
                                                        <li>1</li>
                                                        <li>2</li>
                                                        <li>3</li>
                                                        <li>4</li>
                                                        <li>5</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form_field">
                                            <h4>More Features</h4>
                                            <ul>
                                                <li class="input-field">
                                                    <input type="checkbox" name="cc" id="c1">
                                                    <label for="c1">
                                                        <span></span>
                                                        <small>Parking</small>
                                                    </label>
                                                </li>
                                                <li class="input-field">
                                                    <input type="checkbox" name="cc" id="c2">
                                                    <label for="c2">
                                                        <span></span>
                                                        <small>Air Conditioner</small>
                                                    </label>
                                                </li>
                                                <li class="input-field">
                                                    <input type="checkbox" name="cc" id="c3">
                                                    <label for="c3">
                                                        <span></span>
                                                        <small>Washing Machine</small>
                                                    </label>
                                                </li>
                                                <li class="input-field">
                                                    <input type="checkbox" name="cc" id="c4">
                                                    <label for="c4">
                                                        <span></span>
                                                        <small>Heating</small>
                                                    </label>
                                                </li>
                                            </ul>
                                            <ul>
                                                <li class="input-field">
                                                    <input type="checkbox" name="cc" id="c5">
                                                    <label for="c5">
                                                        <span></span>
                                                        <small>Gym</small>
                                                    </label>
                                                </li>
                                                <li class="input-field">
                                                    <input type="checkbox" name="cc" id="c6">
                                                    <label for="c6">
                                                        <span></span>
                                                        <small>Wireless Internet</small>
                                                    </label>
                                                </li>
                                                <li class="input-field">
                                                    <input type="checkbox" name="cc" id="c7">
                                                    <label for="c7">
                                                        <span></span>
                                                        <small>Elevator</small>
                                                    </label>
                                                </li>
                                                <li class="input-field">
                                                    <input type="checkbox" name="cc" id="c8">
                                                    <label for="c8">
                                                        <span></span>
                                                        <small>Swimming Pool</small>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="form_field">
                                            <a href="#" class="btn btn-outline-primary ">
                                                <span>Search</span>
                                            </a>
                                        </div>
                                    </form>
                                </div><!--widget-property-search end-->
                                <div class="widget widget-featured-property">
                                    <h3 class="widget-title">Featured Property</h3>
                                    <div class="card">
                                        <a href="24_Property_Single.html" title="">
                                            <div class="img-block">
                                                <div class="overlay"></div>
                                                <img src="https://via.placeholder.com/370x295" alt="" class="img-fluid">
                                                <div class="rate-info">
                                                    <h5>$550.000</h5>
                                                    <span>For Rent</span>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="card-body">
                                            <a href="24_Property_Single.html" title="">
                                                <h3>Traditional Apartments</h3>
                                                <p><i class="la la-map-marker"></i>212 5th Ave, New York</p>
                                            </a>
                                        </div>
                                    </div>
                                </div><!--widget-featured-property end-->
                                <div class="widget widget-catgs">
                                    <h3 class="widget-title">Categories</h3>
                                    <ul>
                                        <li>
                                            <a href="#" title=""><i class="la la-angle-right"></i><span>House</span></a>
                                            <span>7</span>
                                        </li>
                                        <li>
                                            <a href="#" title=""><i class="la la-angle-right"></i><span>Condo</span></a>
                                            <span>15</span>
                                        </li>
                                        <li>
                                            <a href="#" title=""><i class="la la-angle-right"></i><span>Townhouse</span></a>
                                            <span>4</span>
                                        </li>
                                        <li>
                                            <a href="#" title=""><i class="la la-angle-right"></i><span>Coop</span></a>
                                            <span>1</span>
                                        </li>
                                    </ul>
                                </div><!--widget-catgs end-->
                                <div class="widget widget-posts">
                                    <h3 class="widget-title">Popular Posts</h3>
                                    <ul>
                                        <li>
                                            <div class="wd-posts">
                                                <div class="ps-img">
                                                    <a href="14_Blog_Open.html" title="">
                                                        <img src="https://via.placeholder.com/112x89" alt="">
                                                    </a>
                                                </div><!--ps-img end-->
                                                <div class="ps-info">
                                                    <h3><a href="14_Blog_Open.html" title="">Traditional Apartments</a></h3>
                                                    <strong>$125.700</strong>
                                                    <span><i class="la la-map-marker"></i>212 5th Ave, New York</span>
                                                </div><!--ps-info end-->
                                            </div><!--wd-posts end-->
                                        </li>
                                        <li>
                                            <div class="wd-posts">
                                                <div class="ps-img">
                                                    <a href="14_Blog_Open.html" title="">
                                                        <img src="https://via.placeholder.com/112x89" alt="">
                                                    </a>
                                                </div><!--ps-img end-->
                                                <div class="ps-info">
                                                    <h3><a href="14_Blog_Open.html" title="">Luxury Home</a></h3>
                                                    <strong>$125.700</strong>
                                                    <span><i class="la la-map-marker"></i>212 5th Ave, New York</span>
                                                </div><!--ps-info end-->
                                            </div><!--wd-posts end-->
                                        </li>
                                        <li>
                                            <div class="wd-posts">
                                                <div class="ps-img">
                                                    <a href="14_Blog_Open.html" title="">
                                                        <img src="https://via.placeholder.com/112x89" alt="">
                                                    </a>
                                                </div><!--ps-img end-->
                                                <div class="ps-info">
                                                    <h3><a href="14_Blog_Open.html" title="">Real Estate Industry</a></h3>
                                                    <strong>$125.700</strong>
                                                    <span><i class="la la-map-marker"></i>212 5th Ave, New York</span>
                                                </div><!--ps-info end-->
                                            </div><!--wd-posts end-->
                                        </li>
                                    </ul>
                                </div><!--widget-posts end-->
                            </div><!--sidebar end-->
                        </div>
                    </div>
                </div><!--listing-main-sec-details end-->
            </div>    
        </section><!--listing-main-sec end-->

