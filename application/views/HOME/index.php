    <style type="text/css">
        .custom{
            cursor: pointer;
            display: block;
            padding: 12px;
            color: #7e7f82;
            font-weight: 500;
            padding-left: 20px;
            font-size: 14px;
            text-transform: capitalize;

            height: 3.1rem;
            border-color: lightgrey;
            /*border-radius: 10px;*/
            line-height: 48px;
        }

        .hauteur{
            /*height: 350px;*/
            height: 21rem;
        }
        .dropdown {
            color: white;
            font-size: 14px;
            font-family: 'poppins';

        }
        .dropdown-content {
          display: none;
          position: absolute;
          min-width: 160px;
          box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
          z-index: 1;
        }

        .dropdown:hover .dropdown-content {
          display: block;
        }
    </style>
    <div class="wrapper">
        <header class="pb">

            <div class="header">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <nav class="navbar navbar-expand-lg navbar-light">
                                <a class="navbar-brand" href="<?php echo site_url(array('Home','index')); ?>">
                                    <img  src="<?= img_url('logo.png');?>" alt="VYMMO"> 

                                </a>
                                <button class="menu-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent">
                                    <span class="icon-spar"></span>
                                    <span class="icon-spar"></span>
                                    <span class="icon-spar"></span>
                                </button>

                                <div class="navbar-collapse" id="navbarSupportedContent">
                                    <ul class="navbar-nav mr-auto">
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo site_url(array('Home','echanger')); ?>" data-toggle="">
                                                ECHANGER
                                            </a>
                                        
                                        </li>
                                        <li class="nav-item ">
                                            <a class="nav-link" href="<?php echo site_url(array('Home','acheter')); ?>" data-toggle="">
                                               ACHETER
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo site_url(array('Home','louer')); ?>" data-toggle="">
                                                LOUER
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="<?php echo site_url(array('Home','service')); ?>" data-toggle="">
                                                SERVICES
                                            </a>
                                          
                                        </li>
                                    </ul>
                                    <div class="d-inline my-2 my-lg-0">
                                        <ul class="navbar-nav">
                                            <li class="nav-item signin-btn dropdown">
                                                <?php if (isset($_SESSION['user']['identity'])) { ?>
                                                <div class="nav-link dropbtn">
                                                    <span>MON ESPACE
                                                      <i class="fa fa-caret-down"></i>
                                                    </span>
                                                    <div class="dropdown-content">
                                                        <a href="<?php echo site_url(array('Admin','index')); ?>" class="nav-link">
                                                            <i class="la la-sign-in"></i>
                                                            <span>
                                                                <b>Tableau de Bord</b>
                                                            </span>
                                                        </a>
                                                        <a href="<?php echo site_url(array('InscriptionManager','deconnexion')); ?>" class="nav-link">
                                                            <i class="la la-sign-in"></i>
                                                            <span>
                                                                <b>Deconnexion</b>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>

                                                <?php }else{ ?>
                                                <a href="<?php echo site_url(array('HOME','login')); ?>" class="nav-link" id="#liveToastBtn">
                                                    <i class="la la-sign-in"></i>
                                                    <span>
                                                        <b>Connexion</b>
                                                    </span>
                                                </a>
                                                    <?php } ?>

                                            </li>
                                            <?php if (isset($_SESSION['user']['identity'])) { ?>
                                                <li class="nav-item submit-btn">
                                                    <a href="<?php echo site_url(array('Annonces','index')); ?>" class="my-2 my-sm-0 nav-link sbmt-btn" data-toggle="modal" data-target="#choix_du_type">
                                                        <i class="fa fa-plus"></i>
                                                        <span>Ajouter une annonce</span>
                                                    </a>
                                                </li>
                                            <?php }else{ ?>
                                                <li class="nav-item submit-btn" id="liveToastBtn">
                                                    <a href="#" class="my-2 my-sm-0 nav-link sbmt-btn">
                                                        <i class="fa fa-plus"></i>
                                                        <span>Ajouter une annonce</span>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                                
                                        </ul>
                                    </div>
                                    <a href="#" title="" class="close-menu"><i class="la la-close"></i></a>
                                </div>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!--header end-->

        <!-- zone de recherche -->
        <section class="banner hp7">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="banner-content">
                            <h1>Trouver les meilleures propriétés <br/>dans un endroit</h1>
                        </div>
                        <div class="widget-property-search">
                            <form action="<?=site_url(array('Home','rechercher'));?>" class="row banner-search" method="POST">
                                <div class="form_field">
                                    <div class="form-group">
                                        <select class="dropeddown h-100 w-100 form-control custom" name="action" >
                                            <option disabled selected><span>VOUS-VOULEZ ?</span></option>
                                            <option value="A Vendre">Acheter</option>
                                            <option value="A Louer">Louer</option>
                                            <option value="A Echanger">Echanger</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form_field">
                                    <div class="form-group">
                                        <select class="dropeddown h-100 w-100 form-control custom" name="type">
                                            <!-- <option disabled selected><span>VOUS-RECHERCHEZ ?</span></option> -->
                                            <?php for ($i=0; $i < count($listeTypeBien) ; $i++) { ?>
                                                <option value="<?=$listeTypeBien[$i]?>"><?=$listeTypeBien[$i]?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form_field">
                                    <div class="form-group ">
                                        <!-- <select class="dropeddown d-block w-100 custom" id="ville" name="ville" > -->
                                        <input type="text" class="form-control custom" name="ville" placeholder="VILLE - LOCALITE" value="<?=set_value('ville');?>">

                                        <!-- </select> -->
                                    </div>
                                </div>
                                <div class="form_field">
                                    <div class="form-group">
                                        <input type="text" class="form-control custom" name="prix" placeholder="BUDGET MAX [XAF]" value="<?=set_value('prix');?>">
                                    </div>
                                </div>
                                <div class="feat-srch">
                                    <div class="more-feat">
                                        <h3> <i class="la la-cog"></i> Afficher plus de fonctionnalités</h3>
                                    </div><!--more-feat end-->
                                    <div class="form_field">
                                        <!-- <a href="#" > -->
                                            <input class="btn btn-outline-primary" type="submit" value="RECHERCHER">
                                            <!-- <span>RECHERCHER</span> -->
                                        <!-- </a> -->
                                    </div>
                                </div><!--more-feat end-->
                                <div class="features_list">
                                    <ul>
                                        <li class="input-field">
                                            <input type="checkbox" name="bb" id="b1">
                                            <label for="b1">
                                                <span></span>
                                                <small>Parking</small>
                                            </label>
                                        </li>
                                        <li class="input-field">
                                            <input type="checkbox" name="bb" id="b2">
                                            <label for="b2">
                                                <span></span>
                                                <small>Gym</small>
                                            </label>
                                        </li>
                                        <li class="input-field">
                                            <input type="checkbox" name="bb" id="b3">
                                            <label for="b3">
                                                <span></span>
                                                <small>Machine à laver</small>
                                            </label>
                                        </li>
                                        <li class="input-field">
                                            <input type="checkbox" name="bb" id="b4">
                                            <label for="b4">
                                                <span></span>
                                                <small>Ascenseur</small>
                                            </label>
                                        </li>
                                        <li class="input-field">
                                            <input type="checkbox" name="bb" id="b5">
                                            <label for="b5">
                                                <span></span>
                                                <small>Espace de rangement</small>
                                            </label>
                                        </li>
                                        <li class="input-field">
                                            <input type="checkbox" name="bb" id="b6">
                                            <label for="b6">
                                                <span></span>
                                                <small>Climatiseur</small>
                                            </label>
                                        </li>
                                        <li class="input-field">
                                            <input type="checkbox" name="bb" id="b7">
                                            <label for="b7">
                                                <span></span>
                                                <small>Internet sans fil</small>
                                            </label>
                                        </li>
                                        <li class="input-field">
                                            <input type="checkbox" name="bb" id="b8">
                                            <label for="b8">
                                                <span></span>
                                                <small>Chauffage</small>
                                            </label>
                                        </li>
                                        <li class="input-field">
                                            <input type="checkbox" name="bb" id="b9">
                                            <label for="b9">
                                                <span></span>
                                                <small>Piscine</small>
                                            </label>
                                        </li>
                                        <li class="input-field">
                                            <input type="checkbox" name="bb" id="b10">
                                            <label for="b10">
                                                <span></span>
                                                <small>Cinéma maison</small>
                                            </label>
                                        </li>
                                    </ul>
                                </div><!--features_list end-->
                            </form><!--baner-form end-->
                        </div><!--widget-property-search end-->
                    </div>
                </div>
            </div>
        </section>

      
        <!-- zone des resultats de recherche -->
        <?php
            if (isset($resultatrecherche)) {
                if (!empty($resultatrecherche)) {
        ?>
        <section class="popular-listing hp2">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6">
                        <div class="section-heading">
                            <h3>Vos Résultats de recherche sont prêts !</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php for($i=0; $i< $resultatrecherche['total']; $i++) { ?>
                            <div class="col-lg-4 col-md-6">
                                <div class="card">
                                    <a href="24_Property_Single.html" title="">
                                        <div class="img-block">
                                            <div class="overlay"></div>
                                            <!-- <img src="https://via.placeholder.com/370x295" alt="" class="img-fluid"> -->
                                            <img src="<?=img_url('user_bien/'.$resultatrecherche[$i]['image']);?>" alt="" class="img-fluid hauteur">
                                            <div class="rate-info">
                                                <h5><?= $resultatrecherche[$i]['prix']?> XAF</h5>
                                                <span><?= $resultatrecherche[$i]['etat']?></span>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="card-body">
                                        <a href="24_Property_Single.html" title="">
                                            <h3><?= $resultatrecherche[$i]['type']?></h3>
                                            <p><i class="la la-map-marker"></i><?= $resultatrecherche[$i]['ville']?></p>
                                        </a>
                                        <ul>
                                            <li>nombre de pièces  : <?= $resultatrecherche[$i]['nbrepiece']?>  </li>
                                            <li>Superficie : <?= $resultatrecherche[$i]['superficie']?>  m<sup>2</sup></li>
                                        </ul>
                                    </div>
                                    <div class="card-footer">
                                        <a href="#" class="pull-left">
                                            <i class="la la-heart-o"></i>
                                        </a>
                                        <a href="#" class="pull-right">
                                            <i class="la la-calendar-check-o"></i> publié le : <?php 
                                                    $a = explode(' ', $resultatrecherche[$i]['date']);
                                                    $jour = $a[0] ;
                                                    echo date('d-m-Y', strtotime($jour)); ?>
                                        </a>
                                    </div>
                                    <a href="24_Property_Single.html" title="" class="ext-link"></a>
                                </div>
                            </div>
                    <?php } ?>
                </div>
            </div>
        </section>
        <?php 
                }else{ ?>
                    <section class="popular-listing hp2 pb-0">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-xl-6">
                                    <div class="section-heading">
                                        <h3 style="color: crimson;">Aucun Résultat trouvé pour vos recherches!</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section> 
        <?php   }
            }
        ?>

        <!-- zone de des dernieres annonces -->
        <section class="popular-listing hp2">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-xl-6">
                        <div class="section-heading">
                            <span>Découvrir</span>
                            <h3>Dernières Annonces </h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php
                        if (isset($recherchepopulaire)) {
                            if (!empty($recherchepopulaire)) {
                                for($i=0; $i< $recherchepopulaire['total']; $i++) { 
                    ?>
                                    <div class="col-lg-4 col-md-6">
                                        <div class="card">
                                            <a href="24_Property_Single.html" title="">
                                                <div class="img-block">
                                                    <div class="overlay"></div>
                                                    <img src="<?=img_url('user_bien/'.$recherchepopulaire[$i]['image']);?>" alt="" class="img-fluid hauteur">
                                                    <div class="rate-info">
                                                        <h5><?= $recherchepopulaire[$i]['prix']?> XAF</h5>
                                                        <span><?= $recherchepopulaire[$i]['etat']?></span>
                                                    </div>
                                                </div>
                                            </a>
                                            <div class="card-body">
                                                <a href="24_Property_Single.html" title="">
                                                    <h3><?= $recherchepopulaire[$i]['type']?></h3>
                                                    <p><i class="la la-map-marker"></i><?= $recherchepopulaire[$i]['ville']?></p>
                                                </a>
                                                <ul>
                                                    <li>nombre de pièces  : <?= $recherchepopulaire[$i]['nbrepiece']?>  </li>
                                                    <li>Superficie : <?= $recherchepopulaire[$i]['superficie']?>  m<sup>2</sup></li>
                                                </ul>
                                            </div>
                                            <div class="card-footer">
                                                <a href="#" class="pull-left">
                                                    <i class="la la-heart-o"></i>
                                                </a>
                                                <a href="#" class="pull-right">
                                                    <i class="la la-calendar-check-o"></i> publié le : <?php 
                                                    $a = explode(' ', $recherchepopulaire[$i]['date']);
                                                    $jour = $a[0] ;
                                                    $heure = $a[1];
                                                    echo date('d-m-Y', strtotime($jour)).' à '.$heure; ?>
                                                </a>
                                            </div>
                                            <a href="24_Property_Single.html" title="" class="ext-link"></a>
                                        </div>
                                    </div>
                    <?php 
                                }
                            }
                        }
                    ?>
                </div>
            </div>
        </section>

    <?php if (isset($_SESSION['modalShow'])) { ?>
    <!-- modal d'abonnement requis -->
    <!-- Button trigger modal -->
    <button type="button" id="modalShower" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop" style="display:none;">
    </button>

    <!-- Modal -->
    <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="staticBackdropLabel"><i class="fa fa-warning"></i>&nbsp; &nbsp; Oups! Abonnement Expiré</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p><?=$_SESSION['modalShow'];?></p>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">        
        $(document).ready(function(){
            $('#modalShower')[0].click();
            console.log($('#modalShower')[0]);
        })    
    </script>
    <?php } ?>

    <!-- Modal de selection de type  -->
    <div class="modal fade" id="choix_du_type" tabindex="-1" role="dialog" aria-labelledby="Type" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Quel type d'annonce voulez-vous déposer ?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="form-group row">
                        <label for="colFormLabelSm" class="col-md-2 col-form-label col-form-label-md" style="font-size: 1.5rem; margin-top: -1%;">Type:</label>
                        <div class="col-md-10">
                            <select class="form-control form-control-lg h-100" id="type" name="type">
                                <option disabled selected value="">Choisir</option>
                                <?php if (isset($_SESSION['listeTypeannonce'])) {
                                    for ($i=0; $i < $_SESSION['listeTypeannonce']['total'] ; $i++) { 
                                        echo '<option value="'.$_SESSION['listeTypeannonce'][$i]['nom'].'">'.$_SESSION['listeTypeannonce'][$i]['nom'].'</option>';
                                    }
                                }else{
                                        // $liste = $this->Annonces->listeTypeannonce();
                                    $liste = array('listeTypeannonce'=>$this->Typeannonce->recuperer());
                                    $_SESSION = array_merge($_SESSION, $liste);

                                    for ($i=0; $i < $_SESSION['listeTypeannonce']['total'] ; $i++) { 
                                        echo '<option value="'.$_SESSION['listeTypeannonce'][$i]['nom'].'">'.$_SESSION['listeTypeannonce'][$i]['nom'].'</option>';
                                    }

                                    ?>
                                <?php }?>   
                            </select>
                        </div>
                        <!-- <div class="col-md-10">
                            <label for="date">Disponible à partir du: </label>
                            <input type="date" name="date" id="date" class="form-control">
                        </div> -->
                    </form>
                    <p>Votre bien est disponible dès le</p>
                    <form class="form-group row">
                        <label for="colFormLabelSm" class="col-md-2 col-form-label col-form-label-md" style="font-size: 1.5rem; margin-top: -1%;">Date:</label>
                        <div class="col-md-10">
                            <input type="date" name="date" id="date" class="form-control h-100" min="<?=date('Y-n-d');?>">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <div class="my-2 my-sm-0 nav-link sbmt-btn" data-dismiss="modal">Annuler</div>
                    <a href="<?php echo site_url(array('Annonces','index')); ?>" id="valider" class="my-2 my-sm-0 nav-link sbmt-btn" style="display: none;">Valider</a>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        
        $(document).ready(function(){
            $('#date').change(function(){
                console.log('ici');
                var val = $('#type').val();
                var dat = $('#date').val();
                var valider = document.getElementById('valider').removeAttribute('style');
                    // valider
                    // console.log(valider);
                var jqxhr = $.ajax({
                    method: "POST",
                    url: "recupType",
                    data: { type: val, date: dat },
                    dataType : 'json'
                })
                  .done(function(data) {
                    console.log('success: ');
                    console.log(data);
                  })
                  .fail(function(data) {
                    console.log(data);
                  })
                  .always(function() {
                    // alert( "complete" );
                  });

            });

        })    
    </script>

    <!-- Toast de demande de connection -->
    <div class="position-fixed top-0 right-0 p-3" style="z-index: 5; left: 45rem; top: 7rem;">
        <div id="liveToast" class="toast hide" role="alert" aria-live="assertive" aria-atomic="true" data-delay="2500">
            <div class="toast-header" style="background-color: hsl(25, 65%, 84%);">
                <i class="fa fa-warning"></i>&nbsp; &nbsp;
                <strong class="mr-auto">Attention ! SVP</strong>

                <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="toast-body">
                Veuillez, d'abord vous Connecter.
            </div>
        </div>
    </div>

    <!-- Toast Confirmation d'inscription -->
    <?php if (isset($_SESSION['registered'])) { ?>
        <div class="position-fixed top-0 right-0 p-3" style="z-index: 5; right: 0; top: 7rem;">
            <div id="registerToast" class="toast hide" role="alert" aria-live="assertive" aria-atomic="true" data-delay="3500">
                <div class="toast-header bg-light" style="color: green;">
                    <i class="fa fa-check"></i>&nbsp; &nbsp;
                    <strong class="mr-auto"><?= $_SESSION['registered'];?></strong>

                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $('#registerToast').toast('show');
        </script>
    <?php } ?>
    <script type="text/javascript">
      $('#liveToastBtn').on('click', function(){
         $('#liveToast').toast('show');    
     });
    </script>

    <!-- Toast d'erreurs -->
    <?php if (isset($_SESSION['toastError'])) { ?>
        <div class="position-fixed top-0 right-0 p-3" style="z-index: 5; right: 0; top: 7rem;">
            <div id="registerToast" class="toast hide" role="alert" aria-live="assertive" aria-atomic="true" data-delay="3500">
                <div class="toast-header bg-error" style="color: green;">
                    <i class="fa fa-error"></i>&nbsp; &nbsp;
                    <strong class="mr-auto"><?= $_SESSION['registered'];?></strong>

                    <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $('#registerToast').toast('show');
        </script>
    <?php } ?>
    <script type="text/javascript">
      $('#liveToastBtn').on('click', function(){
         $('#liveToast').toast('show');    
     });
    </script>

    <script type="text/javascript">
        var sms='';
        // DATAVILLE.push('Bamena');
        for  (var i=0; i < DATAVILLE[0].tot; i++) {
            sms+='<option class="select2" value="'+DATAVILLE[0][i].ville+'">'+DATAVILLE[0][i].ville;
            sms+='</option>';
        }
        $("#ville").append($(sms));
    </script>


    <?php unset($_SESSION['registered']); ?>
