<?= 
	css('multiform');
	print_r($_SESSION);
	// if (isset($_SESSION['user']['location'])) {
	if (isset($_SESSION['user'])) {
		redirect(site_url(array("Welcome","index")));
	}
?>

<main>

	<div class="stepper">
	    <div class="step--1 step-active">Identification</div>
	    <div class="step--2">Contacts</div>
	    <div class="step--3">Localisation</div>
	    <!-- <div class="step--4">Finish</div> -->
	</div>

	<form class="form form-active" method="POST" action="<?= site_url(array('Inscription','add_datauser')) ?>">
	    <div class="form--header-container">
	      <h1 class="form--header-title" id="user-info">
	        Infos Personnelles
	      </h1>

	      <?php if (isset($_SESSION['error'])) {
		     echo '
	        	 <p class="form--header-text" style="color: red;">'.$_SESSION['error'].'</p>
		     	 ';
	      }else{
	      	 echo '
	        	 <p class="form--header-text"> Tell us more about you. </p>
		     	 ';
	      }?>
	    </div>
	    	<input type="hidden" id="form1indicator" value="<?php if(isset($_SESSION['status'])){echo $_SESSION['status'];}else{echo "normal";}  ?>">
	    	<p>
				<!-- <label for="nom">Nom:</label> -->
				<input id="nom" type="text" name="nom" placeholder="Entrer votre nom" required="true" value="<?= set_value("nom"); ?>">
			</p> 
			<p>
				<!-- <label for="email">Email:</label> -->
				<input id="email" type="email" name="email" placeholder="Entrer votre adresse email" required="true" value="<?= set_value("email"); ?>">
			</p>
 			<p>
				<!-- <label for="password">Mot de passe:</label> -->
				<input id="password" type="password" name="password" placeholder="Entrer votre mot de passe" required="true" value="<?= set_value("password"); ?>">
			</p>
			<p>
				<!-- <label for="password_confirm">Confirmation:</label> -->
				<input id="password_confirm" type="password" name="password_confirm" placeholder="Confirmer votre mot de passe" required="true" value="<?= set_value("password_confirm"); ?>">
			</p>
	    <button class="form__btn" id="btn-1">Next</button>
	    <!-- <input type="submit" class="form__btn" id="btn-1" value="Next"> -->
	</form>

	<form class="form" method="POST" action="<?= site_url(array('Inscription','add_datacontact')) ?>">
	    <div class="form--header-container">
	      <h1 class="form--header-title" id="contact-info">
	        Contact 
	      </h1>

	      <?php if (isset($_SESSION['error'])) {
		     echo '
	        	 <p class="form--header-text" style="color: red;">'.$_SESSION['error'].'</p>
		     	 ';
	      }else{
	      	 echo '
	        	 <p class="form--header-text"> Give us your contact. </p>
		     	 ';
	      }?>
	      
	    </div>

    	<p>
			<!-- <label for="telephone1">Telephone 1:</label> -->
			<input id="telephone1" type="text" name="tel1" placeholder="Entrer votre telephone1" required="" maxlength="22" value="<?= set_value("tel1"); ?>">
		</p>
		<p>
			<!-- <label for="telephone2">Telephone 2:</label> -->
			<input id="telephone2" type="text" name="tel2" placeholder="Entrer votre telephone2" maxlength="22" value="<?= set_value("tel2"); ?>">
		</p>
		<p>
			<!-- <label for="whatsapp">Whatsapp:</label> -->
			<input id="whatsapp" type="text" name="whatsapp" placeholder="Entrer votre contact whatsapp" required="" maxlength="22" value="<?= set_value("whatsapp"); ?>">
		</p>
		<p>
			<!-- <label for="facebook">Facebook:</label> -->
			<input id="facebook" type="text" name="facebook" placeholder="Entrer votre contact facebook" maxlength="255" value="<?= set_value("facebook"); ?>">
		</p>
		<p>
			<!-- <label for="twitter">Twitter:</label> -->
			<input id="twitter" type="text" name="twitter" placeholder="Entrer votre contact twitter" maxlength="255" value="<?= set_value("twitter"); ?>">
		</p>
		<p>
			<!-- <label for="instagram">Instagram:</label> -->
			<input id="instagram" type="text" name="instagram" placeholder="Entrer votre contact instagram" maxlength="255" value="<?= set_value("instagram"); ?>">
		</p>
		<p>
			<!-- <label for="linkedin">Linkedin:</label> -->
			<input id="linkedin" type="text" name="linkedin" placeholder="Entrer votre contact linkedin" maxlength="255" value="<?= set_value("linkedin"); ?>">
		</p>
		<p>
			<!-- <label for="telegram">Telegram:</label> -->
			<input id="telegram" type="text" name="telegram" placeholder="Entrer votre contact telegram" maxlength="255" value="<?= set_value("telegram"); ?>">
		</p> 
	    <button class="form__btn" id="btn-2-prev">Previous</button>
	    <!-- <button class="form__btn" id="btn-2-next">Next</button> -->
	    <input type="submit" class="form__btn" id="btn-2-next" value="Next">
	</form>

	<form class="form" method="POST" action="<?= site_url(array('Inscription','add_datalocation')) ?>">
	    <div class="form--header-container">
	      <h1 class="form--header-title" id="localisation-info">
	        Infos de Localisation
	      </h1>

	      <?php if (isset($_SESSION['error'])) {
		     echo '
	        	 <p class="form--header-text" style="color: red;">'.$_SESSION['error'].'</p>
		     	 ';
	      }else{
	      	 echo '
	        	 <p class="form--header-text"> Tell us more about your location. </p>
		     	 ';
	      }?>
	      
	    </div>
    	<p>
			<!-- <label for="pays">pays:</label> -->
			<input id="pays" type="text" name="pays" placeholder="Entrer votre nom de pays" required="true" value="<?= set_value("pays"); ?>">
		</p>
		<p>
			<!-- <label for="ville">ville:</label> -->
			<input id="ville" type="text" name="ville" placeholder="Entrer votre nom de ville" required="true" value="<?= set_value("ville"); ?>">
		</p>
		<p>
			<!-- <label for="adresse">adresse:</label> -->
			<input id="adresse" type="text" name="adresse" placeholder="Adresse: 567 rue joffre Akwa" required="true" value="<?= set_value("adresse"); ?>">
		</p>
		<p>
			<!-- <label for="longitude">longitude:</label> -->
			<input id="longitude" type="text" name="longitude" placeholder="Entrer votre longitude" value="<?= set_value("longitude"); ?>">
		</p>
		<p>
			<!-- <label for="latitude">latitude:</label> -->
			<input id="latitude" type="text" name="latitude" placeholder="Entrer votre latitude" value="<?= set_value("latitude"); ?>">
		</p>
	    <button class="form__btn" id="btn-3-prev">Previous</button>
	    <!-- <button class="form__btn" id="btn-3">Submit</button> -->
	    <input type="submit" class="form__btn" id="btn-3" value="Valider">
	</form>

	<div class="form--message"></div>

</main>

<?= js('jQuery-3.6.0.min'); ?>
<?//= js('js2'); ?>
<?//= js('multiform'); ?>
<?= js('inscription'); ?>
