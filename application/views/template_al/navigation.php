  
  
<aside class="main-sidebar">
  <section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li><a href="<?php echo site_url(array('Administration','index')); ?>"><i class="fa fa-circle-o text-red"></i> <span> Home</span></a></li>
      
      <li class="treeview  menu-open">
        <a href="#">
          <i class="fa fa-users"></i>
          <span>Annuaire</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="<?php echo site_url(array('Administration','AddUser')); ?>"><i class="ion-android-add-circle"></i>  Ajouter un User</a></li>
          <li><a href="<?php echo site_url(array('Administration','ListeUser')); ?>"><i class="fa fa-star"></i>Liste des Users</a></li>
        </ul>
      </li>
    
    </ul>
  </section>
</aside>

  
