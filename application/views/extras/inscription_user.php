<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="Kevin TONBONG & FADHIL Abouba for Digital Zangalewa" />
        <title>Gest-immo</title>
        <!-- Favicon-->
        <!-- <link rel="icon" type="image/x-icon" href="logo_icon.png" /> -->
        <link rel="shortcut icon" type="image/x-icon.png" href="<?= base_url()?>logo-icon.png" />
        <!-- Bootstrap icons-->
        <!-- <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" type="text/css" /> -->
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <!-- <link href="css/styles.css" rel="stylesheet" /> -->
    </head>
	
	<body>
		<ul class="select2-selection__rendered">
            <li class="select2-selection__choice" title="alaska" data-select2-id="103">
                <span class="select2-selection__choice__remove" role="presentation">×</span>
                alaska
            </li>
            <li class="select2-selection__choice" title="california" data-select2-id="104">
                <span class="select2-selection__choice__remove" role="presentation">×</span>
                california
            </li>
            <li class="select2-selection__choice" title="delaware" data-select2-id="105">
                <span class="select2-selection__choice__remove" role="presentation">×</span>
                delaware
            </li>
            <li class="select2-selection__choice" title="tennessee" data-select2-id="106">
                <span class="select2-selection__choice__remove" role="presentation">×</span>
                tennessee
            </li>
            <li class="select2-search select2-search--inline">
                <input class="select2-search__field" type="search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="searchbox" aria-autocomplete="list" placeholder="" style="width: 0.75em;">
            </li>
        </ul>
	</body>
</html>