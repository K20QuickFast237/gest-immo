<?php 
  // Actualisation de l'etat_abonnement
  // if (  $_SESSION['user']['nombre_publication'] < 3) {
  //   // code...
  // }
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <style type="text/css">
    div{
      /*background-color: red;*/
    }
  </style>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>Gest-immo</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?= base_url().'assets/plugins'; ?>/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url().'assets/dist'; ?>/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<!-- <body class="hold-transition layout-top-nav dark-mode"> -->
<body class="hold-transition layout-top-nav">
  <!-- Pour la nav bar -->
    <!-- wrapper -->
    <div class="wrapper">

          <!-- Navbar -->
          <nav class="main-header navbar navbar-expand-md navbar-light navbar-muted">
            <div class="container">
              <a href="<?= site_url(array("Welcome","index"));?>" class="navbar-brand">
                <img src="<?= base_url(); ?>/assets/images/logo_icon3.png" alt="Gest-immo Logo" class="brand-image img-circle elevation-3"
                     style="opacity: .8; ">
                <span class="brand-text font-weight-light">VYMMO</span>
              </a>
              
              <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>

              <div class="collapse navbar-collapse order-3 d-flex justify-content-between" id="navbarCollapse">
                <!-- Left navbar links -->
                <ul class="navbar-nav flex-item">
                  <li class="nav-item">
                    <a href="#" class="nav-link">Echanger</a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">Acheter</a>
                  </li>
                  <li class="nav-item">
                    <a href="#" class="nav-link">Louer</a>
                  </li>
                  <li class="nav-item dropdown">
                    <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Services</a>
                    <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
                      <li><a href="#" class="dropdown-item">Contacter un Huissier </a></li>
                      <li><a href="#" class="dropdown-item">Contacter un Demenageur</a></li>
                    </ul>
                  </li>
                </ul> 
                <ul class="navbar-nav flex-item">
                  <li class="nav-item p-2">
                    <a href="#" class="nav-link d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm text-white" style="opacity: .6;">
                      <i class="fas fa-plus fa-sm text-white"> </i> Ajouter une Annonce
                    </a>
                  </li>
                  <li class="nav-item p-2 dropdown">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" id="dropdown-profil" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="mr-2 d-none d-lg-inline text-gray-600"><?php if(isset($_SESSION['user']['identity']['nom'])){ echo$_SESSION['user']['identity']['nom'];}else{echo "";}?></span>
                      <?php if(isset($_SESSION['user']['profil']['image'])){echo '<img class="img-circle brand-image" src="<?= base_url();?>assets/images/'.$_SESSION['user']['profil']['image'];} ?>
                    </a>
                    <ul aria-labelledby="dropdown-profil" class="dropdown-menu border-0 shadow">
                      <li><a href="#" class="dropdown-item">Connexion</a></li>
                      <li><a href="<?= site_url((array('Inscription','inscrire'))) ?>" class="dropdown-item">Inscription</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
          <!-- /.navbar -->

      <!-- </div> -->
      <div class="content-wrapper">
          <!-- caroussel -->
            <div class="row">
              <div class="col" style="height: 20rem;">             
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img class="d-block" src="<?= img_url('Vymmo_1.gif');?>" alt="First slide">
                  </div>
                </div>
              </div>
            </div>
          <!-- /.caroussel -->

          <!-- recherche 1 -->
          <div class="row justify-content-around">
            <div class="col-sm-8">
              <!-- <div class="col" style=""> -->
              <section class="content">
                <div class="container">
                  <div class="card card-danger">
                    <!-- card-body -->
                    <form class="form-horizontal">
                      <div class="card-body">
                        <div class="row">
                          <div class="col-3">
                            <!-- select -->
                              <div class="form-group">
                                <label>Que voulez-vous faire?</label>
                                <select class="form-control">
                                  <option>Acheter</option>
                                  <option>Vendre</option>
                                </select>
                              </div>
                            <!-- select -->
                          </div>
                          <div class="col-4">
                            <!-- select -->
                              <div class="form-group">
                                <label>Que recherchez-vous?</label>
                                <select class="form-control">
                                  <option>appartement</option>
                                  <option>Studio</option>
                                  <option>chambre</option>
                                </select>
                              </div>
                            <!-- select -->
                          </div>
                          <div class="col-5">
                            <!-- select -->
                              <div class="form-group">
                                <label>Ville ou localité?</label>
                                <select class="form-control">
                                  <option>localite 1</option>
                                  <option>localite 2</option>
                                  <option>localite 3</option>
                                  <option>localite 4</option>
                                </select>
                              </div>
                            <!-- select -->
                          </div>
                        </div>
                        <div class="row ">
                          <div class="offset-2 col-sm-3">
                            <!-- text input -->
                            <div class="form-group">
                              <label>Quel est votre budget max?</label>
                              <input type="text" class="form-control" placeholder="Enter ...">
                            </div>
                          </div>
                          <div class="offset-1 col-sm-2">
                            <!-- submit button -->
                            <div class="form-group">
                              <label>C'est bon?</label>
                              <button type="submit" class="form-control btn btn-primary">Let's Go</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                    <!-- /.card-body -->
                  </div>
                </div>
              </section>
            </div>
          </div>
          <!-- ./recherche 1 -->

      </div>
    </div>
      <!-- <div class="container-fluid"> -->
          <!-- <div class="row justify-content-center"> -->
            <!-- Main Footer -->
            <footer class="main-footer d-flex justify-content-center">
              <!-- To the right -->
              <div class="float-right d-none d-sm-inline">
                
              </div>
              <!-- Default to the left -->
              <strong>Copyright &copy; 2021-2022 <a href="#">Digital Zangalewa proudly powered by Inchclass</a>.</strong> All rights reserved.
            </footer>
          <!-- </div> -->
      <!-- </div> -->
    <!-- ./wrapper -->

  

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="<?= base_url().'assets/plugins'; ?>/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?= base_url().'assets/plugins'; ?>/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url().'assets/dist'; ?>/js/adminlte.min.js"></script>
</body>
</html>
