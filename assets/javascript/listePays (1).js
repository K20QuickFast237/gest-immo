

	var DATA={ 'total':'5',
		
		
		'0':{
			'nom':'Cameroun',
			'abrev':'CM',
			'ambl':'cameroun.png',
			'drapeau':'Cameroun.png',
			'codTel':'+237'
			},
			
		'1':{
			'nom':'Gabon',
			'abrev':'GB',
			'ambl':'gabon.png',
			'drapeau':'Gabon.png',
			'codTel':'+241'
			},
		'2':{
			'nom':'Madagascar',
			'abrev':'MA',
			'ambl':'madagascar.png',
			'drapeau':'Madagascar.png',
			'codTel':'+261'
			},
		'3':{
			'nom':'Nigeria',
			'abrev':'NI',
			'ambl':'nigeria.png',
			'drapeau':'Nigeria.png',
			'codTel':'+234'
			},
		'4':{
			'nom':'Tchad',
			'abrev':'Tc',
			'ambl':'Tchad.png',
			'drapeau':'tchad.png',
			'codTel':'+235'
			}

	};


	var DATAVILLE={
			'tot':'4',
			
			//ville du cameroun


			'0' : {
				'tot' : '74',
				
				
				'0':{
					'ville':'Abong-Mbang'
				},
				
				'1':{
					'ville':'Akonolinga'
				},

				'2':{
					'ville':'Bafang'
				},
				
				'3':{
					'ville':'Bafia'
				},

				'4':{
					'ville':'Bali'
				},
				
				'5':{
					'ville':'Bamenda'
				},
				
				'6':{
					'ville':'Bangangte'
				},

				'7':{
					'ville':'Batibo'
				},

				'8':{
					'ville':'Blangoua'
				},				

				'9':{
					'ville':'Buea'	
				},

				'10':{
					'ville':'Banyo'
				},

				'11':{
					'ville':'Bogo'
				},

				'12':{
					'ville':'Bélabo'
				},

				'13':{
					'ville':'Bertoua'
				},
				
				'14':{
					'ville':'Dschang'
				},

				'15':{
					'ville':'Douala'
				},

				'16':{
					'ville':'Edéa'
				},
				
				'17':{
					'ville':'Ebolowa'
				},

				'18':{
					'ville':'Eséka'
				},

				'19':{
					'ville':'Figuil'
				},

				'20':{
					'ville':'Fontem'
				},

				'21':{
					'ville':'Foumbot'
				},

				'22':{
					'ville':'Foumban'
				},
				
				'23':{
					'ville':'Garoua'
				},

				'24':{
					'ville':'Garoua-Boulaï'
				},

				'25':{
					'ville':'Gazawa'
				},

				'26':{
					'ville':'Guider'
				},

				'27':{
					'ville':'Guidiguis'
				},

				'28':{
					'ville':'Kékem'
				},

				'29':{
					'ville':'Koutaba'
				},

				'30':{
					'ville':'Kousséri'
				},

				'31':{
					'ville':'Kumba'
				},

				'32':{
					'ville':'Kumbo'
				},
				
				'33':{
					'ville':'Kribi'
				},

				'34':{
					'ville':'Limbe'
				},

				'35':{
					'ville':'Manjo'
				},

				'36':{
					'ville':'Loum'
				},

				'37':{
					'ville':'Makénéné'
				},

				'38':{
					'ville':'Maga'
				},

				'39':{
					'ville':'Magba'
				},

				'40':{
					'ville':'Mbandjock'
				},

				'41':{
					'ville':'Batouri'
				},

				'42':{
					'ville':'Mamfé'
				},

				'43':{
					'ville':'Mora'
				},

				'44':{
					'ville':'Mbanga'
				},

				'45':{
					'ville':'Melong'
				},

				'46':{
					'ville':'Maroua'
				},

				'47':{
					'ville':'Mbalmayo'
				},
				
				'48':{
					'ville':'Mbouda'
				},

				'49':{
					'ville':'Muyuka'
				},

				'50':{
					'ville':'Meiganga'
				},
				
				'51':{
					'ville':'Mokolo'
				},
				
				'52':{
					'ville':'Ngaoundéré'
				},

				'53':{
					'ville':'Nkongsamba'
				},

				'54':{
					'ville':'Nkambé'
				},

				'55':{
					'ville':'Ndop'
				},
				
				'56':{
					'ville':'Nanga-Eboko'
				},

				'57':{
					'ville':'Nkoteng'
				},

				'58':{
					'ville':'Ngaoundal'
				},

				'59':{
					'ville':'Obala'
				},

				'60':{
					'ville':'Pitoa'
				},
				
				'61':{
					'ville':'Sangmélima'
				},

				'62':{
					'ville':'Tcholliré'
				},

				'63':{
					'ville':'Tonga'
				},
				
				'64':{
					'ville':'Tombel'
				},

				'65':{
					'ville':'Touboro'	
				},

				'66':{
					'ville':'Tibati'
				},

				'67':{
					'ville':'Tiko'
				},

				'68':{
					'ville':'Wum'
				},
				
				'69':{
					'ville':'Yagoua'
				},
				
				'70':{
					'ville':'Yokadouma'
				},

				'71':{
					'ville':'Fundong'
				},

				'72':{
					'ville':'Yabassi'
				},

				'73':{
					'ville':'Yaounde'
				}

			},

			
			// ville du gabon



			'1' : {
					'tot' : '33',
					
					'0':{
						'ville':'Bitam'
					},

					'1':{
						'ville':'Booué'
					},

					'2':{
						'ville':'Cocobeach'
					},

					'3':{
						'ville':'Fougamou'
					},
					
					'4':{
						'ville':'Franceville'
					},
					
					'5':{
						'ville':'Gamba'
					},

					'6':{
						'ville':'Kango'
					},
					
					'7':{
						'ville':'Koulamoutou'
					},

					'8':{
						'ville':'Lambaréné'
					},

					'9':{
						'ville':'Lastourville'
					},

					'10':{
						'ville':'Lékoni'
					},

					'11':{
						'ville':'Libreville'
					},

					'12':{
						'ville':'Makokou'
					},	
					
					'13':{
						'ville':'Mbigou'
					},

					'14':{
						'ville':'Medouneu'
					},

					'15':{
						'ville':'Mimongo'
					},

					'16':{
						'ville':'Minvoul'
					},

					'17':{
						'ville':'Mounana'
					},
					
					'18':{
						'ville':'Mouila'
					},

					'19':{
						'ville':'Moanda'
					},

					'20':{
						'ville':'Mayumba'
					},
					
					'21':{
						'ville':'Mitzic'
					},
					
					'22':{
						'ville':'Mékambo'	
					},

					'23':{
						'ville':'Ndendé'
					},

					'24':{
						'ville':'Nkan'
					},
										
					'25':{
						'ville':'Ntoum'
					},

					'26':{
						'ville':'Ndjolé'
					},

					'27':{
						'ville':'Omboué'
					},

					'28':{
						'ville':'Okondja'
					},

					'29':{
						'ville':'Oyem'
					},

					'30':{
						'ville':'Port-Gentil'
					},
					
					'31':{
						'ville':'Tchibanga'
					},

					'32':{
						'ville':'Tsogni'
					}	
					
				},
 

 // ville de madagascar


			'2' : {
					'tot' : '31',
					
					'0':{
						'ville':'Ambanja'
					},

					'1':{
						'ville':'Ambatondrazaka'
					},

					'2':{
						'ville':'Amboasary'
					},

					'3':{
						'ville':'Ambositra'
					},

					'4':{
						'ville':'Amparafaravola'
					},

					'5':{
						'ville':'Antalaha'
					},

					'6':{
						'ville':'Antsirabé'
					},

					'7':{
						'ville':'Betioky'
					},
					
					'8':{
						'ville':'Diego-Suarez'
					},

					'9':{
						'ville':'Fandriana'
					},

					'10':{
						'ville':'Faratsiho'
					},
					
					'11':{
						'ville':'Fianarantsoa'
					},

					'12':{
						'ville':'Ikongo'
					},

					'13':{
						'ville':'Ivongo'
					},

					'14':{
						'ville':'Mahanoro'
					},
    
					'15':{
						'ville':'Majunga'
					},

					'16':{
						'ville':'Mananara Nord'
					},

					'17':{
						'ville':'Manakara'
					},
					
					'18':{
						'ville':'Manjakandriana'
					},

					'19':{
						'ville':'Marovoay'
					},

					'20':{
						'ville':'Morondava'
					},

					'21':{
						'ville':'Nosy'
					},

					'22':{
						'ville':'Sambava'	
					},

					'23':{
						'ville':'Soanierana'
					},

					'24':{
						'ville':'Soavinandriana'
					},		
		
					'25':{
						'ville':'Tananarive'
					},

					'26':{
						'ville':'Tamatave'
					},
					
					'27':{
						'ville':'Tuléar'
					},
					
					'28':{
						'ville':'Tôlanaro'
					},

					'29':{
						'ville':'Varika'
					},
					
					'30':{
						'ville':'Vavatenina'
					}

				},





			// ville du Nigeria
			'3' : {
					'tot' : '51',
					
					
					
					'0':{
						'ville':'Aba'
					},
					
					'1':{
						'ville':'Akure'
					},

					'2':{
						'ville':'Ado Ekiti'	
					},
					
					'3':{
						'ville':'Abeokuta'
					},
					
					'4':{
						'ville':'Bauchi'
					},

					'5':{
						'ville':'Benin City'
					},

					'6':{
						'ville':'Damaturu'
					},

					'7':{
						'ville':'Ede'
					},
					
					'8':{
						'ville':'Efon Alaaye'
					},
					
					'9':{
						'ville':'Enugu'
					},

					'10':{
						'ville':'Gombe'
					},

					'11':{
						'ville':'Gboko'
					},
					
					'12':{
						'ville':'Gusau'
					},
					
					'13':{
						'ville':'Gombe'
					},

					'14':{
						'ville':'Ibadan'
					},

					'15':{
						'ville':'Ife'
					},
					
					'16':{
						'ville':'Iseyin'
					},
					
					'17':{
						'ville':'Ikorodu'
					},
					
					'18':{
						'ville':'Ilesha'
					},
					
					'19':{
						'ville':'Ikot Ekpene	'
					},
					
					'20':{
						'ville':'Ise'
					},
					
					'21':{
						'ville':'Ijebu Ode'
					},
					
					'22':{
						'ville':'Ikire'
					},
					
					'23':{
						'ville':'Iwo'
					},
					
					'24':{
						'ville':'Ilorin'
					},

					'25':{
						'ville':'Jimeta'
					},

					'26':{
						'ville':'Jos'
					},
					
					'27':{
						'ville':'Kano'
					},
				
					'28':{
						'ville':'Kaduna'
					},

					'29':{
						'ville':'Katsina'
					},
					
					'30':{
						'ville':'Lagos'
					},

					'31':{
						'ville':'Maiduguri'
					},

					'32':{
						'ville':'Makurdi'
					},

					'33':{
						'ville':'Minna'
					},
					
					'34':{
						'ville':'Mubi'
					},

					'35':{
						'ville':'Nnewi'
					},

					'36':{
						'ville':'Ogbomosho'
					},
					
					'37':{
						'ville':'Okene'
					},

					'38':{
						'ville':'Owo'
					},

					'39':{
						'ville':'Owerri'
					},

					'40':{
						'ville':'Ondo'
					},
					
					'41':{
						'ville':'Oshogbo'
					},
					
					'42':{
						'ville':'Onitsha'
					},

					'43':{
						'ville':'Oyo'
					},

					'44':{
						'ville':'Port Harcourt'
					},

					'45':{
						'ville':'Shagamu'
					},

					'46':{
						'ville':'Sokoto'
					},
					
					'47':{
						'ville':'Umuahia'
					},

					'48':{
						'ville':'Ugep'
					},

					'49':{
						'ville':'Warri'
					},
					
					'50':{
						'ville':'Zaria'
					}
					
					
			},

			

//ville du Tchad



				'4' : {
					'tot' : '44',
					
					
					'0':{
						'ville':'Abéché'
					},

					'1':{
						'ville':'Am'
					},
					
					'2':{
						'ville':'Ati'
					},
					
					'3':{
						'ville':'Adré'
					},
					
					'4':{
						'ville':'Aozou'
					},

					'5':{
						'ville':'Bongor'
					},
					
					'6':{
						'ville':'Bitkine'
					},

					'7':{
						'ville':'Benoye'
					},
					
					'8':{
						'ville':'Bokoro'
					},
					
					'9':{
						'ville':'Béré'	
					},
                      
					'10':{
						'ville':'Bousso'
					},

					'11':{
						'ville':'Bébédjia'
					},
					
					'12':{
						'ville':'Biltine'
					},

					'13':{
						'ville':'Bol'
					},
					
					'14':{
						'ville':'Beinamar'
					},
        
					'15':{
						'ville':'Baïbokoum'
					},
					
					'16':{
						'ville':'Béboto'
					},
					
					'17':{
						'ville':'Doba'
					},

					'18':{
						'ville':'Dourbali'
					},

					'19':{
						'ville':'Fianga'
					},

					'20':{
						'ville':'Faya-Largeau'
					},

					'21':{
						'ville':'Guelendeng'
					},

					'22':{
						'ville':'Goundi'	
					},
             
					'23':{
						'ville':'Gounou-Gaya'
					},

					'24':{
						'ville':'Kélo'
					},

	             	'25':{
						'ville':'Koumra'
					},
					
					'26':{
						'ville':'Kyabé'
					},

					'27':{
						'ville':'Laï'
					},
					
					'28':{
						'ville':'Léré'
					},

					'29':{
						'ville':'Mao'
					},
					
					'30':{
						'ville':'Massaguet'
					},
					
					'31':{
						'ville':'Massakory'
					},

					'32':{
						'ville':'Massenya'
					},

					'33':{
						'ville':'Melfi'
					},

					'34':{
						'ville':'Moïssala'
					},

					'35':{
						'ville':'Mongo'
					},

					'36':{
						'ville':'Moundou'
					},
					
					'37':{
						'ville':'Moussoro'
					},
					
					'38':{
						'ville':'Ndjamena'
					},

					'39':{
						'ville':'Ngama'
					},

					'40':{
						'ville':'Oum-Hadjer'
					},
					
					'41':{
						'ville':'Pala'
					},

					'42':{
						'ville':'Sarh'
					},
					
					'43':{
						'ville':'Timan'
					}
					
				}

			
	};	






