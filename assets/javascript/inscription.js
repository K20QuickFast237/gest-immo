
$(document).ready(function(){

  var tester = $('#form1indicator').val();

  const formBtn1 = document.querySelector("#btn-1");
  const formBtnPrev2 = document.querySelector("#btn-2-prev");
  const formBtnNext2 = document.querySelector("#btn-2-next");
  const formBtnPrev3 = document.querySelector("#btn-3-prev");
  const formBtn3 = document.querySelector("#btn-3");

  const form1 = formBtn1.parentElement;
  const form2 = formBtnNext2.parentElement;
  const form3 = formBtn3.parentElement;

  const email = document.querySelector('#email');
  const confirm = document.querySelector('#password_confirm');
  // const tel1 = document.querySelector('#telephone1');
  // const tel2 = document.querySelector('#telephone2');
  const wsap = document.querySelector('#whatsapp');


  const gotoNextForm = (prev, next, stepPrev, stepNext) => {
    // Get form through the button
    const prevForm = prev.parentElement
    const nextForm = next.parentElement
    const nextStep = document.querySelector(`.step--${stepNext}`)
    const prevStep = document.querySelector(`.step--${stepPrev}`)
    // Add active/inactive classes to both previous and next form
    nextForm.classList.add("form-active")
    nextForm.classList.add("form-active-animate2")
    prevForm.classList.add("form-inactive2")
    // Change the active step element
    prevStep.classList.remove("step-active")
    nextStep.classList.add("step-active")
    // Remove active/inactive classes to both previous an next form
    form1.classList.remove("form-active")

    setTimeout(() => {
      prevForm.classList.remove("form-active")
      prevForm.classList.remove("form-inactive2")
      nextForm.classList.remove("form-active-animate2")
    }, 1000)
  }

  const gotoPrevForm = (prev, next, stepPrev, stepNext) => {
    // Get form through the button
    const prevForm = prev.parentElement
    const nextForm = next.parentElement
    const nextStep = document.querySelector(`.step--${stepNext}`)
    const prevStep = document.querySelector(`.step--${stepPrev}`)
    // Add active/inactive classes to both previous and next form
    nextForm.classList.add("form-active")
    nextForm.classList.add("form-active-animate")
    prevForm.classList.add("form-inactive")
    // Change the active step element
    prevStep.classList.remove("step-active")
    nextStep.classList.add("step-active")
    // Remove active/inactive classes to both previous an next form
    setTimeout(() => {
      prevForm.classList.remove("form-active")
      prevForm.classList.remove("form-inactive")
      nextForm.classList.remove("form-active-animate")
    }, 1000)
  }

  // var form1status = $('#form1indicator').val();
  // console.log(form1status);
  
  // email value control
    email.addEventListener('change', function(e){
      emailval = $('#email').val();
      var jqxhr = $.ajax({
      method: "POST",
      url: "verifyemail",
      data: { 
      email: emailval
      },
      dataType : 'json'
      })
      .done(function(data) {
        // gotoNextForm(formBtn1,formBtnNext2,1,2);
        if(emailval ==""){
          $('.form--header-text')[0].innerHTML = "Oups! Ce champs est requis";
          $('.form--header-text')[0].style.color = "red";
        }
        else if(data['response'] =="done"){
          $('.form--header-text')[0].innerHTML = "Tell us more about you.";
          $('.form--header-text')[0].style.color = "#fff";
        }
        else {
          $('.form--header-text')[0].innerHTML = "Oups! Cette adresse Email est déjà utilisée";
          $('.form--header-text')[0].style.color = "red";
        }
      })
      .fail(function(data) {
        console.log(data);
      })
    })

  // password_confirm value control
    confirm.addEventListener("change", function(e) {
      if($('#password').val() == $('#password_confirm').val()){
        $('.form--header-text')[0].innerHTML = "Tell us more about you.";
        $('.form--header-text')[0].style.color = "#fff";
      }else{
        $('.form--header-text')[0].innerHTML = "Oups! Confirmation Incorrecte.";
        $('.form--header-text')[0].style.color = "red";
      }
    })
  
  // Button listener of form 1
    formBtn1.addEventListener("click", function(e) {
      if($('#password').val() == $('#password_confirm').val()){
        $('.form--header-text')[0].innerHTML = "Tell us more about you.";
        $('.form--header-text')[0].style.color = "#fff";
        form1.submit();
        // gotoNextForm(formBtn1, formBtnNext2, 1, 2);
      }else{
        $('.form--header-text')[0].innerHTML = "Oups! Confirmation Incorrecte.";
        $('.form--header-text')[0].style.color = "red";
      }

      // e.preventDefault();
    })
  


  // Next button listener of form 2
    // form2.addEventListener("submit", function(e) {
    //   form2.submit();
    //   gotoNextForm(formBtnNext2, formBtn3, 2, 3)
    //   // e.preventDefault()
    // })

  // Previous button listener of form 2
    formBtnPrev2.addEventListener("click", function(e) {
      gotoPrevForm(formBtnNext2, formBtn1, 2, 1)
      e.preventDefault()
    })



    // Previous button listener of form 3
    formBtnPrev3.addEventListener("click", function(e) {
      gotoPrevForm(formBtnPrev3, formBtnNext2, 3, 2)
      e.preventDefault()
    })

  // Submit button listener of form 3
    // form3.addEventListener("submit", function(e) {
    //   form3.submit();
    //   document.querySelector(`.step--3`).classList.remove("step-active")
    //   document.querySelector(`.step--4`).classList.add("step-active")
    //   form3.style.display = "none"
    //   document.querySelector(".form--message").innerHTML = `
    //    <div class="form--message-text">
    //    <h1> Bien Venu Parmi Nous! </h1>
    //    <a>
    //    <button class="btn--validation" id="">Retourner a l'<b>Accueil</b></button>
    //    </a>
    //    <a>
    //    <button class="btn--validation" id="">Creer une <b>Annonce</b></button>
    //    </a>
    //    </div>
    //    `
    //   // e.preventDefault()
    // })
    function afterlocated(){
      document.querySelector(`.step--1`).classList.remove("step-active")
      document.querySelector(`.step--3`).classList.remove("step-active")
      // document.querySelector(`.step--4`).classList.add("step-active")
      form1.style.display = "none"
      document.querySelector(".form--message").innerHTML = `
      <div class="form--message-text">
      <h1> Bien Venu Parmi Nous! </h1>
      `
      setTimeout(()=>{
      },500)
    }

  switch (tester){
    case "identified" : gotoNextForm(formBtn1, formBtnNext2, 1, 2);
        break;
    case "contacted" : gotoNextForm(formBtnNext2, formBtn3, 2, 3);
        break;
    case "located" : afterlocated();
        break;
  }

})