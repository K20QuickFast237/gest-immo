-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : sam. 20 nov. 2021 à 12:50
-- Version du serveur : 10.4.21-MariaDB
-- Version de PHP : 7.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `gest-immo`
--

-- --------------------------------------------------------

--
-- Structure de la table `abonnement`
--

CREATE TABLE `abonnement` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'free',
  `prix` int(11) NOT NULL,
  `etat` varchar(255) NOT NULL DEFAULT 'invalide',
  `date_debut` datetime NOT NULL DEFAULT current_timestamp(),
  `date_fin` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `abonnement`
--

INSERT INTO `abonnement` (`id`, `id_user`, `type`, `prix`, `etat`, `date_debut`, `date_fin`) VALUES
(1, 1, 'annuel', 0, 'valide', '2021-10-14 13:16:33', '2022-01-01');

-- --------------------------------------------------------

--
-- Structure de la table `avis`
--

CREATE TABLE `avis` (
  `id` int(11) NOT NULL,
  `id_user_emetteur` int(11) NOT NULL,
  `id_user_cible` int(11) DEFAULT NULL,
  `id_bien` int(11) DEFAULT NULL,
  `note` int(11) DEFAULT NULL,
  `commentaire` text DEFAULT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `bien`
--

CREATE TABLE `bien` (
  `id` int(11) NOT NULL,
  `id_user_proprio` int(11) DEFAULT NULL,
  `id_user_utilisateur` int(11) DEFAULT NULL,
  `id_localisation` int(11) DEFAULT NULL,
  `id_description` int(11) DEFAULT NULL,
  `id_reservation` int(11) DEFAULT NULL,
  `id_equipement` int(11) DEFAULT NULL,
  `prix` int(11) NOT NULL,
  `nombre_avis` int(11) NOT NULL DEFAULT 0,
  `type` varchar(255) NOT NULL,
  `document` varchar(255) DEFAULT NULL,
  `etat` varchar(255) NOT NULL DEFAULT 'autre'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `bien`
--

INSERT INTO `bien` (`id`, `id_user_proprio`, `id_user_utilisateur`, `id_localisation`, `id_description`, `id_reservation`, `id_equipement`, `prix`, `nombre_avis`, `type`, `document`, `etat`) VALUES
(1, 1, 1, 1, 1, NULL, 1, 670000, 0, 'Appartement', NULL, 'A Echanger');

-- --------------------------------------------------------

--
-- Structure de la table `connexion`
--

CREATE TABLE `connexion` (
  `nom` varchar(255) CHARACTER SET utf8 NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `random` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='Pour API de création de compte';

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `tel1` varchar(50) NOT NULL,
  `tel2` varchar(50) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `whatsapp` varchar(50) NOT NULL,
  `facebook` varchar(50) DEFAULT NULL,
  `twitter` varchar(50) DEFAULT NULL,
  `instagram` varchar(50) DEFAULT NULL,
  `linkedin` varchar(50) DEFAULT NULL,
  `telegram` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `contact`
--

INSERT INTO `contact` (`id`, `id_user`, `tel1`, `tel2`, `email`, `whatsapp`, `facebook`, `twitter`, `instagram`, `linkedin`, `telegram`) VALUES
(1, 1, '2147483647', '2147483647', 'admin@gmail.com', '2147483647', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `description`
--

CREATE TABLE `description` (
  `id` int(11) NOT NULL,
  `id_bien` int(11) NOT NULL,
  `prix` int(11) NOT NULL,
  `superficie` int(11) DEFAULT NULL,
  `nombre_piece` int(11) DEFAULT NULL,
  `image` text DEFAULT NULL,
  `nombre_chambre` int(11) DEFAULT NULL,
  `nombre_salle_manger` int(11) DEFAULT NULL,
  `autre` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `description`
--

INSERT INTO `description` (`id`, `id_bien`, `prix`, `superficie`, `nombre_piece`, `image`, `nombre_chambre`, `nombre_salle_manger`, `autre`) VALUES
(1, 1, 250000, 250, 5, '{\"1\":\"g3.jpg\"}', 2, NULL, '[]');

-- --------------------------------------------------------

--
-- Structure de la table `equipement`
--

CREATE TABLE `equipement` (
  `id` int(11) NOT NULL,
  `id_bien` int(11) NOT NULL,
  `eau_chaude` varchar(255) DEFAULT NULL,
  `chauffage` varchar(255) DEFAULT NULL,
  `climatisation` varchar(255) DEFAULT NULL,
  `espace_evenement` varchar(255) DEFAULT NULL,
  `classeur` varchar(255) NOT NULL,
  `autre` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `equipement`
--

INSERT INTO `equipement` (`id`, `id_bien`, `eau_chaude`, `chauffage`, `climatisation`, `espace_evenement`, `classeur`, `autre`) VALUES
(1, 1, 'ok', NULL, NULL, NULL, 'ok', '[]');

-- --------------------------------------------------------

--
-- Structure de la table `localisation`
--

CREATE TABLE `localisation` (
  `id` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_bien` int(11) DEFAULT NULL,
  `pays` varchar(255) NOT NULL,
  `ville` varchar(255) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `longitude` varchar(255) DEFAULT NULL,
  `latitude` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `localisation`
--

INSERT INTO `localisation` (`id`, `id_user`, `id_bien`, `pays`, `ville`, `adresse`, `longitude`, `latitude`) VALUES
(1, 1, NULL, 'Cameroun', 'Edea', 'Gare', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `id_user_emetteur` int(11) NOT NULL,
  `id_user_cible` int(11) NOT NULL,
  `contenu` text DEFAULT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp(),
  `etat` varchar(255) NOT NULL DEFAULT 'non lu'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_message` int(11) DEFAULT NULL,
  `contenu` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `profil`
--

CREATE TABLE `profil` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_bien` int(11) DEFAULT NULL,
  `id_services` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `profil`
--

INSERT INTO `profil` (`id`, `id_user`, `id_bien`, `id_services`, `image`, `description`, `date_creation`) VALUES
(1, 1, NULL, 1, NULL, 'Administrateur du site, Toujours disponible... ', '2021-10-14 13:16:33');

-- --------------------------------------------------------

--
-- Structure de la table `publication`
--

CREATE TABLE `publication` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_bien` int(11) DEFAULT NULL,
  `id_profil` int(11) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `etat` varchar(255) NOT NULL DEFAULT 'invalide',
  `date_disponibilite` varchar(255) NOT NULL,
  `duree_reservation` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_bien` int(11) DEFAULT NULL,
  `duree` int(11) NOT NULL,
  `prix` int(11) NOT NULL,
  `date_creation` datetime NOT NULL DEFAULT current_timestamp(),
  `date_fin` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_profil` int(11) NOT NULL,
  `maintenance de la plateforme` varchar(255) DEFAULT NULL,
  `demenagement` varchar(255) DEFAULT NULL,
  `peinture` varchar(255) DEFAULT NULL,
  `plomberie` varchar(255) DEFAULT NULL,
  `maintenance` varchar(255) DEFAULT NULL,
  `elaboration_de_contrats` varchar(255) DEFAULT NULL,
  `assistance_juridique` varchar(255) DEFAULT NULL,
  `contrat_de_bail` varchar(255) DEFAULT NULL,
  `gestion_location` varchar(255) DEFAULT NULL,
  `assurance_locative` varchar(255) DEFAULT NULL,
  `prospection` varchar(255) DEFAULT NULL,
  `construction` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `services`
--

INSERT INTO `services` (`id`, `id_user`, `id_profil`, `maintenance de la plateforme`, `demenagement`, `peinture`, `plomberie`, `maintenance`, `elaboration_de_contrats`, `assistance_juridique`, `contrat_de_bail`, `gestion_location`, `assurance_locative`, `prospection`, `construction`) VALUES
(1, 1, 0, 'ok', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, 0, 'ok', 'ok', 'ok', 'ok', 'ok', 'ok', NULL, 'ok', 'ok', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `typeannonce`
--

CREATE TABLE `typeannonce` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `typeannonce`
--

INSERT INTO `typeannonce` (`id`, `nom`) VALUES
(1, 'Echange'),
(2, 'Location'),
(3, 'Vente'),
(4, 'Offre de Services');

-- --------------------------------------------------------

--
-- Structure de la table `typebien`
--

CREATE TABLE `typebien` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `typebien`
--

INSERT INTO `typebien` (`id`, `nom`) VALUES
(1, 'Appartement'),
(2, 'Studio'),
(3, 'Chambre'),
(4, 'Bureau'),
(5, 'Maison');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `id_contact` int(11) DEFAULT NULL,
  `id_profil` int(11) DEFAULT NULL,
  `id_localisation` int(11) DEFAULT NULL,
  `id_services` int(11) DEFAULT NULL,
  `niveau` int(11) NOT NULL DEFAULT 1,
  `nombre_publication` int(11) NOT NULL DEFAULT 0,
  `nombre_avis` int(11) NOT NULL DEFAULT 0,
  `nombre_mesg_non_lu` int(11) NOT NULL DEFAULT 0,
  `nombre_notification` int(11) NOT NULL DEFAULT 0,
  `nombre_annonce` int(11) NOT NULL DEFAULT 0,
  `etat_abonnement` varchar(255) NOT NULL DEFAULT 'invalide',
  `date_inscription` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `email`, `password`, `id_contact`, `id_profil`, `id_localisation`, `id_services`, `niveau`, `nombre_publication`, `nombre_avis`, `nombre_mesg_non_lu`, `nombre_notification`, `nombre_annonce`, `etat_abonnement`, `date_inscription`) VALUES
(1, 'admin', 'admin@gmail.com', 'admin01', 1, 1, 1, 1, 5, 0, 0, 0, 0, 0, 'valide', '2021-10-14 13:16:33'),
(2, 'moderateur', 'moderateur@gmail.com', 'mederateur01', NULL, NULL, NULL, 2, 5, 0, 0, 0, 0, 0, 'valide', '2021-10-15 16:44:59');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `abonnement`
--
ALTER TABLE `abonnement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `abonnement_user_fk` (`id_user`);

--
-- Index pour la table `avis`
--
ALTER TABLE `avis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_avis_user_emetteur_fk` (`id_user_emetteur`),
  ADD KEY `id_avis_user_cible_fk` (`id_user_cible`),
  ADD KEY `id_avis_bien_fk` (`id_bien`);

--
-- Index pour la table `bien`
--
ALTER TABLE `bien`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_bien_user_proprio_fk` (`id_user_proprio`),
  ADD KEY `id_bien_user_utilisateur_fk` (`id_user_utilisateur`);

--
-- Index pour la table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contact_user_fk` (`id_user`);

--
-- Index pour la table `description`
--
ALTER TABLE `description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_description_bien_fk` (`id_bien`);

--
-- Index pour la table `equipement`
--
ALTER TABLE `equipement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_equipement_bien_fk` (`id_bien`);

--
-- Index pour la table `localisation`
--
ALTER TABLE `localisation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_localisation_user_fk` (`id_user`),
  ADD KEY `id_localisation_bien_fk` (`id_bien`);

--
-- Index pour la table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_message_user_emetteur_fk` (`id_user_emetteur`),
  ADD KEY `id_message_user_cible_fk` (`id_user_cible`);

--
-- Index pour la table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `profil`
--
ALTER TABLE `profil`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profil_user_fk` (`id_user`),
  ADD KEY `profil_services_fk` (`id_services`);

--
-- Index pour la table `publication`
--
ALTER TABLE `publication`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_publication_bien_fk` (`id_bien`),
  ADD KEY `id_publication_user_fk` (`id_user`),
  ADD KEY `id_publication_profil_fk` (`id_profil`);

--
-- Index pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_reservation_user_fk` (`id_user`),
  ADD KEY `id_reservation_bien_fk` (`id_bien`);

--
-- Index pour la table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `services_user_fk` (`id_user`),
  ADD KEY `id_profil` (`id_profil`);

--
-- Index pour la table `typeannonce`
--
ALTER TABLE `typeannonce`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `typebien`
--
ALTER TABLE `typebien`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `abonnement`
--
ALTER TABLE `abonnement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `avis`
--
ALTER TABLE `avis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `bien`
--
ALTER TABLE `bien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `description`
--
ALTER TABLE `description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `equipement`
--
ALTER TABLE `equipement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `localisation`
--
ALTER TABLE `localisation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `profil`
--
ALTER TABLE `profil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `publication`
--
ALTER TABLE `publication`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `typeannonce`
--
ALTER TABLE `typeannonce`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `typebien`
--
ALTER TABLE `typebien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `abonnement`
--
ALTER TABLE `abonnement`
  ADD CONSTRAINT `abonnement_user_fk` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `avis`
--
ALTER TABLE `avis`
  ADD CONSTRAINT `id_avis_bien_fk` FOREIGN KEY (`id_bien`) REFERENCES `bien` (`id`),
  ADD CONSTRAINT `id_avis_user_cible_fk` FOREIGN KEY (`id_user_cible`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `id_avis_user_emetteur_fk` FOREIGN KEY (`id_user_emetteur`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `bien`
--
ALTER TABLE `bien`
  ADD CONSTRAINT `id_bien_user_proprio_fk` FOREIGN KEY (`id_user_proprio`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `id_bien_user_utilisateur_fk` FOREIGN KEY (`id_user_utilisateur`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `contact`
--
ALTER TABLE `contact`
  ADD CONSTRAINT `contact_user_fk` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
